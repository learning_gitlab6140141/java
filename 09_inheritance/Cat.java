package animal;
public class Cat extends PetAnimal //this is child class, this extends keyword helps object to access parent class

{
	public static void main (String[] args)
	{
	Cat cc= new Cat();
	cc.pet();
	cc.sound();
	}
	public void sound() //method overriding in child class
	{
		System.out.println("meow");
	}
}

/*java animal.Cat 
out put:

i am a pet
meow
*/