package animal;
public class Dog //this is child class, this extends keyword helps object to access parent class

{
	public static void main (String[] args)
	{
	Dog dd= new Dog();
	dd.pet();
	dd.sound();
	}
	public void sound() //method overriding in child class
	{
		System.out.println("bark");
	}
	
}

/*java animal.Dog 
out put:

i am a pet
bark*/
