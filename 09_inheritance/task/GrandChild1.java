package generation;
public class GrandChild1 extends Child2
{
	public static void main(String[] args1)
	{
		GrandChild1 myObj= new GrandChild1();
			myObj.doing();
			myObj.gParent();
			myObj.self();
	}
	public void self()
	{
	System.out.println("from GrandChild1 class==> 1st grand child");
	}
}

/*
**out put**
from child2 class==> i am on mytask
from GrandParent class==> you are my grand child
from GrandChild1 class==> 1st grand child

*/