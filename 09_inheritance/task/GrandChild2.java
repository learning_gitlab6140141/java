package generation;
public class GrandChild2 extends Child2
{
public static void main(String[] args2)
	{
		GrandChild2 myObj= new GrandChild2();
			myObj.gParent();
			myObj.self();
			myObj.calPar();
	}
	public void self()
	{
	super.self();//using 'super' key word for calling parent method using child object
	System.out.println("from GrandChild2 class==> 2nd grand child");
	}
}

/*
**out put**
from GrandParent class==> you are my grand child
from Child2 class==> i am 2nd child
from GrandChild2 class==> 2nd grand child
from Parent class==> i am parent

*/