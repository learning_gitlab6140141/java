package collection_Comparable_Comparator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class laptopMonish implements Comparator{
  int price;
  String brand;
  int ram;

  public laptopMonish(String brand, int ram , int price) {
    this.price=price;
    this.ram=ram;
    this.brand=brand;
  }

  public laptopMonish() {
    // TODO Auto-generated constructor stub
  }

  public static void main(String[] args) {
    ArrayList al = new ArrayList();
    al.add(new laptopMonish("lenovo",12,20000));
    al.add(new laptopMonish("dell",12,3000));
    al.add(new laptopMonish("asus",14,3000));
    
    laptopMonish c = new laptopMonish();
    Collections.sort(al,c);
    for (Object o : al) {
		System.out.println(o);
	}
  }

  @Override
  public int compare(Object o1, Object o2) {
    
    laptopMonish l1= (laptopMonish) o1;
    laptopMonish l2= (laptopMonish) o2;
    
    if(l2.ram>l1.ram)
      return -1;
      else
    return 1;
  }
  @Override
  public String toString() {
    
    return ""+price+" "+brand+" "+ram;
  }
}