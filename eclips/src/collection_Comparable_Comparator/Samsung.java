package collection_Comparable_Comparator;

import java.util.ArrayList;
import java.util.Collections;

public class Samsung implements Comparable {
	
	int price, ram, mp;

	public Samsung(int price, int ram, int mp) {
		this.price = price;
		this.ram = ram;
		this.mp = mp;
	}

	public static void main(String[] args) {
		Samsung s1 = new Samsung(10101, 6, 12);
		Samsung s2 = new Samsung(10010, 6, 12);
		int result =s1.compareTo(s2);
		if(result>0)
			System.out.println("s1 price is high");
		else if(result<0)
			System.out.println("s2 price is high");
		else
			System.out.println("s1 and s2 price are equal");
		
		ArrayList name=new ArrayList();
		name.add("eswaramoorthy");
		name.add("manoj");
		name.add("monisha");
		name.add("sarath Kumar");
		name.add("monish");
		name.add("vignesh");
		System.out.println(name);
		ComparatorDemo cd=new ComparatorDemo();
		Collections.sort(name,cd);
		System.out.println(name);
		
	}

	@Override //upcasting
	public int compareTo(Object obj) {
		Samsung s2=(Samsung) obj;//down casting
		
		if(this.price>s2.price)
		return +123;
		else if(s2.price >this.price)
			return -987;
		
		return 0;
	}

}
