package learnRegex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {

	private Object apl;

	public static void main(String[] args) {
		RegexDemo rd = new RegexDemo();
		rd.basic();
		rd.apl();
		rd.spaceCheck();
		rd.capSma();
		rd.matchWord();
		rd.mblNo();	
		rd.split();
		rd.pat();

	}

	private void pat() {
		int i=0;
		String pattern = "fa";
		String input = "fafafafaffaaffaafafafafa";
		Pattern patternObj = Pattern.compile(pattern);
		Matcher matcherObj = patternObj.matcher(input);
		while (matcherObj.find()) {
			System.out.print(" "+matcherObj.group() + "-"+ ++i);
		}

	}

	private void split() {
		String pattern = "-";
		String input = "28-March-2023";
		Pattern patternObj = Pattern.compile(pattern);
		String[] items = patternObj.split(input);
		for (int i = 0; i < items.length; i++) {
			System.out.println("\n"+items[i]);
		}
		
		for (String st : items) {
			System.out.println("fore "+st);
		}
	}

	private void mblNo() {
		System.out.println();
		String mobile = "919884012810";
//		 Pattern patternObj = Pattern.compile("[6-9][0-9]{9}");//when entered country code made some error
		Pattern patternObj = Pattern.compile("(0|91)?[6-9][0-9]{9}"); // '?' tells that it may have or may not
		Matcher matcherObj = patternObj.matcher(mobile);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group());
		}

	}

	private void matchWord() {

		String password = "Chennai is the capital of TamilNadu";
		Pattern patternObj = Pattern.compile("TamilNadu$");
//		System.out.println("search last match using $");
//		Pattern patternObj = Pattern.compile("[^a]");
		// ("[^A-Z0-9]")- delete things given inside this[^x]
		Matcher matcherObj = patternObj.matcher(password);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group());
		}
	}

	private void capSma() {
		String password = "Chennai 600042 is Velachery";
		Pattern patternObj = Pattern.compile("\\D");
		Matcher matcherObj = patternObj.matcher(password);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group());
		}
	}

	private void basic() {
		String input = "My mobile number is AGR 9884010000 and 545445456 my fathers 9830283746";
//	Pattern patternObj = Pattern.compile("\\d{10}");
		// \D is a character class that matches any non-digit character.
		// It is the opposite of \d, which matches any digit.
//	Pattern patternObj = Pattern.compile("[0123456789]"); 
//	Pattern patternObj = Pattern.compile("[0-9]"); 
	Pattern patternObj = Pattern.compile("[a-zA-Z]"); //can use A-z
//		Pattern patternObj = Pattern.compile("[A-e1-5]");
		Matcher matcherObj = patternObj.matcher(input);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group()+" ");
//	   System.out.println(matcherObj.start());
//	   System.out.println(matcherObj.end());

		}

	}

	private void spaceCheck() {
		System.out.println("\nspace check");
		String password = "Chennai is  the capital city";
		Pattern patternObj = Pattern.compile("\\s");// S-to remove space.
		Matcher matcherObj = patternObj.matcher(password);
		int count = 0;
		while (matcherObj.find()) {
			count++;
			System.out.print(matcherObj.group());
		}
		System.out.println(count);

	}

	private void apl() {
		String password = "Chennai1234@gmail.com";
		Pattern patternObj = Pattern.compile("[A-Za-z0-9]");
		Matcher matcherObj = patternObj.matcher(password);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group());
		}
	}

}
