package typeCasting;

public class Engineer extends Student  {

	public static void main(String[] args) {
		Engineer eng=new Engineer();
		eng .doProject();
//		eng.study();
		
		Student sEng=new Engineer();
//		sEng.study();
		
		Student s=eng;            //up-cast(or) widen casting
		s.study();

	}

	public void study() {
		System.out.println("reference");
	}

	public void doProject() {
		System.out.println("doing project");
		
	}

}
