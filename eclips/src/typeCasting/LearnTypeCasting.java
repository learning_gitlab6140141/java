package typeCasting;

public class LearnTypeCasting {

	public static void main(String[] args) {
		LearnTypeCasting ltc= new LearnTypeCasting ();
		ltc.flToInt();
	}

	private void flToInt() {
		float price=103.70f;
		int price1=(int)price;
		System.out.println("float :"+price);
		System.out.println("float->int :"+price1);
		
		float price2=price1;
		System.out.println("int->float : "+price2);
		
//		String d="12";
//		int str= (int)d;
		
		char ch='a';
		int chNo=ch;
		System.out.println("char->numb :"+chNo);
		
		int no=1;
		//boolean boo=(boolean)no;
		
//		boolean val=true;
//		String booStr=(String) val;
		
	}

}
