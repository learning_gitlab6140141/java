package typeCasting;

public class Student {

	public static void main(String[] args) {
		Student st=new Student();
//		st.study();
		
		Student se=new Engineer();
		System.out.println("stu obj eng");
		se.study();
		Engineer en = (Engineer)se;
		en.doProject();          //down-casting (or) Narrow-casting
		en.study();
		
		
//		Engineer sng=(Engineer) new Student();
//		sng.study();
	}
	public void study() {
		System.out.println("course book");
	}
}
