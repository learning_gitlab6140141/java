package pattern;

public class InvPat {
	int r, c;
	int n =3;

	public static void main(String[] args) {
		InvPat ip = new InvPat();
//		ip.revTr();
		ip.tri();
//		ip.invTri();
//		ip.trial();
		
	}

	private void trial() {
		for(r=n;r>=1;r--)
		{
//			for(c=n;c>r;c--) {
//				System.out.print("."); 
//			}
			for(c=1;c<=r;c++) {
				System.out.print(c+" "); 
			}
			System.out.println(); 
		}
		
	}

	private void tri() {
		for(r=1;r<=n;r++)
		{
			for(c=n;c>r;c--) {
				System.out.print("."); 
			}
			for(c=1;c<=r;c++) {
				System.out.print(c+" "); 
			}
			System.out.println(); 
		}
		
	}

	private void invTri() {
		//also equi tria
		for(r=1;r<=n;r++)
		{
			for(c=1;c<r;c++) {
				System.out.print(" "); 
			}
			for(c=n;c>=r;c--) {
				System.out.print(c+" "); 
			}
			System.out.println(); 
		}
		for(r=n;r>=1;r--)
		{
			for(c=n;c>r;c--) {
				System.out.print("."); 
			}
			for(c=1;c<=r;c++) {
				System.out.print(c+" "); 
			}
			System.out.println(); 
		}
		
	}

	private void revTr() {
		for (r = 1; r <= 3; r++) {
	
//				for (c = 3; c >=(r+1); c--) {
//					System.out.print("@ ");
//						}
//				System.out.println();
				
//				for (c = 3; c >=(r); c--) {
//					System.out.print("# ");
//					for (c = 1; c <=(r-1); c++) {
//						System.out.print("@ ");}
//				}
//				System.out.println();
			for (c=1;c<=r;c++) {
				System.out.print("# ");
				for (int a = 3; a >=(r+1); a--) {
					System.out.print("@ ");
						}
			}
			System.out.println();
			
		}

	}

}
