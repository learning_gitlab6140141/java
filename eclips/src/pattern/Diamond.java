package pattern;

public class Diamond {
	int n=4;
	int r,c;
	
	public static void main(String[] args) {
		Diamond dd=new Diamond();
		dd.uppr();
		dd.dwpr();
//		int[] array = {1, 2, 3, 4, 5};
//		String s[] ={"monish"};
//		int a = 3;
//		System.out.print(s[0]," stuff here bah bla %s" );

	}

	private void dwpr() {
		for(r=(n-1);r>=1;r--) {
			for(c=n;c>r;c--) {
				System.out.print(". ");
			}
			for(c=1;c<=r;c++){
				System.out.print(c+" ");
			}
			for(c=1;c<r;c++){
				System.out.print(c+" ");
			}
			for(c=n;c>r;c--) {
				System.out.print(". ");
			}
			System.out.println();
		}
		
	}

	private void uppr() {
		for(r=1;r<=n;r++) {
			for(c=n;c>r;c--) {
				System.out.print(". ");
			}
			for(c=1;c<=r;c++) {
				System.out.print(c+" ");
			}
			for(c=1;c<r;c++){
				System.out.print(c+" ");
			}
			for(c=n;c>r;c--) {
				System.out.print(". ");
			}			
			System.out.println();
		}
		
	}

}
