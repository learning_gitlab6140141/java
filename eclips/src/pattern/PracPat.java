package pattern;

public class PracPat {

	public static void main(String[] args) {
		PracPat pp=new PracPat();
		int n=5;
//		pp.inHaj(n);
		pp.hasAj(n);
		byte a=2;
		System.out.println(a);
		byte b=3;
		System.out.println(b);
		int c=a+b;
		System.out.println(c);
	}

	private void hasAj(int n) {
		for(int i=n;i>0;i--) {
			for(int j=n;j>=i;j--) {
				System.out.print(j+" ");
			}
			
			for(int k=1;k<i;k++)
				System.out.print(i+" ");
			
			System.out.println();
		}
		
	}

	private void inHaj(int n) {
		for (int row=1;row<=n;row++) {
			for(int col=n;col>=row;col--) {
				System.out.print(col+ " ");
			}
			
			for(int col2=1;col2<row;col2++)
				System.out.print(row+" ");
			
			System.out.println();
		}
		
	}

}
