package pattern;

import java.util.Scanner;

public class Zer_One {
	
	
	
//	public Zer_One(int n) {
//		this.n=n;
//	}

	public static void main(String[] args) {
		System.out.print("enter an number: ");
		Scanner scno=new Scanner(System.in);
		int n=scno.nextInt();
		
		Zer_One zo=new Zer_One();
//		zo.midNo(n);
		zo.midZe(n);
	}

	private void midZe(int n) {
		for (int i = n; i >= 0; i--) {
			for (int j = i; j >= 0; j--) {
				System.out.print(j+" ");
			}
			for (int k=1 ; k<=n-i;k++)
				System.out.print(k+" ");
			System.out.println();
		}

	}

	private void midNo(int n) {
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=n;j++) {
				if(i+j==n+1 || i==j)
					System.out.print(i+" ");
				else
					System.out.print("0 ");
				
			}
			System.out.println();
		}
		
	}

}
