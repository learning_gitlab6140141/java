package pattern;

public class DiffPatt {

	public static void main(String[] args) {
		DiffPatt dp=new DiffPatt();
		int n=5;
//		dp.starNu(n);
//		dp.prim(n);
//		dp.halTri(n);
//		dp.arrow(n);
//		dp.ZS(n);
		dp.nuX(n);
		

	}

	private void nuX(int n) {
		for(int r=1;r<=n;r++) {
			for(int c=1;c<=n;c++) {
				if(r==c||r+c==n+1) 
					System.out.print(c+" ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
		
	}

	private void ZS(int n) {
		for(int r=1;r<=n;r++) {
			for(int c=1;c<=(n*2)-1;c++) {
				if(r==c||c==(n)||r+c==(n*2))
				System.out.print("* ");
				else
					System.out.print(0+" ");
			}
			System.out.println();
		}
		
	}

	private void arrow(int n) {
	
		for (int i=n;i>=0;i--) {
			int inc=i;
			for(int j=n;j>=i;j--) {
				System.out.print(inc+++" ");
			}
			System.out.println();
		}
	
		for(int i=1;i<=n;i++) {
			int inc=i;
			for(int j=n;j>=i;j--) {
				System.out.print(inc+++" ");
			}
			System.out.println();
		}
		
	}

	private void halTri(int n) {
		int te=(n*3)-3;
		for(int i=1;i<=n;i++) {
			for (int j=1;j<=i;j++) {
				if(j==1)
					System.out.print(i+" ");
				
				else if(i==n) {
					System.out.print((i+j-1)+" ");
				}
				
				else if(i!=j)
					System.out.print("  ");
				
				else if(i==j) {
					System.out.print(""+(te--));
					}

			}
			System.out.println();
		}
		
	}

	private void prim(int n) {
		for (int i=1;i<=n;i++) {
			for (int j=n;j>i;j--)
				System.out.print(" ");
			for(int j=1;j<=i;j++)
				System.out.print(i+" ");
			System.out.println();
		}
		
		
	}

	private void starNu(int n) {
		for(int i=n;i>=0;i--) {
			for (int j=i;j>=0;j--) {
				if(i==j)
					System.out.print(i+" ");
				else
				System.out.print("* ");
			}
			System.out.println();
		}
		
	}

}
