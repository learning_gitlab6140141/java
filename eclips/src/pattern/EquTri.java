package pattern;

public class EquTri {
	
	int r,c;
	int n=4;

	public static void main(String[] args) {
		EquTri et=new EquTri();
		et.fst();
//		et.sec();
	}

	private void sec() {
		for (r=1;r<=n;r++) {
			for(c=1;c<r;c++) {
				System.out.print("  ");
			}
			for(c=n;c>=r;c--) {
				System.out.print(c+" ");
			}
			for(c=n;c>r;c--) {
				System.out.print(c+" ");
			}
			System.out.println();
		}
		
	}

	private void fst() {
		for (r=1;r<=n;r++) {
			for(c=n;c>r;c--) {
				System.out.print("  ");
			}
			for(c=1;c<=r;c++) {
				System.out.print(c+" ");
			}
			for(c=r;c>1;c--) {
				System.out.print(c-1+" ");
			}
			System.out.println();
		}
		
	}

}
