package singletone;

public class SingletonMain {
	private static int i=1;
public static void main(String[] args) {
//	Singleton st=new SingletonMain();
	
	Singleton st1=Singleton.getInstance("a");
	Singleton st2=Singleton.getInstance("c");
	
	st1.displayMsg(5);
	st2.displayMsg(6);
	
	
}
public void displayMsg(int n) {
	
	System.out.println("callued overriding method "+i+++" time "+n);
}
}
