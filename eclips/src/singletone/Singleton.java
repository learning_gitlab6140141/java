package singletone;

public class Singleton {
	private static Singleton singleton;// private- only this methods can access this cant assceess from outer ;
										//static class dependent
	private static int i=1;

	private Singleton(String st) {
		System.out.println("constructor-->  Singleton "+st);
	}

	public static Singleton getInstance(String st) {
		int i=1;
		if (singleton == null)// on calling first its null
		{
			System.out.println("obj created"+i++);
			singleton = new Singleton("d");
		}
		System.out.println(st+" string");
		return singleton;

	}

	public void displayMsg(int n) {
		
		System.out.println("callued using singletone method "+i+++" time "+n);
	}

}
