package sortingMeth;

import java.util.Arrays;

public class BubbleSort {

	public static void main(String[] args) {
		int[] nums = { 2, 0, 2, 1, 1, 2 };
		sortColors(nums);
	}

	private static void sortColors(int[] n) {
		for(int i=0;i<n.length;i++) {
			for (int j=i+1;j<n.length;j++) {
				if(n[i]>n[j]) {
					int te=n[i];
					n[i]=n[j];
					n[j]=te;
				}
			}
		}
		System.out.println(Arrays.toString(n));
	}

}
