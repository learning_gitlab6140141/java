package sortingMeth;

import java.util.Arrays;

public class MergSort {

	public static void main(String[] args) {
		int[] nums = { 2, 0, 2, 1, 1, 0 };
		int[] ou=(sortColors(nums));
		System.out.println(Arrays.toString(ou));

	}

	private static int[] sortColors(int[] n) {
		int len = n.length;

		if (len == 1)
			return n;

		int[] left = sortColors(Arrays.copyOfRange(n, 0, len / 2));
		int[] right = sortColors(Arrays.copyOfRange(n, len / 2, len));

		return merge(left, right);
	}

	private static int[] merge(int[] fir, int[] sec) {
		int[] joined = new int[fir.length + sec.length];
		int i = 0, j = 0, k = 0;
		while (i < fir.length && j < sec.length) {
			if (fir[i] < sec[j])
				joined[k++] = fir[i++];
			else
				joined[k++] = sec[j++];
		}

		while (i < fir.length)
			joined[k++] = fir[i++];
		while (j < sec.length)
			joined[k++] = sec[j++];
		return joined;
	}

}
