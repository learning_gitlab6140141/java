package array;

public class reAry {
	static int max;
	static int min;

	public static void main(String[] args) {
		reAry ra = new reAry();
		ra.oneD();
		ra.twoD();
		System.out.println("max in " + max);
		System.out.println("min in " + min);
	}

	private void twoD() {
		System.out.println("2D");
		int[][] gros = { { 56, 98, 54, 76, 81 },
				         { 55, 78, 105, 67, 34, 54 },
				         { 70, 85, 76, 84, 93, 45, 60 } };
		max = 0;
		min = gros[0][0];
		System.out.println("gros :" + gros.length);
		System.out.println("gros[0] :" + gros[0].length);
		for (int i = 0; i < gros.length; i++) {
			for (int j = 0; j < gros[i].length; j++) {
				System.out.print(gros[i][j] + " ");

			}
			System.out.println();
		}
		System.out.println();

		for (int i = 0; i < gros.length; i++) {
			for (int j = 0; j < gros[i].length; j++) {

				if (max < gros[i][j])
					max = gros[i][j];

				if (min > gros[i][j])
					min = gros[i][j];
			}

		}
	}

	private void oneD() {
		int[] price = { 10, 40, 50, 80, 74 };
		System.out.println("pri length: " + price.length);
		for (int a = 0; a < price.length; a++) {
			System.out.println(price[a]);
		}

	}

}
