package array;

public class Grade {

	int[] mark = { 90, 98, 99, 58, 98 };

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grade g = new Grade();
		g.grade(g.avg());
	}

	private void grade(int avg) {
		int fail = 0;
		int max = 0;
		int min = mark[0];
		System.out.println("Average= " + avg);

		for (int i = 0; i < mark.length; i++) {
			if(mark[i]>=max)
			{
				max = mark[i];
			}
			if(mark[i]<=min)
			{
				min = mark[i];
			}
			if (mark[i] < 35) {
				fail += 1;
			}
		}
		System.out.println("Max mark "+max);
		System.out.println("Min mark "+min);
				
		
		if (fail == 0) {
			if (avg >= 90) {
				System.out.println("Grade:A+");
			} else if (avg >= 80) {
				System.out.println("Grade:A");
			} else if (avg >= 70) {
				System.out.println("Grade:B+");
			} else if (avg >= 60) {
				System.out.println("Grade:B");
			} else if (avg >= 50) {
				System.out.println("Grade:C");
			} else {
				System.out.println("Grade:D");
			}
		} else {
			System.out.println("Grade:F");
			System.out.println("no of subjects failed= "+fail);

		}
	}

	private int avg() {

		int total = 0;
		int avg = 0;

		for (int i = 0; i < mark.length; i++) {
			total += mark[i];
		}

		avg = total / mark.length;

		return avg;

	}

}
