package array;

public class MultDArray {
	int r = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MultDArray ma = new MultDArray();
		ma.twoD();
		ma.threeD();

	}

	private void threeD() {
		// TODO Auto-generated method stub
		int student = 0;
		
		System.out.println("\n3D Array\n");
		int[][][] mark = { { { 90, 65, 89, 100, 81 },
							 { 45, 25, 89, 60,18 },
							 { 35, 89, 70, 58, 90 } },
				{ { 56, 75, 90, 38, 61 },
				  { 45, 25, 35, 46, 18 },
				  { 35, 89, 60, 58, 90 } } };
		
		System.out.println(mark.length);//2
		System.out.println(mark[0].length);//3
		System.out.println(mark[0][0].length);//5
		
		for (;student< mark.length;student++) {
			for(int term = 0;term< mark[student].length;term++) {
				for(int score = 0;score< mark[student][term].length;score++) {
					System.out.print(mark[student][term][score]+" ");
				}
				System.out.println();				
			}
			System.out.println();	
		}

	}

	private void twoD() {
		// TODO Auto-generated method stub
		System.out.println("2D Array\n");
		int[][] mark = { { 90, 65, 89, 70, 81 },
						 { 45, 25, 89, 100, 18 },
						 { 35, 89, 70, 58, 90 } };
//while (r<=2)
//{
//	int c=0;
//	while (c<=4)
//	{
//		System.out.print(mark[r][c]+" ");
//		c++;
//	}
//	System.out.println();
//	r++;
//}

		for (; r < mark.length; r++) {

			for (int c = 0; c < mark[r].length; c++) {
				System.out.print(mark[r][c] + " ");
			}
			System.out.println();
		}

	}

}
