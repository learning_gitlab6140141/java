package test;

import java.util.Scanner;

public class GetInput {

    public static void main(String[] args) {
        GetInput gi = new GetInput();
//        Scanner so = new Scanner(System.in);
//
//        System.out.println("Enter two numbers: ");
//        double no1 = so.nextInt();
//        double no2 = so.nextInt();
//
//        System.out.println("Enter an operator (+, -, *, /): ");
//        String oper = so.next();
//        gi.math(no1, no2, oper);
        
        gi.ary();
    }

    private void ary() {
    	Scanner inp=new Scanner(System.in);
    	System.out.println("enter the size");
    	int size= inp.nextInt();
    	int[] ar=new int[size];
    	System.out.println("enter "+size+" numbers");
    	for (int value : ar) {
			value=inp.nextInt();
		}
    	for (int out : ar) {
			System.out.println(out);
		}
		
	}

	private void math(double no1, double no2, String oper) {
        
        switch (oper) {
            case "+":
                System.out.println("Sum of them: " + (no1 + no2));
                break;
            case "-":
                System.out.println("Difference between them: " + (no1 - no2));
                break;
            case "*":
                System.out.println("Product of them: " + (no1 * no2));
                break;
            case "/":
            	try {
            		System.out.println("Product of them: " + (no1 / no2));
            	}
            	catch (ArithmeticException ae){
            		System.out.println("no2 cannot be zero");
            	}

                break;
            default:
                System.out.println(oper+" is an Invalid operator");
        }
    }
}
