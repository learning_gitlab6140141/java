package looping;

public class NumSeri {
	
	public static void main(String[] args) {
		int row =1;
		int no =1;
		while (row <= 4)
		{
			int col =1;
			
			while(col <= 5)
			{
				System.out.print(no+" ");
				no++;
				col++;
			}
			
			row++;
			System.out.println();
		}
	}

}
