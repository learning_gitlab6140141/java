package looping.copy;

public class RowColSwap {
	
	public static void main(String[] args) {
		int row =1;
		while (row<=5)
		{
			int col =5;
			while(col >= 1)
			{
				System.out.print(col+" ");
				col--;
			}
			row++;
			System.out.println();
		}
		}

}
