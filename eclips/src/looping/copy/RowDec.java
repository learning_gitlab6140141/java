package looping.copy;

public class RowDec {
	
	public static void main(String[] args) {
		int row =5;
		while (row>=1)
		{
			int col =1;
			while(col<5)
			{
				System.out.print(row+" ");
				col++;
			}
			row--;
			System.out.println();
		}
		}
	}
