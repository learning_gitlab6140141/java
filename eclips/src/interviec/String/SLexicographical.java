package interviec.String;

//Lexicographical Order (Dictionary Order) 
import java.io.*;
import java.util.Arrays; 

public class SLexicographical { 

 // this method sort the string array lexicographically. 
 public static void
 sortLexicographically(String strArr[]) 
 { 
     for (int i = 0; i < strArr.length; i++) { 
         for (int j = i + 1; j < strArr.length; j++) { 
             if (strArr[i].compareToIgnoreCase(strArr[j]) > 0)
             { 
                 String temp = strArr[i]; 
                 strArr[i] = strArr[j]; 
                 strArr[j] = temp; 
             } 
         } 
     } 
 } 

 // this function prints the array passed as argument 
 public static void printArray(String strArr[]) 
 { 
     for (String string : strArr) 
         System.out.print(string + " "); 
     System.out.println(); 
 } 

 public static void main(String[] args) 
 { 
     // Initializing String array. 
     String[] stringArray
         = { "Harit",    "Girish", "Gritav", 
             "Lovenish", "Nikhil", "Harman" }; 

     // sorting String array lexicographically. 
     sortLexicographically(stringArray); 

     String[] st
     = { "Harit",    "Girish", "Gritav", 
         "Lovenish", "Nikhil", "Harman" }; 
     printArray(stringArray); 
     
     Arrays.sort(st);
     System.out.println(Arrays.toString(st)); 
 } 
}