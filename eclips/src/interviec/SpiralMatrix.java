package interviec;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {

	public static void main(String[] args) {
		SpiralMatrix sm = new SpiralMatrix();
		sm.spiral();
		int[][] matrix = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };
		System.out.println(sm.spiral(matrix));

	}

	public List<Integer> spiral(int[][] spi) {
//        int minR = 0, minC = 0,co=1,
//         maxR = spi.length-1, maxC = spi[0].length-1;
//
//		List<Integer> ar = new ArrayList<Integer>();
//
//			while (co <= (maxR * maxC)) {
//
//				for (int c = minC; c <= maxC; c++) {
//					ar.add(spi[minR][c]);
//                    co++;					
//				}
//
//				minR++;
//				
//
//				for (int r = minR; r <= maxR; r++) {
//					ar.add(spi[r][maxC]);
//                    co++;
//				}
//				
//				maxC--;
//
//				for (int c = maxC; c >= minC; c--) {
//					ar.add(spi[maxR][c]);
//                    co++;
//				}
//
//				maxR--;
//
//				for (int r = maxR; r >= minR; r--) {
//					ar.add(spi[r][minC]);
//                    co++;
//				}
//
//				minC++;
//
//			}
//		
//		/***********************************
//		 * printing
//		 ************************************/
//		
//		return ar;
		int minR = 0, minC = 0, maxR = spi.length - 1, maxC = spi[0].length - 1;

		List<Integer> result = new ArrayList<>();

		while (minR <= maxR && minC <= maxC) {

			for (int c = minC; c <= maxC; c++) { // for (int c = minC; c <= maxC; c++)
				result.add(spi[minR][c]);

			}

			minR++;

			for (int r = minR; r <= maxR; r++) {// for (int r = minR; r <= maxR; r++)
				result.add(spi[r][maxC]);
			}

			maxC--;

			if (minR <= maxR) {// extra
				for (int c = maxC; c >= minC; c--) {// for (int c = maxC; c >= minC; c--)
					result.add(spi[maxR][c]);
				}
				maxR--;
			}

			if (minC <= maxC) {// extra
				for (int r = maxR; r >= minR; r--) {// for (int r = maxR; r >= minR; r--)
					result.add(spi[r][minC]);
				}
				minC++;
			}
		}

		return result;
	}

	private void spiral() {
		int co = 1, row = 4, col = 5, inc = 0, minR = 0, minC = 0, maxR = row - 1, maxC = col - 1;

		int[][] spi = new int[row][col];
		List<Integer> ar = new ArrayList<Integer>();

		try {
			while (minR <= maxR && minC <= maxC) { // or we can use (co<(row*col))
				System.out.println(++inc + ") minR: " + minR + " minC: " + minC + " maxR: " + maxR + " maxC: " + maxC);

				for (int c = minC; c <= maxC; c++) {
					spi[minR][c] = co++;
					ar.add(spi[minR][c]);
				}

				minR++;

				System.out.println(++inc + ") minR: " + minR + " minC: " + minC + " maxR: " + maxR + " maxC: " + maxC);

				for (int r = minR; r <= maxR; r++) {
					spi[r][maxC] = co++;
					ar.add(spi[r][maxC]);
				}

				maxC--;
				System.out.println(++inc + ") minR: " + minR + " minC: " + minC + " maxR: " + maxR + " maxC: " + maxC);

				for (int c = maxC; c >= minC; c--) {
					spi[maxR][c] = co++;
					ar.add(spi[maxR][c]);
				}

				maxR--;
				System.out.println(++inc + ") minR: " + minR + " minC: " + minC + " maxR: " + maxR + " maxC: " + maxC);

				for (int r = maxR; r >= minR; r--) {
					spi[r][minC] = co++;
					ar.add(spi[r][minC]);
				}

				minC++;
				System.out.println(++inc + ") minR: " + minR + " minC: " + minC + " maxR: " + maxR + " maxC: " + maxC);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		/***********************************
		 * printing
		 ************************************/
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				if (spi[r][c] < 10)
					System.out.print(" ");
				System.out.print(spi[r][c] + " ");
			}
			System.out.println();
		}

		System.out.println(ar);
	}

}
