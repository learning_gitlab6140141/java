package exception;

public class AccHolder {

	public static void main(String[] args) {
		int balance = 20;

		try {
			if (balance < 30) {
				throw new Recharge();
			} else {
				System.out.println("your balance is " + balance);
			}
		} catch (Recharge r) {
			System.out.println("insufficirnt balance");
		}

	}

}
