package exception;

public class InsufficientBalance extends Exception {
	private static final long serialVersionUID = 1L;
	// a constructor must be use to handle the arguments passed from customeEx..  class
	double amt;
	
	public InsufficientBalance(int amt) {
		this.amt=amt;
	}
	public InsufficientBalance() {
		System.out.println("nothing");
	}
	

}
