package exception;

import java.util.Scanner;

public class ExceDemo {

	public static void main(String[] args) {
		ExceDemo ed = new ExceDemo();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter two digits");
		int no1 = sc.nextInt();
		int no2 = sc.nextInt();
		ed.subtract(no1, no2);
		ed.divide(no1, no2);
		ed.fromUse();

	}

	private void fromUse() {
		Scanner sc = new Scanner(System.in);
		  System.out.println("Enter size :");
		  int size = sc.nextInt();
		  int[] ar = new int[size];
		  for (int i = 0; i < ar.length; i++) {
		   System.out.print("Enter value :");
		   int value = sc.nextInt();
		   ar[i] = value;
		   System.out.println();
		  }

		  for (int marks : ar) {
		   System.out.print(" "+marks);
		  }

	}

	private void subtract(int no1, int no2) {
		System.out.println("sub " + (no1 - no2));

	}

	private void divide(int no1, int no2) {
		try { //try block
		System.out.println("div " + (no1 / no2));
		int[] ar= new int[no1];//NASE
		for (int i=0;i<10;i++) {
			System.out.println(ar[i]);
		}
		} catch(ArithmeticException ae){//ae is the object of this
			ae.printStackTrace();//System.out.println("check no2");
		} catch(NegativeArraySizeException nas) {
			System.out.println("Check no1");
		} catch (Exception e) {
			System.out.println("Something wrong");
		}
		finally {//code cleaning area
			System.out.println("will run always");
		}

	}
		

}
