package net_problem;

import java.util.Arrays;

public class LeftRightDiff {

	public static void main(String[] args) {
		int[] ary = { 10, 4, 8, 3 };
		lrD(ary);
	}

	private static void lrD(int[] nums) {
		
		int[] left = new int[nums.length];
		int[] right = new int[nums.length];
		int[] diff = new int[nums.length];
		for (int i = 1; i < nums.length; i++) {
//			if(nums[i]==nums[0])
				left[0]=0;
//			else
				left[i]=left[i-1]+nums[i-1];
			System.out.println("left "+left[i]);
		}
		
		System.out.println();
		
		for (int i = (nums.length - 2); i >= 0; i--) {
//			if(nums[i]==nums[(nums.length - 1)])
				right[nums.length - 1]=0;
//			else
			right[i]=right[i+1]+nums[i+1];
			
			System.out.println("right "+right[i]);
		}
		
		System.out.println();
		
		for (int i = 0; i < nums.length; i++) {
			System.out.println("\nlef "+left[i]);
			System.out.println("rig "+right[i]);
			diff[i]=Math.abs(left[i]-right[i]);
			
			System.out.println("diff "+diff[i]);
		}
		System.out.println("\n"+Arrays.toString(diff));
	}

}
