package net_problem;

import java.util.HashMap;

public class AlphabetCount {
    public static void main(String[] args) {
        String input = "Hello world";
        HashMap<Character, Integer> alphabetCount = countAlphabets(input);

        System.out.println("Alphabet counts: " + alphabetCount);
    }

    public static HashMap<Character, Integer> countAlphabets(String input) {
        HashMap<Character, Integer> alphabetCount = new HashMap<>();

        for (char ch : input.toCharArray()) {
            if (Character.isLetter(ch)) {
                // Convert character to lowercase for case-insensitivity
                ch = Character.toLowerCase(ch);

                // Update count in the map
                alphabetCount.put(ch, alphabetCount.getOrDefault(ch, 0) + 1);
            }
        }

        return alphabetCount;
    }
}
