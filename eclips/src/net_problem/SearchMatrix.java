package net_problem;

import java.util.Arrays;

public class SearchMatrix {

	public static void main(String[] args) {
		SearchMatrix sm = new SearchMatrix();
//		int[][] matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
//		int target = 5;
//		System.out.println(sm.searchMatrix(matrix, target));

		int[][] mat = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		for (int r = 0; r < mat.length; r++) {
			for (int c = 0; c < mat[r].length; c++) {
				System.out.print(mat[r][c]);
			}
			System.out.println();
		}

		int[][] re = sm.transpose(mat);

		for (int r = 0; r < mat.length; r++) {
			for (int c = 0; c < mat[r].length; c++) {
				System.out.print(re[r][c]);
			}
			System.out.println();
		}

	}

	public int[][] transpose(int[][] matrix) {
//		 int i=0;
//	        for(int r=0;r<matrix.length;r++){
//	            for(int c=0;c<matrix[r].length;c++){
//	                if(r!=c && r>c){
//	                    int temp=matrix[r][c];
//	                    matrix[r][c]=matrix[c][r];
//	                    System.out.print("r : "+r+" c : "+c+" __ "+matrix[r][c]+" & ");
//	                    matrix[c][r]=temp;
//	                    System.out.println(matrix[c][r]+" times :"+i++);
//	                }
//	            }
//	        }
//	        return matrix;

		int[][] tr = new int[matrix[0].length][matrix.length];
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[0].length; c++) {
				tr[c][r] = matrix[r][c];
				System.out.print(tr[c][r]);
			}
			System.out.println();
		}
		return tr;
	}

	public boolean searchMatrix(int[][] matrix, int target) {
		if (matrix == null || matrix.length < 1 || matrix[0].length < 1) {
			return false;
		}
//	        int c = matrix[0].length-1;
//	        int r = 0;
//	        while(c >= 0 && r <= matrix.length-1) {
//	            if(target == matrix[r][c]) {
//	                return true;
//	            } else if(target < matrix[r][c]) {
//	                c--;
//	            } else if(target > matrix[r][c]) {
//	                r++;
//	            }
//	        }
//	        return false;
		int r = 0, c = matrix[0].length - 1;
		while (c >= 0 && r < matrix.length) {
			if (matrix[r][c] == target)
				return true;
			else if (target < matrix[r][c])
				c++;
			else if (target > matrix[r][c])
				r++;
		}

		return false;
	}
}
