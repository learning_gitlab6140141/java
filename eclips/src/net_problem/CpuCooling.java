package net_problem;

import java.util.Arrays;

public class CpuCooling {
	    public int leastInterval(char[] tasks, int n) {
	        int[] freq = new int[26];
	        for (char task : tasks) {
	            freq[task - 'A']++;
	        }
//	        System.out.println(Arrays.toString(freq));
	        Arrays.sort(freq);
	        System.out.println(""+Arrays.toString(freq));
	        int work = freq[25] - 1;
	        int idle = work * n;

	        for (int i = 24; i >= 0; i--) {
	        	System.out.println(i+")idle: "+idle);
	            idle -= Math.min(work, freq[i]);
//	        	 idle -= freq[i];
	            System.out.println(i+")idle: "+idle+" freq: "+freq[i]+" chunk: "+work);
	            if(freq[i]<=0) break;
	        }

	        return idle < 0 ? tasks.length : tasks.length + idle;
	    }
	    
	    public static void main(String[] args) {
	    	char[] tasks = {'A','A','B','B','B','C','C','C','C'};
	    	int n = 1;
	    	
	    	CpuCooling cp=new CpuCooling();
	    	System.out.println(cp.leastInterval(tasks,  n));
		}
	
}
