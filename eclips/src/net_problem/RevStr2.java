package net_problem;

public class RevStr2 {

	public static void main(String[] args) {
		String s = "abcdefg";
		int k = 2;
		System.out.println(reverseStr(s,k));
	}
	
	  public static String reverseStr(String s, int k) {
	        char[] c=s.toCharArray();
	        char[] ns = null;
	        for(int i=0;i<s.length()-k;i+=(k-1)) {
	        	ns[i]=s.charAt(k-1);
	        	ns[k-1]=s.charAt(i);
	        }
	        
	        return String.valueOf(ns);
	    }

}
