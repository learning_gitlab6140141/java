package net_problem;

public class Apr1 {

	public static void main(String[] args) {
		String s = "Hello World  ";
		System.out.println(lengthOfLastWord(s));

	}
	
    public static int lengthOfLastWord(String s) {
    	int count=0;
        boolean entry=false;
        for(int i=s.length()-1;i>=0;i--){
            if(s.charAt(i)!=' ')
            	entry=true;
            if(entry && s.charAt(i)!=' ')
            	count++;
            
            if(count>0 && s.charAt(i)==' ')
            	break;           
        }
        
        return count;
    }

}
