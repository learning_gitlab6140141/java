package net_problem;

import java.util.Arrays;

import array.Array;

public class LngCommonPre {

	public static void main(String[] args) {
		LngCommonPre cp = new LngCommonPre();
		rev(-153);
//		System.out.println(Integer.MIN_VALUE+" & "+Integer.MAX_VALUE);
//		String[] ary= {"flower","flow","flight"};
//		cp.ar(ary);
		System.out.println(rev2(-123));
//		String comm="";
//		System.out.println(ary[0]);
//		String word=ary[0];
//		System.out.println(word);
//		System.out.println(ary[0].charAt(0));

	}

	private static int rev2(int x) {
		int revNum = 0;
		while (x != 0) {
			int lastDigit = x % 10;
			x = x / 10;

			if (revNum > Integer.MAX_VALUE / 10 || (revNum == Integer.MAX_VALUE / 10 && lastDigit > 7)) {
				return 0;
			}
			if (revNum < Integer.MIN_VALUE / 10 || (revNum == Integer.MIN_VALUE / 10 && lastDigit < -8)) {
				return 0;
			}

			revNum = (revNum * 10) + lastDigit;
		}

		return revNum;
	}

	private static void rev(int x) {

		if (x > 0) {
			System.out.println(numRev(x));
		} else {
			x *= -1;
			System.out.println(numRev(x) * -1);
		}

	}

	private static int numRev(int x) {
		int reverse = 0;
		for (; x > 0; x /= 10) {
			if (reverse > Integer.MAX_VALUE / 10 || reverse < Integer.MIN_VALUE / 10)
				return 0;
			reverse = (reverse * 10) + x % 10;
		}
//		System.out.println(reverse);

		return reverse;

	}
/*****************    reverse even short   ***********************	
	public static void main(String[] args) {
		System.out.println(rev(-123));

	}

	private static int rev(int i) {
		int inv=0;
		while(i!=0) {
			inv=(inv*10)+i%10;
			i/=10;
		}
		
		return inv;
	}
	****************   reverse even short   ***********************/	

	private void ar(String[] ary) {
		Arrays.sort(ary);
		String com = "";
		for (int i = 0; i < ary[0].length(); i++) {
			if (ary[0].charAt(i) == ary[(ary.length - 1)].charAt(i))
				com += ary[0].charAt(i);
			else
				break;
		}
		System.out.print(com);
	}

}
