package net_problem;

import java.util.Arrays;

public class MissingNu {

	public static void main(String[] args) {
		int[] n = { 3, 4, -1, 1 };
		MissingNu ms = new MissingNu();
		System.out.println(ms.missingNumber(n));

/********************************************** findDuplicate **********************************************/
		int[] nu = { 1, 3, 4, 3, 2 };
//		System.out.println(ms.findDuplicate(nu));

/********************************************** Reverse String **********************************************/
//		char[] s = {'h','e','l','l','o'};
//		ms.reverseString(s);

	}
	
	
	
	
	
	
	
	

	public int missingNumber(int[] nums) {
		int i = 0;
		Arrays.sort(nums);
		System.out.println(Arrays.toString(nums));
		for (i =0; i < nums.length; i++) {
			if (i != nums[i])
				return i;
		}
		return i;

		/**************** easy missing number leed code ***************/
//		 int n = nums.length;
//	        
//	        int actualSum = ((n)*(n+1))/2;
//
//	        int arrSum =0;
//	        for(int i=0;i<nums.length;i++)
//	        {
//	            arrSum+=nums[i];
//	        }
//
//	        return actualSum-arrSum;

	}
	
	

	public int findDuplicate(int[] nums) {
		int duplicate = 0;
//	        for(int i=0;i<nums.length;i++)
//	            for(int j=i+1;j<nums.length;j++)
//	            if(nums[i]==nums[j])
//	            duplicate=nums[i];
//
//	            return duplicate;  

/********************************************** using sorting method*********************************************/

//		 Arrays.sort(nums);
//		 for(int i=0;i<nums.length-1;i++) {
//			 if(nums[i]==nums[i+1])
//				 duplicate= nums[i];
//		 }
//		return duplicate;
//	    }
		
		
/********************************************** leed code simple*********************************************/
		
		//{ 1, 3, 4, 2, 3 };		
		int mo=1;
		boolean[] arr = new boolean[nums.length];
//		System.out.println(Arrays.toString(arr));
		for (int i : nums) {
			if (arr[i]) {
//				System.out.println("arr-"+i+"__"+arr[i]);
				return i;
				}
			arr[i] = true;
//			System.out.println("arr-"+i+"__"+arr[i]);
//			System.out.println((mo++)+"__"+Arrays.toString(arr));
		}
		return -1;
	}
	
	
    public void reverseString(char[] s) {
        int left=0,right=s.length-1;
        while(left<=right) {
        	char tem=s[left];
        	s[left]=s[right];
        	s[right]=tem;   
        	left++;
        	right--;
        }
        
        System.out.println(s);
        
        
    }

}
