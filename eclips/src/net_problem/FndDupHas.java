package net_problem;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class FndDupHas {

	public static void main(String[] args) {
		int[] nums = { 4, 3, 2, 7, 8, 2, 3, 1 };
		System.out.println(findDuplicates(nums));

	}

	public static List<Integer> findDuplicates(int[] nums) {
		
		List<Integer> nu=new ArrayList<Integer>();
		
		for(int i=0;i<nums.length;i++) {
			boolean ent=false;
			for(int j=i+1;j<nums.length;j++) {
				
				if(nums[i]==nums[j]) {
					ent=true;
					break;
				}				
			}
			if(ent) {
				nu.add(nums[i]);
			}
		}
		
		return nu;

	}

}
