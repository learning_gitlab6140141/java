package net_problem;

public class MaxNoStringPairs {

	public static void main(String[] args) {
		String[] words = { "cd", "ac", "dc", "ca", "zz" };
		MaxNoStringPairs mnp = new MaxNoStringPairs();
		System.out.println(mnp.maximumNumberOfStringPairs(words));

	/******************* Reverse Prefix of Word *******************/
//		String word = "abcdefd";
//		char ch = 'd';
//		System.out.println(mnp.reversePrefix(word, ch));
		
		
  /*************** Number of Strings That Appear as Substrings in Word ***************/
		
		String[] patterns = {"a","abc","bc","d"};
		String word = "abc";
		System.out.println(mnp.numOfStrings(patterns,word));

	}

	public int maximumNumberOfStringPairs(String[] str) {
		int c = 0;
		for (int i = 0; i < str.length; i++) {
			for (int j = i + 1; j < str.length; j++) {
				if (str[i].charAt(0) == str[j].charAt(1) && str[i].charAt(1) == str[j].charAt(0)) {
					c++;
				}
			}
		}
		return c;
	}

//    public int maximumNumberOfStringPairs(String[] words) {
//        int count=0;
//        for(int i=0;i<words.length;i++) {
////        	String rever=rev(words[i]);
//        	for(int j=i+1;j<words.length;j++) {
//        		if(rev(words[i]).equals(words[j]))
//        			count++;
//        	}
//        }
//        
//        return count;
//    }
//
//	private String rev(String s) {
//		String r="";
//		for(int i=s.length()-1;i>=0;i--)
//			r+=s.charAt(i);
//		return r;
//	}

	/******************* Reverse Prefix of Word *******************/
	public String reversePrefix(String word, char ch) {
		String Re = ""; boolean pr=false;
		int i = 0;
		for (; i < word.length(); i++) {
			if (word.charAt(i) == ch) {
				Re = wdIn(word.toCharArray(), 0, i);
				pr=true;
				break;
			}
		}
		
//		if(pr)
//		for (; i < word.length(); i++)
//			Re += word.charAt(i);

		return Re;
	}

	private String wdIn(char ch[], int st, int en) {
		
		while (st < en) {
			char te=ch[st];
			ch[st]=ch[en];
			ch[en]=te;
			st++;en--;
		}
		return new String(ch);

	}
	
	
	/*************** Number of Strings That Appear as Substrings in Word ***************/	
	public int numOfStrings(String[] patterns, String word) {
        int cou=0;
        for(int i=0 ;i < patterns.length; i++){
            for(int j=0;j<patterns[i].length();j++){
                boolean pr=false;
                for(int k=0; k < word.length();k++){
                    if(patterns[i].charAt(j)==word.charAt(k)){
                        cou++;
                        pr=true;
                        break;
                    }
                }
                if(pr) break;
            }
        }

        return cou;        
    }

}
