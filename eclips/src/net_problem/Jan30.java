package net_problem;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ListDataEvent;

public class Jan30 {

	public static void main(String[] args) {
		Jan30 j3=new Jan30();
		/*********************Maximum Value of a String in an Array**********************/
		String[] strs = {"iw1","61939","7","7i","cye","bv7yg","t3ye6","990"};
		//"1","01","001","0001"
		//"alic3","bob","3","4","00000"
		//"iw1","61939","7","7i","cye","bv7yg","t3ye6","990"
//		int cnt=0;
//		System.out.println("trial "+strs[1].charAt(2));
//		for(int i=0;i<strs[1].length();i++) {
//			int n=strs[1].charAt(i)-'0';
//			System.out.println("loop "+strs[1].charAt(i));
//			System.out.println(n);
//			cnt=(cnt*10)+n;
//		}
//		System.out.println(cnt);
		
	
		//System.out.println(j3.maximumValue(strs));
		/********************* Fizz Buzz**********************/
		int n=20;
		System.out.println(j3.fizzBuzz(n));
	}
	
    public int maximumValue(String[] strs) { 
        int count=0,great=0;
        for(int i=0;i<strs.length;i++) {
        	count=0;
        	for (int j=0;j<strs[i].length();j++) {
        		if (strs[i].charAt(j) >= '0' && strs[i].charAt(j) <= '9') {
        		count=(count*10)+(strs[i].charAt(j)-'0') ;// This works because the ASCII value of '0' is subtracted from the ASCII value of the character
        		}
        		else {
        			count=strs[i].length();
    			break;
    			}
        	}
        	if(count>great)
            	great=count;
        }
        
        return great;
    }
//	 public int maximumValue(String[] strs) {
//	        int ans = 0;
//	        for (String s : strs)
//	            ans = Math.max(ans, x(s));
//	        return ans;
//	    }
//
//	    private static int x(String s) {
//	        for (int i = 0; i < s.length(); ++i)
//	            if (s.charAt(i) >= 'a' && s.charAt(i) <= 'z')
//	                return s.length();
//	        return Integer.parseInt(s);
//	    }
    
public List<String> fizzBuzz(int n) {
        List<String> li=new ArrayList<String>();
        for(int i=1;i<=n;i++) {
        	if(i%15==0)
        		li.add("FizzBuzz");
        	else if(i%3==0 )
        	li.add("Fizz");
        	else if(i%5==0 )
        		li.add("Buzz");
        	else {
        		li.add(Integer.toString(i));//("" + i);
        		System.out.println(String.valueOf(i));
        		
        	}
        		
        }
        return li;
    }

}
