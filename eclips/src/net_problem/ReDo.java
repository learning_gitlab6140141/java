package net_problem;

public class ReDo {

	public static void main(String[] args) {
		ReDo rd = new ReDo();

		String[] words = { "abc", "carac", "ada", "racecar", "cool" };
		System.out.println(rd.firstPalindrome(words));
	}

	public String firstPalindrome(String[] words) {
		for (String st : words) {
			String s = st;
			if (revStr(s))
				return s;
		}

		return "";
	}

private boolean revStr(String s) {
    int left=0,right=s.length()-1;
    while(left<=right){
        if(s.charAt(left++)!=s.charAt(right--)) 
        	return false;
        
    }
	return true;
}

}
