package net_problem;

public class Pract {

	public static void main(String[] args) {
		System.out.println(rev(-123));

	}

	private static int rev(int i) {
		int inv=0;
		while(i!=0) {
			inv=(inv*10)+i%10;
			i/=10;
		}
		
		return inv;
	}

}
