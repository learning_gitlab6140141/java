package net_problem;

import java.util.Arrays;

public class DissapearedNumber {

	public static void main(String[] args) {
		int[] nums = { 4, 3, 2, 7, 8, 2, 3, 1 };
		disNum(nums);

	}

	private static void disNum(int[] nums) {
		int n = 1;
		for (int j = 0; j < nums.length ; j++) {
			for (int i = 0; i < nums.length - n; i++) {
				if (nums[i] > nums[i + 1]) {
					int temp = nums[i];
					nums[i] = nums[i + 1];
					nums[i + 1] = temp;
				}

			}
			n++;
		}
		System.out.println(Arrays.toString(nums));

	}

}
