package net_problem;

public class PivotInteger {

	public static void main(String[] args) {
		PivotInteger pi = new PivotInteger();
		int n = 8;
		System.out.println(pi.pivotInteger(n));

	}

	public int pivotInteger(int n) {
//		if (n == 1)
//			return 1;
//		
//		for(int a=2;a<n;a++) {
//			
//			int l=0,r=0;
//			
//			for(int i=1;i<=a;i++)
//				l+=i;
//			for(int i=a;i<=n;i++)
//				r+=i;
//			
//			if(l==r)
//				return a;
//		}
//		
//		
//		return -1;
		
/************************************ 1_MS ************************************/
		
//		int[] prefs = new int[n + 1];
//        for (int i = 1; i < n + 1; i++) {
//            prefs[i] = prefs[i - 1] + i;
//        }
//
//        for (int i = 1; i < n + 1; i++) {	
//            if (prefs[i] == prefs[n] - prefs[i - 1])
//                return i;
//        }
//
//        return -1;
		
/************************************ 0_MS ************************************/
		
        int y = n*(n+1)/2;
        int x = (int)Math.sqrt(y);
        if(x*x==y) return x;
        else return -1;

	}

}
