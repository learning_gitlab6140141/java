package net_problem;

public class CustomSortString {

	public static void main(String[] args) {
		String order = "cba", s = "abcd";
		String pr = customSortString(order, s);
		System.out.println(pr);
	}

	public static String customSortString(String order, String s) {
		String n = "";

		char[] or = order.toCharArray(), co = s.toCharArray();

		for (char c : or) {
			for (char d : co) {
				if (c == d)
					n += c;
			}
		}

		char[] cn = n.toCharArray();
		for (char d : co) {
			boolean te = true;
			for (char c : cn) {
				if (c == d) {
					te = false;
					break;
				}
			}

			if (te) {
				n+=d;
			}
		}

		return n;

	}

}
