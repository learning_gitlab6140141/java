package net_problem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Jan21 {

	public static void main(String[] args) {
		Jan21 ob=new Jan21();
		
/***********************************************House Robber*****************************************/
		int[] nums = {2,1,1,2};
//		System.out.println(ob.houseRobber(nums));
		
		
		int no=3,bi=0;
		for(;no>0;no/=2) {
			int rem=no%2;
			bi=(bi*10) +rem;
		}
		System.out.println(bi);
/***********************************************Counting Bits*****************************************/
//		Scanner sc=new Scanner(System.in);
//		int n=sc.nextInt();
//		System.out.println(ob.countBits(n));
		
/***********************************************compare string*****************************************/
//		String[] word1={"ab", "c"}, word2={"a", "bc"};
//		
//		String a="",b="";
//		for(int i=0;i<word1.length && i<word2.length;i++) {
//			a+=word1;
//			System.out.println(Arrays.toString(a));
//			b+=word2;
//			System.out.println(b);
//		}
//		
//		if(a.equals(b)) 
//			System.out.println(true);
//		else
//			System.out.println(false);
		
		
/***********************************************Set Mismatch*****************************************/	
		int[] num = {1,2,2,4};
		int[] res = ob.setMismatch(num);
		System.out.println(Arrays.toString(res));
	}
	
	
	
	private int[] setMismatch(int[] nums) {
		int dup=-1,rep=-1;
		for(int i=0;i<nums.length;i++) {
			if ((i+1)!=nums[i]) {
				dup=(nums[i]);
				rep=i+1;
			}
//            else{
//                dup=i;
//                rep=i;
//            }
		}
        return new int[] {dup,rep};
	}



	public int[] countBits(int n) {
        int[] ar=new int[n+1];
        for (int i=0;i<=n;i++) {
        	int bi=bin(i);
        	int adBi=ad(bi);
        	ar[i]=adBi;
        }
        System.out.println(Arrays.toString(ar));
        return ar;
    }

	private int ad(int bi) {
		int sum=0;
		for(;bi>0;bi/=10)
			sum+=bi%10;
		return sum;
	}



	private int bin(int n) {
		int sum=0;
		for(;n>0;n/=2) 
			sum=(sum*10)+n%2;
		
		return sum;
	}



	private int houseRobber(int[] nums) {
		int sum=0,rbIn=0,hig=0;
		for(int i=0;i<nums.length;i++) {
			rbIn=i+1;
			sum=nums[i];
			for(int j=(i+1);j<nums.length;j++) 
				if(j!=rbIn) {
					rbIn=j+1;
					sum+=nums[j];
				}
			if(sum>hig)
				hig=sum;
		}
		return hig;
	}
	
	


}
