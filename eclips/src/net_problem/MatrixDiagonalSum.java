package net_problem;

public class MatrixDiagonalSum {

	public static void main(String[] args) {
		int[][] mat = { { 1, 1, 1, 1 }, 
						{ 1, 1, 1, 1 }, 	
						{ 1, 1, 1, 1 },
						{ 1, 1, 1 }};
		System.out.println(diaSum(mat));

	}

	private static int diaSum(int[][] mat) {
		int sum = 0, inc=0,dec=(mat[0].length-1);
//		System.out.println(mat.length+" inner "+mat[0].length);
		for (int i = 0; i < mat.length; i++) {
			if (mat.length == mat[i].length) {
				if(inc!=dec)
					sum+=mat[i][inc]+mat[i][dec];
				else
					sum+=mat[i][inc];
				inc++;
				dec--;
			}
			else
				return 0;
		}
		return sum;
	}

}
