package net_problem;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Feb25 {

	public static void main(String[] args) {
		Feb25 f = new Feb25();
//		String s = "leEeetcodEe";
//		System.out.println("out "+f.makeGood(s));
//		char w='E'+32;
//		System.out.println(w);
//		char com= 'e'-32;
//		System.out.println(com);
		int[] nums1 = { 2, 4 }, nums2 = { 1, 2, 3, 4 };

		int[] ot = f.nextGreaterElement(nums1, nums2);
		System.out.println(Arrays.toString(ot));

	}

	public String makeGood(String s) {

		String good = "";
		int i = 0, len = s.length() - 2;
		System.out.println(len);
		while (i <= len) {
			if (s.charAt(i) == s.charAt(i + 1) + 32 || s.charAt(i) == s.charAt(i + 1) - 32) {
				i++;
			} else {
				good += s.charAt(i);
			}
			System.out.println(good);

			i++;
		}
		// (i<s.length()-1)&& ... "leEeetcodEe".....
		if ((s.charAt(len - 1) != s.charAt(len) + 32 || s.charAt(len) != s.charAt(len - 1) - 32))
			good += s.charAt(s.length() - 1);

		return good;
	}
//
//	public int[] nextGreaterElement(int[] n1, int[] n2) {
//		int[] nx = new int[n1.length];
//
//		for (int i = 0; i < n1.length; i++) {
//			System.out.println("\ni " + i + " " + n1[i]);
//
//			for (int j = 0; j < n2.length; j++) {
//				System.out.println("j " + j + " " + n2[j]);
//
//				if (n1[i] == n2[j]) {
//
//					for (int k = +j; k < n2.length; k++) {
//						System.out.println("k " + k + " " + n2[k]);
//						if (n2[k] > n1[i]) {
//							nx[i] = n2[k];
//							System.out.println("if " + nx[i]);
//							break;
//						}
//					}
//					if(nx[i] == 0) {
//						nx[i] = -1;
//						System.out.println("out ou if " + nx[i]);
//						}
//				}
//
//				if (nx[i] != 0) {
//					System.out.println("j break");
//					break;
//				}
////				else if(nx[i] == 0) {
////				nx[i] = -1;
////				System.out.println("out ou if " + nx[i]);
////				break;}
//
//			}
//		}
//		return nx;
//	}
	

	    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
	        int n1 = nums1.length, n2= nums2.length;
	        int[] ans = new int[n1], index = new int[10001];

	        for(int i=0;i<n2;i++){
	            index[nums2[i]] = i;
	        }

	        for(int i=0;i<n1;i++){
	            ans[i] = nextElement(index[nums1[i]], n2, nums2);
	        }

	        return ans;
	    }

	    private int nextElement(int index, int n2, int[] nums2){
	        int curr= nums2[index];
	        while(index<n2){
	            if(nums2[index] > curr) return nums2[index];
	            index++;
	        }
	        return -1;
	    }

}
