package net_problem;

public class StrRev {

	public static void main(String[] args) {
		String[] words = { "abc", "car", "ada", "racecar", "cool" };

		System.out.println(firstPalindrome(words));

//		String s = "asds7", rev = "";
//
//		for (int i = s.length() - 1; i >= 0; i--) {
//			rev += s.charAt(i);
//		}
//		System.out.println(rev);
//		if(s.equals(rev)) 
//			System.out.println(s+"is pallindrome");
//		else 
//			System.out.println(s+"is not pallindrome");
	}

	public static String firstPalindrome(String[] words) {
//	for (String st : words) {
//		String s = st;
//
////		for (int i = s.length() - 1; i >= 0; i--) {
////			rev += s.charAt(i);
////		}
////		System.out.println(rev);
//		if(s.equals(revStr(s))) 
//			return s;
//	}
//	
//        return "";
//    }
//
//private static Object revStr(String s) {
//	String rev = "";
//
//	for (int i = s.length() - 1; i >= 0; i--) {
//		rev += s.charAt(i);
//	}
//	return rev;
//}
//
//}
		for (int i = 0; i < words.length; i++) {
			if (isPalindrome(words[i])) {
				return words[i];
			}
		}
		return "";
	}

	public static boolean isPalindrome(String str) {
		int left = 0;
		int right = str.length() - 1;
		while (left < right) {
			if (str.charAt(left) != str.charAt(right)) {
				return false;
			}
			left++;
			right--;

		}
		return true;
	}
}
