package net_problem;

import java.util.Arrays;

public class Mar24 {

	public static void main(String[] args) {
		Mar24 m24 = new Mar24();
		int[] nums = { 1, 3, 4, 2, 2 }, no = { 0, 1, 0, 1, 0, 1, 99 }; // {2,2,3,2};
//		System.out.println(m24.findDuplicate(nums));

		System.out.println(m24.singleNumber(no));

	}

	public int singleNumber(int[] no) {
		for (int i = 0; i < no.length; i++) {

			int count = 0;

			for (int j = 0; j < no.length; j++) {

				if (no[i] == no[j]) 
					count++;
				if(count>=2)
					break;
				
			}

			if (count == 1)
				return no[i];
		}
		return -1;
	}

	public int findDuplicate(int[] nums) {
		boolean[] token = new boolean[nums.length];
		for (int n : nums) {
			if (token[n])
				return n;
			token[n] = true;

		}
		return 0;
	}

}
