package net_problem;

import java.util.HashMap;
import java.util.Map;

public class Feb12 {

	public static void main(String[] args) {
		Feb12 ff=new Feb12();
		int[] nums= {3,3,4};
		System.out.println(ff.majorityElement(nums));

	}
    public int majorityElement(int[] nums) {
//        int maj = 0, count = 0, max = 0;
//        for (int i = 0; i < nums.length; i++) {
//        	count = 0;
//            for (int j = 0; j < nums.length; j++) {
//                if (nums[i] == nums[j])
//                    count++;
//            }
//            if (count > max) {
//                maj = nums[i];
//                max = count;
//            }
//        }
//        return maj;
    	Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        for(int i : nums){
            if(map.get(i) == null){
                System.out.println(map.put(i,1));
            }else{
               System.out.println(map.put(i,map.get(i)+1));
            }
            if(map.get(i) > nums.length/2){
                return i;
            }
        }
        return 0;

    }

}
