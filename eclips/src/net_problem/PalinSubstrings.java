package net_problem;

import java.util.Arrays;

public class PalinSubstrings {

	static PalinSubstrings pss = new PalinSubstrings();

	public static void main(String[] args) {
		/*Input: s = "aaa"
Output: 6
Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".*/

		String st = "yetrereteyeyrur";
		int palin=pss.splitSbSt(st);
		System.out.println(palin);
	}
	
	
	
	
	/*class Solution {

    public boolean isPalindrome(String s, int left, int right) {
        while(left < right) {
            if(s.charAt(left++) != s.charAt(right--)) return false;
        } 
        return true;
    }
    
    public int countSubstrings(String s) {
        int ans = 0;
        int n = s.length();
        for(int i=0;i<n;i++) {
            for(int j=i;j<n;j++) {
                if(isPalindrome(s, i, j)) ans++;
            }
        }
        return ans;
    }
}*/

/*************************************copied method************************************/
	private int splitSbSt(String s) {	
		int count=0;
		 int n = s.length();
		for(int i=0;i<n;i++) {
			for(int j=i;j<n;j++) {
				if(isPalindrome(s,i,j))
					count++;				
			}
		}
		return count;
		    }
		
	
    public boolean isPalindrome(String s, int left, int right) {
        while(left<right) {
        	if(s.charAt(left)!=s.charAt(right))
        	return false;
        	left++;right--;
        	
        }
        return true;
    }
}

		
		
		/*****************my method********************
	private int splitSbSt(String st) 
		char[] c = st.toCharArray();
		String VaSt = "";
//		int l = 2;
		int count=0;
//		System.out.println(Arrays.toString(c));
		for (int i = 0; i < c.length; i++) {
			VaSt = "";
			VaSt += c[i];
			for (int j = i + 1; j < c.length; j++) {
				VaSt += c[j];
				System.out.println(VaSt);
//				boolean b=pss.paliMet(VaSt);
				if(paliMet(VaSt))
					count++;
				
			}
		}
		
		return (count+c.length);
	}

	private boolean paliMet(String vaSt) {
		char[] bc = vaSt.toCharArray();
		String ref="";
		for(int i=bc.length-1;i>=0;i--) {
			ref+=bc[i];
		}
		
		if(vaSt.equals(ref))
				return true;
		return false;

	}

}*/



/******************** 2ms method
 * 
 * 
 * 
 * 
 * 
 * 
// class Solution {
//     public int countSubstrings(String s) {
//         int n = s.length();
//         int ans = 0;
//         for (int i = 0; i < n; i++) {
//             ans += countOfPalindrome(i, i, s) + countOfPalindrome(i, i + 1, s);
//         }
//         return ans;
//     }

//     public int countOfPalindrome(int l, int r, String s) {
//         int cnt = 0, n = s.length();
//         while (l >= 0 && r < n && s.charAt(l) == s.charAt(r)) {
//             cnt++;
//             l--;
//             r++;
//         }
//         return cnt;
//     }
// }
// class Solution {
//     public int countSubstrings(String s) {
//         int count = 0;
//         for (int i = 0; i < s.length(); i++) {
//             count += expandAroundCenter(s, i, i);
//             count += expandAroundCenter(s, i, i + 1);
//         }
//         return count;
//     }

//     private int expandAroundCenter(String s, int left, int right) {
//         int count = 0;
//         while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
//             count++;
//             left--;
//             right++;
//         }
//         return count;
//     }
// class Solution {
//     public int countSubstrings(String s) {
//         int strLength = s.length();
//         int count = 0;
//         for (int i = 0; i < strLength; i++) {
//             count += expandAroundCenter(s, i, i) + expandAroundCenter(s, i, i + 1);
//         }
//         return count;
//     }

//     private int expandAroundCenter(String s, int left, int right) {
// 	int strLength = s.length();
//         int count = 0;
//         while (left >= 0 && right < strLength && s.charAt(left) == s.charAt(right)) {
//             count++;
//             left--;
//             right++;
//         }
//         return count;
//     }
// }
// class Solution {
//     public int countSubstrings(String s) {
//         int n = s.length();
//         int ans = 0;
//         for (int i = 0; i < n; i++) {
//             ans += countOfPalindrome(i, i, s) + countOfPalindrome(i, i + 1, s);
//         }
//         return ans;
//     }

//     public int countOfPalindrome(int leftIndex, int rightIndex, String str) {
//         int count = 0;
//         int n = str.length();
//         while (leftIndex >= 0 && rightIndex < n && str.charAt(leftIndex) == str.charAt(rightIndex)) {
//             count++;
//             leftIndex--;
//             rightIndex++;
//         }
//         return count;
//     }
// }
// class Solution {
//     public int countSubstrings(String s) {
        
//         if (s.length() == 1) {
//             return 1;
//         }
//         char[] chars = s.toCharArray();
//         return lookup(chars, 0, 0, 0) + lookup(chars, 0, 1, 1);
//     }

//     private int lookup(char[] chars, int left, int right, int increment) {
//         int answer = 0;
//         for (int i = 0; i < chars.length; ) {
//             if (left >= 0 && right < chars.length && chars[left] == chars[right]) {
//                 answer++;
//                 left--;
//                 right++;
//             } else {
//                 i++;
//                 left = i;
//                 right = i + increment;
//             }
//         }
//         return answer;
//     }
       
// }

// class Solution {
//     public int countSubstrings(String s) {
//         char[] chars = s.toCharArray();
//         int length = chars.length;
//         int currentIndex = 0;
//         int count = 0;

//         while (currentIndex < length) {
//             int start = currentIndex - 1;
//             int end = currentIndex + 1;

//             while (start >= 0 && end < length && chars[start] == chars[end]) {
//                 count++;
//                 start--;
//                 end++;
//             }

//             start = currentIndex;
//             end = currentIndex + 1;

//             while (start >= 0 && end < length && chars[start] == chars[end]) {
//                 count++;
//                 start--;
//                 end++;
//             }

//             count++;
//             currentIndex++;
//         }

//         return count;
//     }
// }

// class Solution {
//     public int countSubstrings(String s) {
//         char[] chars = s.toCharArray();
//         int count = 0;
//         for (int i = 0; i < s.length() * 2; i++) {
//             int left = i / 2;
//             int right = left + i % 2;
//             while (left >= 0 && right < s.length() && chars[left] == chars[right]) {
//                 count++;
//                 left--;
//                 right++;
//             }
//         }
//         return count;
//     }
// }


class Solution {
    int start=0,length=0,maxLength=0,count=0;
    public int countSubstrings(String s) {
         length =s.length();

        for(int i=0;i<length;i++){
            i = countAndReturnNextIndex(s,i);
            // count++;
        }   
        return count;
    }
    public int countAndReturnNextIndex(String s,int k){
        
        int left = k-1,right=k;
        while(right<length -1 && s.charAt(right) == s.charAt(right+1)) right++;
        int countOfSameChar = right-left;
        if(countOfSameChar >=1){
            //5 =5 + 4 +3 + 2 +1
            //4 = 4 +3 + 2 +1
            count+= (countOfSameChar*(countOfSameChar+1))/2 ;
            // System.out.println(left+" "+right+" "+length);
            // System.out.println(countOfSameChar);
        }
        int nextIndex = right++;
        while(left>=0 && right<length && s.charAt(left)==s.charAt(right)){
            left--;
            right++;
            count++;
        }
        return nextIndex;

    }
}*/
