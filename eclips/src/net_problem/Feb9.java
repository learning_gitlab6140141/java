package net_problem;

import java.util.ArrayList;
import java.util.List;

public class Feb9 {

	public static void main(String[] args) {
		Feb9 n = new Feb9();
		int[] nums = { 1, 2, 3 };
		System.out.println(4 % 2);
		System.out.println(n.largestDivisibleSubset(nums));
	}

	public List<Integer> largestDivisibleSubset(int[] nums) {
		List<Integer> adNo = new ArrayList<Integer>();
		if ((nums[0] % nums[1] == 0 || nums[1] % nums[0] == 0) )
			adNo.add(nums[0]);
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] % nums[i - 1] == 0 || nums[i - 1] % nums[i] == 0)
				adNo.add(nums[i]);
		}

		return adNo;

	}

}
