package net_problem;

import java.util.Arrays;

public class ReverseStringII {

	public static void main(String[] args) {
		String s = "abcdefg";
		int n = 2;
//		System.out.println(reverseStr(s,n));
		
	      String name = "Hello world";

          for (int i = 0; i < name.length(); i++) 
          {
              char currentChar = name.charAt(i);
//              System.out.println(i+" i : "+currentChar );
              int count = 0;
              for (int j = 0; j < i; j++) 
              {
                  if (name.charAt(j) == currentChar) {
                	  System.out.println(j+" j : "+name.charAt(j));
                      count++;
                      break; // Exit the inner loop if a duplicate is found
                  }
              }
              if (count == 0) {
                  int totalCount = 0;
                  for (int k = i; k < name.length(); k++) {
                      if (name.charAt(k) == currentChar) {
                          totalCount++;
                      }
                  }
                  System.out.print(""+currentChar + "-" + totalCount + " , ");
              }
          }
		

	}
	//Input: s = "abcdefg", k = 2
//	Output: "bacdfeg"

	 public static String reverseStr(String s, int k) {
		 
		 int no=0;
		 char[] c=s.toCharArray();
         int len  = c.length;
		 for(int i=0;i<len;i=i+(2*k)) {
             
                int j = Math.min(i+k-1, len-1);
				rev(i,j,c);   
                
		 }
			 return new String(c);
    }
    private static void rev(int i, int j, char[] c) {
		while(i<j) {
			char te=c[i];
			 c[i]=c[j];
			 c[j]=te;
			 i++;
			 j--;
		}
		
	}
	 
//	 char[] c=s.toCharArray();
//	 for(int i=0;i<s.length()-1;i++) {
//		 while(no==0){
//			 char te=c[i];
//			 c[i]=c[i+no];
//			 c[i+no]=te;
//			 i+=(no);	
//             no=k-1;
//             break;
//             }	
//             no--;
//	 }
//		 return new String(c);
    
}
