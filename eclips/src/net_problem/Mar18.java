package net_problem;

import java.util.Arrays;
import java.util.Comparator;

public class Mar18 {

	public static void main(String[] args) {
		Mar18 ma = new Mar18();
//		String word1 = "ab", word2 = "pqr";
//		System.out.println(ma.mergeAlternately(word1,word2));

//		int[][] points = {{10,16},{2,8},{1,6},{7,12}};//{{1,2},{3,4},{5,6},{7,8}};
//		System.out.println(ma.findMinArrowShots(points));
		
		
		String num = "51230100";
		System.out.println(ma.removeTrailingZeros(num));

	}
	
	
	 public String removeTrailingZeros(String num) {
//		 double n=Double.parseDouble(num);
//		  int c=0;
//		 while(n%10==0) {
//			 n=n/10;
//			 System.out.println("count "+c+" n "+n);
//		 }
//		 
//		 return n+"";
		 int remove = 0;

	        for(int i = num.length() - 1; i > 0;i--){
	            if(num.charAt(i) == '0'){
	                remove++;
	            }else {
	                break;
	            }
	        }
	        String result = num.substring(0,num.length() - remove);
	        return result; 
	        
	    }

	 
	 //incomplete************************************************************
	public int findMinArrowShots(int[][] points) {
		if (points == null || points.length == 0) return 0;

        // Sort the intervals based on their end points
        Arrays.sort(points, Comparator.comparingInt(a -> a[1]));
        
        for(int i=0; i < points.length; i++) {
        	for(int j=0;j< points[i].length; j++) {
        		System.out.print(points[i][j]+", ");
        	}
        	System.out.println(); 
        }

        int count = 1; // At least one arrow is needed
        int end = points[0][1];

        // Iterate through the sorted intervals
        for (int i = 1; i < points.length; i++) {
            if (points[i][0] > end) {
                // Current balloon doesn't overlap with the previous one
                count++;
                end = points[i][1];
            }
        }

        return count;

	}

	public String mergeAlternately(String word1, String word2) {
//        String mrg= new String();
//        int len=(word1.length()+word2.length())-2;
//        for(int i=0;i<=len;i++) {
//        	if(i<word1.length())
//        		mrg+=word1.charAt(i);
//        	if(i<word2.length())
//        		mrg+=word2.charAt(i);
//        }
//        
//        return mrg;

		/* more faster than above program */
		StringBuilder mrg = new StringBuilder();

		int len = (word1.length() + word2.length()) - 2;
		for (int i = 0; i <= len; i++) {
			if (i < word1.length())
				mrg.append(word1.charAt(i));
			if (i < word2.length())
				mrg.append(word2.charAt(i));
		}

		return mrg.toString();
	}

}
