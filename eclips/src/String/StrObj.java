package String;

public class StrObj {

	public static void main(String[] args) {
		
		String name="eswaramoorthy";
		String name2=new String ("Eswaramoorthy");
		System.out.println("charAt: "+name.charAt(4));
		System.out.println("equals: "+name.equals(name2));
		System.out.println("equalsIgnoreCase: "+name.equalsIgnoreCase(name2));
		System.out.println("compareTo: "+name.compareTo(name2));//name-name2
		System.out.println("compareToIgnoreCase: "+name.compareToIgnoreCase(name2));
		
		name2=name2.concat(" Mari");
		
		System.out.println("after Concat: "+name2);
		System.out.println("contains: "+name2.contains("Mari"));
		System.out.println("contains: "+name2.contains(name));
		System.out.println("hashCode: "+name.hashCode());
		System.out.println("startsWith: "+name.startsWith("e"));
		System.out.println("endsWith: "+name.endsWith("y"));
		String name3="";
		System.out.println("isEmpty: "+name3.isEmpty());
		System.out.println("indexOf: "+name.indexOf('a'));
		System.out.println("lastIndexOf: "+name.lastIndexOf('a'));
		String name4=" seenu ";
		System.out.println("trim===>"+name4.trim()+"<====");
		
	}

}
