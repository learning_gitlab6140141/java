package String;

public class StringDemo {

	public static void main(String[] args) {

		
		StringDemo sd=new StringDemo();
//		sd.obj();
//		sd.name();
//		sd.hex();
		sd.obMet();
		
				
	}

	private void obMet() {
		  String name3 ="monimsh";
		     // 0123456
		  System.out.println(name3.isEmpty());
		  System.out.println(name3.indexOf('m'));
		  System.out.println(name3.lastIndexOf('m'));
		  
		  String name4 = "viGnEsh";
		     //  0123456
		  
		  System.out.println(name4.substring(2));//start @ 2 and continues
		  System.out.println(name4.substring(2,5));//start @ 2 end @<5
		  char[] ar = name4.toCharArray();
		  for(char ch:ar) {
		   System.out.print(ch+" ");
		  }
		  System.out.println("==============");
		  for(int i=ar.length-1;i>=0;i--) {
		   System.out.print(ar[i]+" ");
		  }
		  System.out.println("==============");
		  System.out.println("==============");
		  
		  System.out.println(name4.toLowerCase());
		  System.out.println(name4.toUpperCase());
		  
		  String name5 ="   12/ 12/2023 iyhy nnu  ";
		  System.out.println("==>"+name5.trim()+"<== trim");
		  System.out.println("==>"+name5.strip()+"<== strip");
		  System.out.println("==>"+name5.stripLeading()+"<== stripLeading");
		  System.out.println("==>"+name5.stripTrailing()+"<== stripTrailing");
		  System.out.println('\u0B85');
		  String[] str = name5.split("/");
		  
		  for (String string : str) {
		   System.out.println(string);
		  }
		  
 String[] str2 = name5.split("/",2);
 System.out.println("second str2");
		  
		  for (String string : str2) {
		   System.out.println(string);
		  }
		  
		  
		  
		  String date = String.join("-", "12","12","2023","dfadsj");
		  System.out.println(date);
		  
		  String dat = String.join("-", str);
		  System.out.println("str "+dat);
		
	}

	private void hex() {
				
	}

	private void obj() {
		String s="abc";
		System.out.println(System.identityHashCode(s));
		String p1="Aus";
		System.out.println("p1 aus: "+System.identityHashCode(p1));
		String p2="India";
		System.out.println("p2 India: "+System.identityHashCode(p2));
		String p3="India";
		System.out.println("p3 India: "+System.identityHashCode(p3));
		String p4="India";
		System.out.println("p4 India: "+System.identityHashCode(p4));
		String p5="India";
		System.out.println("p5 India: "+System.identityHashCode(p5));
		
		String p6=new String("India");
		System.out.println("p6 ind "+System.identityHashCode(p6));
		p6="USA";
		System.out.println("p6 usa "+System.identityHashCode(p6));
		p6="India";
		System.out.println("p6 usa "+System.identityHashCode(p6));
		
		String p7=new String("India");
		System.out.println("p7 "+System.identityHashCode(p7));
		
		System.out.println();
		
	}

	private void name() {
		System.out.println("name");
		String name="Sarathkumar";
		System.out.println(name.length());
		int time=0;
		
		for(int i=0;i<name.length();i++) {
//			System.out.print(name.charAt(i));
			
			if('a'==name.charAt(i)) {
				System.out.print(name.charAt(i)+" ");
				time ++;
			}			
		}
		System.out.println();
		System.out.print("no of a "+time);
		
	}
}