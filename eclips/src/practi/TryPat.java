package practi;

public class TryPat {

	public static void main(String[] args) {
		TryPat tp=new TryPat();
//		int n=5;
//		tp.fir(n);
//		tp.sec(n);
		/**************************diamond try*****************/
//	      int n = 4;  // Adjust the value of 'n' based on the pattern size
//
//	        int[][] pattern = new int[n][n];
//
//	        // Fill the pattern array
//	        int count = 1;
//	        for (int i = 0; i < n; i++) {
//	            for (int j = i; j < n - i; j++) {
//	                pattern[j][i] = count++;
//	            }
//	        }
//
//	        // Print the pattern
//	        for (int i = 0; i < n; i++) {
//	            for (int j = 0; j < n; j++) {
//	                if (pattern[i][j] != 0) {
//	                    System.out.printf("%-4d", pattern[i][j]);
//	                } else {
//	                    System.out.print("    ");
//	                }
//	            }
//	            System.out.println();
//	        }
		tp.sleepPat();

	}

	private void sleepPat() {
		int n=4,nu=1;
		for (int i=1;i<=n;i++) {
			for(int j=1;j<i;j++) {
				System.out.print("    ");
			}
			for(int j=0;j<n;j++) {
				System.out.print(nu+++"   ");
			}
			System.out.println();
		}
		
	}

	private void sec(int n) {
		for(int i=1;i<=n;i++) {
			for(int j=1;j<i;j++) {
				System.out.print("* ");
			}
			for(int j=n;j>=i;j--) {
				System.out.print(i+" ");
			}
			System.out.println();
		}
		
	}

	private void fir(int n) {
		for(int i=1;i<=n;i++) {
			for(int j=n;j>i;j--) {
				System.out.print("  ");
			}
			for(int j=1;j<=i;j++) {
				if(i%2==0)
					System.out.print("# ");
				else
				System.out.print(i+" ");
				}
			System.out.println();
		}
		
	}

}
