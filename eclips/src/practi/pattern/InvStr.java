package practi.pattern;

import java.util.Scanner;

public class InvStr {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("enter no :");
		int n = sc.nextInt();
		for (int row = 0; row < n; row++) {
			for (int col = n - 1; col > row; col--)
				System.out.print("  ");
			for (int col = 0; col <= row; col++)
				System.out.print(" " + (row + 1));
			for (int col = 1; col <= row; col++)
				System.out.print(" " + (row + 1));
			System.out.println();
		}
		for (int row = n; row >0; row--) {
			for (int col =row; col < n - 1; col++)
				System.out.print(" *");
			System.out.println();
		}

	}

}
