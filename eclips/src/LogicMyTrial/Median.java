package LogicMyTrial;

import java.util.Arrays;

public class Median {

	public static void main(String[] args) {
		int[] a={1,7,4};
		int[] b= {4,5,6};
		int[] ar=new int[a.length+b.length];
		
		for(int i=0;i<a.length;i++)
        {
            ar[i]=a[i];
        }
        for(int i=0;i<b.length;i++)
        {
            ar[i+a.length]=b[i];
        }
        
		Arrays.sort(ar);
				
//		System.out.println(ar);
		
		int n=ar.length;
		float median=0 ;
		if(n%2 != 0) {
			median=ar[(n/2)];
		}
		else {
			median=(float)(ar[(n-1)/2]+ar[n/2])/2;
		}
		
		System.out.println(median);

	}

}
