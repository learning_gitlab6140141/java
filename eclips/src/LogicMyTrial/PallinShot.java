package LogicMyTrial;

import java.util.Scanner;

public class PallinShot {

	public static void main(String[] args) {

		PallinShot ps=new PallinShot();
		System.out.println("give a number to check palindrom : ");
		Scanner sc = new Scanner(System.in);
		int no =sc.nextInt();
		System.out.println(ps.isPalindrome(no));
		
	}

	private boolean isPalindrome(int x) {
		if((x!=0&&x%10==0))
		return false;
		
		int reverse=0;
//		int original=x;
		while(x>reverse) {
			reverse = reverse*10 + x%10;
			x/=10;
		}
		
		return (x == reverse)||(x==reverse/10);
	}

}
