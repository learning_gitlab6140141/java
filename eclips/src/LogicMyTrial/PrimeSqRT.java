package LogicMyTrial;

import java.util.Scanner;

public class PrimeSqRT {
	
 static PrimeSqRT pe=new PrimeSqRT();
 
 
	public static void main(String[] args) {
		System.out.println("enter a number to find prime or not : ");
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
//		pe.squRt(num);
		pe.prime(num);
	}

	public boolean prime(int num) {
		int div=2;
		boolean prime=true;
		while(div< pe.squRt(num)) {
			if (num%div==0) {
				System.out.println(num+" is not a prime num");
				prime=false;
				break;
			}
			div++;
		}
		if (prime==true)
		System.out.println(num+" is a prime num");
		return prime;
	}

	private int squRt(int num) {
		int sqRt=2;
		while(sqRt<num ) {
			if(num/sqRt==sqRt) {
//				System.out.println(sqRt+" is the squire root");
				break;
			}
			sqRt++;
		}
		return sqRt;
	}

}
