package inheritance_08.salem;
public class Youngstar_2 extends Parent //this change this object to access parent class
{
	public static void main (String[] args)
	{
		Youngstar yy=new Youngstar();
		
		yy.fixMarage();
		yy.meetRelatives();
			
	}	
	public void fixMarage()//"method overriding" i.e., parent has differet method but children uses its method
	{
		
		System.out.println(" child rules for marriage");
		super.fixMarage();
	}
	
	public void study()
	{
		System.out.println("Study");
	}
}