package inheritance_08.salem;
public class GrandParent
{
	public void takeRest()
	{
		System.out.println("\nGrandParent-takeRest");
	}
	public void visit()
	{
		System.out.println("\nGrandParent-visit");
	}
}