package inheritance_08.salem;
public class Youngstar extends Parent //this change this object to access parent class

{
	static int balance=50000;
	
	public static void main (String[] args)
	{
		Youngstar yy=new Youngstar();
		//yy.study();
		//yy.gardening();
		yy.fixMarage();
		yy.meetRelatives();
		//System.out.println("\ncalling the variable using child class : "+Youngstar.balance);//private access in Parent cant be accessed in child class
		//System.out.println("calling the variable using parent class : "+Parent.balance);
		//System.out.println("calling the variable using child object : "+yy.balance); 
		
		yy.takeRest();
		yy.visit();
		
		//Parent pp=new Parent();//extra space created , not needed, wrong method calling
		//pp.fixMarage();
		
	}	
	public void fixMarage()//"method overriding" i.e., parent has differet method but children uses its method
	{
		System.out.println("super."+super.balance);
		System.out.println("this."+this.balance);
		super.fixMarage(); //yy.fixMarage();
		System.out.println(" child rules for marriage");
	}
	public void study()
	{
		System.out.println("Study");
	}
}