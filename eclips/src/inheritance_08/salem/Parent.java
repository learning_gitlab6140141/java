package inheritance_08.salem;
public class Parent extends GrandParent
{
	static int balance=10000;
	
	private void savings()// private method also cant be accessed in child class
	{
		System.out.println("savings");
	}
	public void fixMarage()
	{
		System.out.println("\nparents rules for marriage");
	}
	public void gardening()
	{
		System.out.println("gardening");
	}
	public void takeRest()
	{
		System.out.println("\nParent-takeRest");
	}
	protected void meetRelatives()
	{
	System.out.println("meetRelatives");
	}
}