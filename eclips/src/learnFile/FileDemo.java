package learnFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileDemo {

	public static void main(String[] args) throws Exception  {

		FileDemo fd = new FileDemo();
		fd.meth();
//		fd.fileFind();
//		fd.writ();
//		fd.bufferWrit();
		

		
		
	}

	private void bufferWrit() throws IOException {
		File note = new File("/home/eswar/Documents/it job/FileWrite.pdf");
		note.createNewFile();

		FileWriter pen = new FileWriter(note, true);
		BufferedWriter bw = new BufferedWriter (pen);
		
		bw.write("aaaaaaa");
		bw.write("bbbbbbb");
		bw.flush();
		bw.close();
		
		
		FileReader reader = new FileReader(note);
		BufferedReader br= new BufferedReader(reader);
		String st = br.readLine();
		int count=0;
		while (st != null) {
			count++;
			System.out.print(st);
			st=br.readLine();
		}
		System.out.println("\n"+count);
		
	}

	private void fileFind() {
		File ff = new File("/home/eswar/Documents/trial_for_java-11");
		File[] files = ff.listFiles();
		System.out.println("length ==> "+files.length);
		for (int i = 0; i < files.length; i++) {
			String name = files[i].getName();
			int dot = name.lastIndexOf(".");
			if (dot > 0) {
				String extension = name.substring(dot);
				if (extension.equals(".java"))
					System.out.println(files[i].getName());
			}
		}
	}

	private void writ() throws Exception  {
		File note = new File("/home/eswar/Documents/it job/FileWrite.txt");
		note.createNewFile();

		FileWriter pen = new FileWriter(note, true); // append
		pen.write('1');
		pen.write('5');
		pen.write('3');
		pen.write('7');
		pen.flush();
		pen.close();

		FileReader reader = new FileReader(note);
		int r = reader.read();
		while (r != -1) {
			System.out.print((char) r);
			r = reader.read();
		}

	}

	private void meth() throws IOException {
		File ff = new File("/home/eswar/Documents/it job/FileMets.txt");
		System.out.println("createNewFile: " + ff.createNewFile());
		System.out.println("canRead: " + ff.canRead());
		System.out.println("canWrite: " + ff.canWrite());
		System.out.println("getPath: " + ff.getPath());
		System.out.println("isFile: " + ff.isFile());
		System.out.println("isDirectory: " + ff.isDirectory());
		System.out.println(ff.isHidden());
		System.out.println(ff.delete());

	}

}
