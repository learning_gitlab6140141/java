package consoleProj.Pizza;

public class PizzaPrice {
	private int price;
	private boolean veg;
	
	public PizzaPrice(boolean veg) {
		this.veg=veg;
		if (this.veg)
			this.price=300;
		else
			this.price=400;
	}
	public void getPizzaPrice() {
		System.out.println(this.price);
	}

}
