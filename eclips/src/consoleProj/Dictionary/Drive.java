package consoleProj.Dictionary;

import java.util.HashMap;
import java.util.Scanner;
import java.io.*;

class Words {
	// global variable in another class
	public String wordName, meaning;

	// constructor - single and parameterized accessed using getters and setters
	Words(String wordName, String meaning) {
		this.wordName = wordName;
		this.meaning = meaning;
	}

}




//Create dictionary class - which has 'HASMAP' (key value:---[key-String , value Words] ) public method 
class Dictionary {
	public HashMap<String, Words> map = new HashMap<String, Words>();

	// loadRecords():- this method is to load the files from .txt file
	public void loadRecords() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader("/home/eswar/Documents/git/java/dictionary.txt"));//---/home/eswar/Documents/git/java/dictionary.txt
		
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			String[] parsedLine = line.split(",");
			String wordName = parsedLine[0],meaning = parsedLine[1];
			Words words = new Words(wordName, meaning);
			this.map.put(wordName, words);
		}
		
		br.close();
	}

	// method to put them inside the hash map
	public String findWord(String word) {
		
		// i will go through the keys i.e., HashMap<String, Words> -  String is the key - Words is object
		for (String i : map.keySet()) {
			if (i.equals(word)) {
				Words newWord = map.get(i);
				return newWord.meaning;
			}
		}
		return null;
	}
	
	
//to add new words to the dictionary	
	public void saveRecords() throws Exception{
		BufferedWriter bw= new BufferedWriter(new FileWriter("/home/eswar/Documents/git/java/dictionary.txt"));
		for(String i:map.keySet()) {
			Words w=map.get(i);
//			bw.write("\n");
			bw.write(w.wordName);
			bw.write(",");
			bw.write(w.meaning);
			bw.write("\n");
		}
		bw.flush();
		bw.close();
	}

}




public class Drive {

	public static void main(String[] args) throws Exception {
		System.out.println("enter a word to find");
		Scanner sc=new Scanner(System.in);
		String wrd=sc.next();
		Dictionary obj = new Dictionary();
		obj.loadRecords();
		System.out.println(obj.findWord(wrd));		
		obj.saveRecords();

	}

}
