package consoleProj;

import java.util.Scanner;

public class CreditCardVali {

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.println("enter your credit card number");
		String no=sc.next();
		int length=no.length();
//		System.out.println(length);
//		System.out.println(no);
		if (length==15) {
			if(no.startsWith("34")||no.startsWith("37")) {
				System.out.println("American Express");
			}else if(no.startsWith("2123")||no.startsWith("1800")) {
				System.out.println("JDC");
			}
		}
		else if (length==14){
			if(no.startsWith("36") ||
					 no.startsWith("38")  ||
					 no.startsWith("300") ||
					 no.startsWith("301") ||
					 no.startsWith("302") ||
					 no.startsWith("303") ||
					 no.startsWith("304") ||
					 no.startsWith("305") ) 
				System.out.println("Diners Club");
			
		}
		else if ((length==13)||(length==16)){
			if(no.startsWith("4")) 
				System.out.println("Visa Card");
			else if(no.startsWith("3")) 
				System.out.println("JDC");
			else if(no.startsWith("60")) 
				System.out.println("Platinum Card");
			else if(no.startsWith("51") ||
					 no.startsWith("52") ||
					 no.startsWith("53") ||
					 no.startsWith("54") ||
					 no.startsWith("55") ) 
				System.out.println("Master Card");
			else if(no.startsWith("6011")) 
				System.out.println("Discover Card");
			
		}
		else {
			System.out.println("Invalid Credit/Debit card number");
		}
		
		char[] ch=no.toCharArray();
		int[] eve=new int[ch.length/2];
		int[] odd=new int[eve.length];
		int j=0,k=0;
		for(int i=0;i<ch.length;i+=2) {
			eve[j]=(ch[i]-48)*2;
			j++;
		}
		for(int i=1;i<=ch.length;i+=2) {
			odd[k]=(ch[i]-48);
			k++;
		}
		int eveSum=0,oddSum=0;
		for(int i=0;i<eve.length;i++) 
			eveSum+=(eve[i]%10)+(eve[i]/10);
		for(int i=0;i<odd.length;i++) 
			oddSum+=odd[i];

		if ((eveSum+oddSum)%10==0)
			System.out.println(" valid Credit/Debit card number");
		else
			System.out.println("Invalid Credit/Debit card number");
	}

}
