package consoleProj.BusReservation;
import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;  


public class Booking {
	String passengerName;
	int BusNo;
	Date date;
	
	Booking(){
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter name of passenger");
		passengerName=scan.next();
		System.out.println("Enter bus number");
		BusNo=scan.nextInt();
		System.out.println("Enter date dd-mm-yyyy");

		String dateInput=scan.next();//will return date in string which Date will not accept  so create string object

		SimpleDateFormat dateFormet=new SimpleDateFormat("dd-MM-yyyy");//import method
		try {
			date = dateFormet.parse(dateInput);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

	public boolean isAvailable(ArrayList<Booking> bookings, ArrayList<Bus> buses) {
		int capacity = 0;
		for(Bus bus:buses) {
			if (bus.getBusNo()==BusNo)
				capacity =bus.getCapacity();
		}
		int booked=0;
		for (Booking b:bookings) {
			if(b.BusNo == BusNo && b.date.equals(date)) {
				booked++;
			}
		}
		return booked<capacity?true:false;
	}
	
}
