package consoleProj.BusReservation;

public class Bus {
	
	private int busNo;
	private boolean ac;
	private int capacity;
	
	public Bus(int no, boolean ac, int cap) {
		this.busNo=no;
		this.ac=ac;
		this.capacity=cap;
	}

	public int getCapacity() { //accessor method
		return capacity;
	}

	public void setCapacity(int capacity) { //mutator method
		this.capacity = capacity;
	}

	public boolean isAc() {
		return ac;
	}

	public void setAc(boolean ac) {
		this.ac = ac;
	}

	public int getBusNo() {
		return busNo;
	}

		public void displayBusInfo() {
			System.out.println("Bus No: "+busNo+" AC: "+ac+" Total Capacity : "+capacity);
		}
}
