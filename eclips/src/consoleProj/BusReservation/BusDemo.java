package consoleProj.BusReservation;

import java.util.ArrayList;
import java.util.Scanner;

public class BusDemo {

	public static void main(String[] args) {
//		Bus bus1=new Bus(1,true,45);//in order to replace this bus array is created
//		Bus[] buses=new Bus[50];//less efficient as it has default memory so use array list
//		ArrayList<Integer> nums=new ArrayList<Integer>();//similar to this

		ArrayList<Bus> buses = new ArrayList<Bus>(); // can create many bus object using this
		ArrayList<Booking> bookings = new ArrayList<Booking>();

		buses.add(new Bus(1, true, 2));
		buses.add(new Bus(2, false, 5));
		buses.add(new Bus(3, true, 6));
		buses.add(new Bus(4, false, 60));

		int userOpt = 1;
		Scanner scan = new Scanner(System.in);

		for (Bus b : buses) {
			b.displayBusInfo();
		}

		while (userOpt == 1) {
			System.out.println("Enter 1 to Book and 2 to Exit");
			userOpt = scan.nextInt();
			if (userOpt == 1) {
				Booking booking = new Booking();
				if (booking.isAvailable(bookings, buses)) {
					bookings.add(booking);
					System.out.println("your booking is confirmed");
				} else
					System.out.println("Sorry bus isfull try another bus or date");
			} else if (userOpt != 2) {
				System.out.println("Enter valid input");
				userOpt = 1;
			}
		}

	}

}
