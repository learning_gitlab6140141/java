package consoleProj.library;

public class NormalUser extends User {

	public NormalUser(String name) {
		super(name);
	}

	public NormalUser(String name, String email, String phone_number) {
		super(name, email, phone_number);
	}

}
