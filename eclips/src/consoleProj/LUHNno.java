package consoleProj;

import java.util.Arrays;
import java.util.Scanner;

public class LUHNno {

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.println("enter your credit card number");
		String no=sc.next();
		int length=no.length();
//		System.out.println(length);
//		System.out.println(no);
		if (length==15) {
			if(no.startsWith("34")||no.startsWith("37")) {
				System.out.println("American Express");
			}else if(no.startsWith("2123")||no.startsWith("1800")) {
				System.out.println("JDC");
			}
		}
		else if (length==14){
			if(no.startsWith("36") ||
					 no.startsWith("38")  ||
					 no.startsWith("300") ||
					 no.startsWith("301") ||
					 no.startsWith("302") ||
					 no.startsWith("303") ||
					 no.startsWith("304") ||
					 no.startsWith("305") ) 
				System.out.println("Diners Club");
			
		}
		else if ((length==13)||(length==16)){
			if(no.startsWith("4")) 
				System.out.println("Visa Card");
			else if(no.startsWith("3")) 
				System.out.println("JDC");
			else if(no.startsWith("60")) 
				System.out.println("Platinum Card");
			else if(no.startsWith("51") ||
					 no.startsWith("52") ||
					 no.startsWith("53") ||
					 no.startsWith("54") ||
					 no.startsWith("55") ) 
				System.out.println("Master Card");
			else if(no.startsWith("6011")) 
				System.out.println("Discover Card");
			
		}
		else {
			System.out.println("Invalid Credit/Debit card number");
		}
		
		char[] ch=no.toCharArray();
		System.out.println(ch);
		if (luhn(ch) % 10 == 0)
			System.out.println("Valid Credit/Debit card number");
		else
			System.out.println("Invalid Credit/Debit card number");

	}

	private static int luhn(char[] ar) {
		int eveSum = 0, oddSum = 0;
		for (int i = 0; i < ar.length; i++) {
			if (i % 2 == 0)
				eveSum += eve(ar[i]-48);
			else
				oddSum += ar[i]-48;

		}
//		System.out.println(eveSum + oddSum);
		return (eveSum + oddSum);
	}

	private static int eve(int i) {
		int temp = i * 2, sum = 0;

		if (temp <= 9) {
//			System.out.println(temp);
			return temp;
		} 
			for (int eve = temp; eve > 0; eve /= 10) {
				sum += eve % 10;
			}
	
//		System.out.println(sum);
		return sum;
	}

}
