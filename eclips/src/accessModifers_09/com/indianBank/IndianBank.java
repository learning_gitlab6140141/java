package accessModifers_09.com.indianBank;
import accessModifers_09.com.indianBank.loan.InterestCalculation;
public class IndianBank
{
	
public static void main(String[] args)
{
  InterestCalculation ic = new InterestCalculation(8,"HDFC");
  ic.calculate();
  //ic.salary();
  IndianBank ib = new IndianBank();
  ib.salary();
  ib.calculate();
}
private void calculate() {
	System.out.println("calculate method in IndianBank ");
	
}
public void salary()
{
  System.out.println("salary method in IndianBank ");
}
}