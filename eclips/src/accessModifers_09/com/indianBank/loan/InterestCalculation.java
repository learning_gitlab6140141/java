package accessModifers_09.com.indianBank.loan;

public class InterestCalculation {
	public int i;
	public String b;

	public InterestCalculation(int i, String b) {
		this.i = i;
		this.b = b;
		System.out.println("InterestCalculation-CONST");
	}

	public static void main(String[] args) {
		// private int mb = 500;
		// System.out.println("Indian Bank");
		// System.out.println(10);
		InterestCalculation ic = new InterestCalculation(5, "IOB");
		ic.calculate();
		// System.out.println(ic.i);
	}

	public void calculate() {
		System.out.println("Interest-calculate");
		System.out.println(this.i);
		System.out.println(i);
	}

}