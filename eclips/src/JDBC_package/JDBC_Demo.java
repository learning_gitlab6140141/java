package JDBC_package;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.postgresql.shaded.com.ongres.scram.common.stringprep.StringPreparation;

public class JDBC_Demo {

	public static void main(String[] args) throws SQLException {
		JDBC_Demo jd=new JDBC_Demo();
		jd.select();
//		jd.create();
//		jd.update();
		jd.del();

	}

	private void del() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String dltQuery="delete from employee where id= ?";//two question mark
		
		Connection con= DriverManager.getConnection(url, userName, password);
		PreparedStatement pst=con.prepareStatement(dltQuery);
		pst.setInt(1, 4);
		int change= pst.executeUpdate();
		System.out.println(change+" no of row affaeced");
		con.close();
		
	}

	private void update() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String upQuery="update employee set salary = ? where id= ?";//two question mark
		
		Connection con= DriverManager.getConnection(url, userName, password);
		PreparedStatement pst=con.prepareStatement(upQuery);
		pst.setInt(1, 50000);
		pst.setInt(2, 4);// in top it has 2nd q mark so here 2 
		int change= pst.executeUpdate();
		System.out.println(change+" no of row affaeced");
		con.close();		
	}

	private void create() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String inQuery="insert into employee values(?,?,?)";
		
		Connection con= DriverManager.getConnection(url, userName, password);
		PreparedStatement pst=con.prepareStatement(inQuery);
		pst.setInt(1, 4);
		pst.setString(2, "manoj");
		pst.setInt(3, 45000);
		int change= pst.executeUpdate();
		System.out.println(change+" no of row affaeced");
		con.close();
		
	}

	private void select() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String query="select * from employee";
		
		Connection con= DriverManager.getConnection(url, userName, password);
		Statement st= con.createStatement();
		ResultSet rs=st.executeQuery(query);
	
		
		while(rs.next()) {
			System.out.println(rs.getInt(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getInt(3)+"\n");
		}
		con.close();
	}

}
