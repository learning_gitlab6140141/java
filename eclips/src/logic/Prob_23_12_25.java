package logic;

import java.util.Scanner;

public class Prob_23_12_25 {
	Scanner BasePowe=new Scanner(System.in);
	
	public static void main(String[] args) {
		Prob_23_12_25 prog=new Prob_23_12_25();
//		prog.powers();
//		prog.numberPower();
//		prog.numberPowerReve();
		prog.numberPower_OddEve();

	}
	
	private void numberPower_OddEve() {
		int no=1;
		//getting input
//		Scanner BasePowe=new Scanner(System.in);
		System.out.println("enter base and power number ");
		int value= BasePowe.nextInt();
		
		//programming		
		while(no<=value) {
			int base=no,power=no,result=1;
			while(power>0) {
				result*=base;
//				System.out.print(result+" ");
				power--;
			}
			if(result%2!=0) {		
			System.out.println("odd base and power value "+base);
			System.out.print(result+" ");
			System.out.println();}
			
			else if(result%2==0) {		
				System.out.println("Even base and power value "+base);
				System.out.print(result+" ");
				System.out.println();}
			
			no++;
		}
		
	}

	private void numberPowerReve() {
		System.out.println("enter the base and power number for reverse");
		int value= BasePowe.nextInt();
		while (value>=1) {
			int base=value,power=value,result=1;
			System.out.println("base "+base+" and power value is "+power);
			while(power>=1) {
				result*=base;
				power--;
			}
			System.out.println(result);
			value--;
		}
		
	}

	private void numberPower() {
		int no=1;
		//getting input
//		Scanner BasePowe=new Scanner(System.in);
		System.out.println("enter the base and power number");
		int value= BasePowe.nextInt();
		
		//programming		
		while(no<=value) {
			int base=no,power=no,result=1;
			System.out.println("base "+base+" and power value is "+power);
			while(power>0) {
				result*=base;
//				System.out.print(result+" ");
				power--;
			}
			System.out.print(result+" ");
			System.out.println();
			no++;
		}
	}

	private void powers() {
		int base=2,power=5,result=1;
		while(power>0) {
			result=result*base;
			System.out.println(result);
			power--;
		}
		
	}

}