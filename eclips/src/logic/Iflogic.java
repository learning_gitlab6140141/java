package logic;

public class Iflogic {
	int no;

	public static void main(String[] args) {
		Iflogic log = new Iflogic();
//		log.method_1();
//		log.method_2();
//		log.method_3();
//		log.schem_1();// check
		log.paddy();
		log.dadDaugh();

	}

	private void dadDaugh() {
		System.out.println("dad and dauter");
		int day=0,amt=0;
		while(day<=5) {
			amt=amt+day;
			day++;			
		}
		System.out.println(amt);
		
	}

	private void paddy() {
		int day=1,paddy=1;
		while (day<=10) {
			paddy *=2;
			day++;
		}
		System.out.println(paddy);
		
	}

	private void schem_1() {
		int count=0,ft=30,up=2,down=1;
		while(ft>0){
			ft=ft-up+down;
			count++;
			System.out.println(count);
		}
		
	}

	private void method_1() {
		no = 1;
		System.out.println("1st method");
		while (no <= 5) {
			
			System.out.print(no*2 + " ");
			no++;
		}
		System.out.println("");

	}

	private void method_2() {
		no = 1;
		System.out.println("2nd method");
		while (no <= 10) {
			no++;
			System.out.print(no + " ");
			no++;
		}
		System.out.println("");
	}

	private void method_3() {
		no = 1;
		System.out.println("3rd method");
		while (no <= 10) {
			if (no % 2 == 0) {
				System.out.print(no + " ");
			}
			no++;
		}
		System.out.println("");

	}

}
