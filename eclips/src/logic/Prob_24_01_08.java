package logic;

public class Prob_24_01_08 {

	public static void main(String[] args) {
//		Prob_24_01_08 arr = new Prob_24_01_08();
		EBbill();

	}

	private static void EBbill() {
		String[] month = { "jan", "feb", "mar", "apr", "may", "june" };
		int[] bill = {240, -230, 450, 375, 400, 300 };

		int big = bill[0], count = 0;

		for (int i = 0; i < bill.length; i++)
			if (bill[i] > big)
				big = bill[i];
		System.out.println("big :" + big);

		for (int i = 0; i < bill.length; i++)
			if (bill[i] < big)
				big = bill[i];

		System.out.println("small : " + big);

		for (int i = 0; i < bill.length; i++)
			if (bill[i] == 450) {
				count++;
				System.out.println("monts : " + month[i]);
			}
		System.out.println("count : " + count);

		twoBig(bill);
		twoSmall(bill);

	}

	private static void twoBig(int[] bill) {
		int first = 0, second = 0;
		for (int i = 0; i < bill.length; i++) {
			if (first < bill[i]) {
				second = first;
				first = bill[i];
			} else if (second < bill[i] && second!=first)
				second = bill[i];
		}
		
		System.out.println("first Big : "+first+" second Big :"+second);

	}

	private static void twoSmall(int[] bill) {
//		{ 240, 230, 450, 375, 400, 300 }
			int first = bill[0], second = bill[1];
			for (int i = 1; i < bill.length; i++) {
				if (first > bill[i]) {
					second = first;
					first = bill[i];
				} else if (second >= bill[i] && second>first)
					second = bill[i];
			}
			
			System.out.println("first Small : "+first+" second Small :"+second);
		

	}

}
