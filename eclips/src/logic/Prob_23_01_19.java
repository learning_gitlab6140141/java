package logic;

import java.util.Arrays;

public class Prob_23_01_19 {

	public static void main(String[] args) {
		int[][] a = { { 10, 20 }, { 30, 40 } };
		int[][] b = { { 40, 30 }, { 20, 10 } };
		addArray(a, b);

	}

	private static void addArray(int[][] a, int[][] b) {
		int r = a.length , c = b.length ;
		int[][] res = new int[r][c];
		for (int i = 0; i < r-1; i++) {
			for (int j = 0; j < c-1; j++) {
				res[r][c] = a[r][c] + b[r][c];				
			}
		}
		for(int i=0;i<res.length;i++) {
			System.out.println(Arrays.toString(res[i]));
		}
	}

}
