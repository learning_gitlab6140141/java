package logic;

import java.util.Scanner;

public class Prob_24_01_02 {
	static Prob_24_01_02 prog = new Prob_24_01_02();
	Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		prog.neonNum();
		prog.strongNum();
		prog.perfectNum();
		prog.spyNum();
		prog.primeNum();
		prog.squRoot();	
		prog.fibonacci();
		

	}

	private void fibonacci() {
		System.out.println("enter a number: ");
		int no = sc.nextInt(),a=0,b=1;
		while(a<=no) {
			System.out.println(a);
			b=a+b;
			a=b-a;
			
		}
		
	}

	private void squRoot() {
		System.out.println("enter a number to find its squire root : ");
		int no = sc.nextInt();
		int div=2;
		while(div<no) {
			if(no/div==div) {
				System.out.println(div+" is the squire root");
				break;
			}
			div++;
		}
	}

	private void primeNum() {
		System.out.println("test prime number are not: ");
		int no = sc.nextInt();
		int div=2;
		while(div<no){
			if(no%div==0) {
				System.out.println(no+" not a prime");
				break;
			}
			else
				div++;
		}
		
		if(div==no)
			System.out.println(no+" is a prime");
		
	}

	private void spyNum() {
		//check spy number		
				System.out.println("test spy number are not: ");				
				int no = sc.nextInt();
				// short form of if else
				// calling two methods prog.add(no) and prog.mult(no)
				System.out.println((prog.add(no)) == (prog.mult(no)) ? no + " spy number" : no + " not a spy number");
		//end of check spy number
		
	}

	private Object mult(int no) {
		int mult = 1;
		while (no > 0) {
			mult *= no % 10;
			no /= 10;
		}
		System.out.println("multiplication of the number is " + mult);
		return mult;
	}

	private Object add(int no) {
		int sum = 0;
		while (no > 0) {
			sum += no % 10;
			no /= 10;
		}
		System.out.println("addition of the number is " + sum);
		return sum;
	}

	private void perfectNum() {
		System.out.println("test perfect number are not: ");
		int no = sc.nextInt(), div = 1, sum = 0;
		for (; div < no; div++) {
			if (no % div == 0)
				sum += div;
			div++;
		}
		System.out.println(((div - 1) == no) ? no + " is perfect number" : no + " is not a perfect number");

	}

	private void neonNum() {
		System.out.println("test neon number are not: ");
		int no = sc.nextInt(), squ = (no * no), sum = 0;
		for (; squ > 0; squ /= 10) {
			sum += squ % 10;
		}
		System.out.println((sum == no) ? no + " is neon number" : no + " is not a neon number");
	}

	private void strongNum() {
		System.out.println("\ntest strong number are not: ");
		int no = sc.nextInt(), copy = no, sum = 0;
		while (no > 0) {
			int digit = no % 10;
			int factor = factorial(digit);
			sum += factor;
			no /= 10;
		}
		System.out.println((copy == sum) ? copy + " is strong number" : copy + " is not strong number");
	}

	private int factorial(int no) {
		int result = 1;
		while (no > 0) {
			result *= no;
			no--;
		}
		return result;
	}

}
