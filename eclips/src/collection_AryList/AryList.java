package collection_AryList;

import java.util.ArrayList;

public class AryList {

	public static void main(String[] args) {
		ArrayList al=new ArrayList();
		al.add(10);
		al.add(14.5);
		al.add(true);
		al.add("eswar");
		
		System.out.println("add method "+al.add("mari"));
		
		System.out.println(al);
		
		al.add(3, 798);
		System.out.println(al);
		
		System.out.println("get index value in al index 1 :"+al.get(1));
		System.out.println("check empty in al  :"+al.isEmpty());
		System.out.println("remove index value in al :"+al.remove(1));
		System.out.println("contain in al :"+al.contains("mari"));
//		System.out.println("*****try****** "+al.contains(al.get(1)));
		System.out.println("reprint al :"+al);
		
		ArrayList al2=new ArrayList();	
		System.out.println("al store in al2 :"+al2.addAll(al));
		al2.add("geetha");
		al2.add("geetha");
		al2.add("divs");
		System.out.println("print al2 :"+al2);
		System.out.println("in al2 :"+al2.containsAll(al));
		System.out.println("before print al :"+al);
		System.out.println("before print al2 :"+al2);
		
		System.out.println("al2.indexOf(\"geetha\") : "+al2.indexOf("geetha"));
		  System.out.println("al2.lastIndexOf(\"geetha\") : "+al2.lastIndexOf("geetha"));
		  System.out.println("al2.indexOf(\"manoj\") : "+al2.indexOf("manoj"));
		  System.out.println("before equals : "+al2.equals(al));
//		al2.remove(1);
//		System.out.println("remove check :"+al2);
		System.out.println(al2.retainAll(al));
		System.out.println("print al :"+al);
		System.out.println("print al2 :"+al2);
		
	
		
		System.out.println("equals : "+al2.equals(al));
//		  System.out.println("al2.indexOf(\"manoj\") : "+al2.indexOf("manoj"));
//		  System.out.println("al2.lastIndexOf(\"manoj\") : "+al2.lastIndexOf("manoj"));
		  al2.add("sarathkumar");
		  al2.add("vignesh");
		  
		  System.out.println("al : "+al);
		  System.out.println("al2 : "+al2);
		  al2.removeAll(al);
		  
		 
		  System.out.println("removeAll : "+al2);
		  
		  al2.addAll(al);
		  System.out.println("al "+al);
		  System.out.println("al2 "+al2);
		  System.out.println(al2.size());
		  al2.add(2, "prabakaran");
		  System.out.println("add(2, \"prabakaran\") : "+al2);
		  al2.set(1,"abc");
		  System.out.println("set(1, \"abc\") : "+al2);
		  
		  System.out.println("\n*********************obj*********************\n");
		  Object[] ar = al2.toArray();
		  for (Object ob : ar) {
		   System.out.println(ob);
		  }
	}

}
