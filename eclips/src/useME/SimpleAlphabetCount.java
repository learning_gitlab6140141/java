package useME;

import java.util.HashMap;

public class SimpleAlphabetCount {
    public static void main(String[] args) {
        String s = "Hello world";
        HashMap<Character, Integer> alpCnt = new HashMap<>();

        for (char ch : s.toCharArray()) {
            if (Character.isLetter(ch)) {
            	alpCnt.put(ch, alpCnt.getOrDefault(ch, 0) + 1);
                System.out.println("in loop : " + alpCnt);
            }
        }

        System.out.println("Alphabet counts: " + alpCnt);
    }
}

