package useME;

import java.util.Arrays;

public class Sumaa {

	public static void main(String[] args) {
		Sumaa su = new Sumaa();
		String s = "holy bible of jesus";

		char[] c = s.toCharArray();
		/********************* reverse string *******************************/
//		int st=0;
//		int l=0,r=c.length;
//		for(int i=0;i<r;i++) {
//			if(c[i]==' ') {
//				su.rev(c, st, i-1);
//				st=i+1;
//			}
//		}
//		su.rev(c, st, r-1);
////		System.out.println("char : "+ Arrays.toString(c));
//		System.out.println(su.rev(c,l,r-1));

		/************************* remove duplicate ***************************/
		su.count(c);
	}

	private void count(char[] c) {

		for (int i = 0; i < c.length; i++) {

			char curr = c[i];
			boolean allow = true;

			for (int j = 0; j < i; j++) {
				if (c[j] == curr) {
					allow = false;
					break;
				}
			}
			
			if (allow && c[i] !=' ') {
				int count=0;
					for(int j=0;j<c.length;j++) {
						
						if (curr==c[j]) {
							count++;
						}
					}

				System.out.print(curr+"-"+ count+"\n");
			}			
			
		}

	}

	private char[] rev(char[] c, int l, int r) {

		while (l < r) {
			char te = c[l];
			c[l] = c[r];
			c[r] = te;
			l++;
			r--;
		}

		return c;
	}

}
