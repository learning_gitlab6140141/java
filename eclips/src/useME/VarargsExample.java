package useME;

public class VarargsExample {
    // Method with varargs
    public static void printNumbers(int... s) {
        System.out.print("Numbers: ");
        for (int num : s) {
            System.out.print(num + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // Calling the method with different numbers of arguments
        printNumbers(1, 2, 3);
        printNumbers(10, 20, 30, 40, 50);
        printNumbers(); // You can also call it with zero arguments
        printNumbers(7,8);
        trStr("no");
        trStr("po","ty","gh");
    }

	private static void trStr(String... st) {
		for (String str : st) {
			System.out.println(str);
		}
		
	}
}
