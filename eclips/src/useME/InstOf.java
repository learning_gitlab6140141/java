package useME;

public class InstOf {

	public static void main(String[] args) {
//	       Object obj = "Hello, Java!";
//
//	        // Check if obj is an instance of String
//	        if (obj instanceof String) {
//	            String str = (String) obj; // Type casting is safe here
//	            System.out.println("The object is a String: " + str);
//	        } else {
//	            System.out.println("The object is not a String.");
//	        }
		
//		StringBuilder sb1=new StringBuilder("Scripting");
//		sb1.insert(2, "uuu").delete(7, 8);
//		System.out.println(sb1);
//		sb1.delete(1,7);
//		System.out.println(sb1.toString());
		
		String a="90";
		System.out.println("a->"+a.hashCode());
//		a="80";
		String b="90";
//		a="90";
		
		System.out.println("a->"+a.hashCode());
		System.out.println("b->"+b.hashCode());
		
		System.out.println(a==b?"y":"n");
		System.out.println(a.equals(b));
		
        String sti="90";
        int i=Integer.parseInt(sti);
        System.out.println(++i);
		
	}

}
