package useME;

public class Matrix {

	public static void main(String[] args) {
		Matrix m = new Matrix();
		int[][] a = { { 2, 3 },
				      { 4, 5 } },
				
				b = { { 3, 7 },
					  { 4, 3 } };
		
		int[][] c = { { 1, 2 },
			          { 3, 4 } },
			
			    d = { { 1, 2, 3 },
				      { 4, 5, 6 } },
			    
    		    e = { { 1, 2, 3 },
				      { 4, 5, 6 },
				      { 8, 5, 9 }};

//		m.add(a, b);
//		m.mul(c,d);
//		m.trans(d);
//		m.diaAdd(e);
		
		
		//printing 2d with enhances fore
		for(int[] ar:e) {
			for(int v:ar) {
				System.out.print(v+" ");
			}
			System.out.println();
		}
		

	}

	private void diaAdd(int[][] e) {
		int l=0,ri=0,t=0;  //
		
		for(int r=0;r<e.length;r++) {
			for(int c=0;c<e[r].length;c++) {
				if(r==c) l+=e[r][c];//principal diagonal
				if(r+c==e.length-1) ri+=e[r][c]; //secondary diag
			}
		}
		
		System.out.println("left: "+l+" + right: "+ri+" ="+(l+ri));
		
	}

	private void trans(int[][] d) {
		int[][] c=new int[d[0].length][d.length];
		
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c[i].length; j++) {
				c[i][j]=d[j][i];
				System.out.print(c[i][j]+ " ");
			}
			
			System.out.println();
		}
	}

	private void mul(int[][] a, int[][] b) {
		
		// 2  |x | 2 == 2  | x | 3 = 2  | x | 3
		//----+--+---------+---+--------+---+-------------
		// r1 |  | c1   r2 |   | c2  r1 |   | c2      		
//   a = { { 1, 2 },
//         { 3, 4 } },
		
//   b = { { 1, 2, 3 },  
//	       { 4, 5, 6 } },        
// a * b = {{(1*1)+(2*4) (1*2)+(2*5) (1*3)+(2*6)}, -> {{(a00*b00)+(a01*b10) (a00*b10)+(a01*b11) (a00*b02)+(a01*b12)},
//		    {(3*1)+(4*4) (3*2)+(4*5) (3*3)+(4*6)}} ->  {(a10*b00)+(a11*b10) (a10*b10)+(a11*b11) (a10*b02)+(a11*b12)}}
		
		int[][] c = new int[a[0].length][b[0].length];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b[0].length; j++) {
				for(int k=0;k<a[0].length;k++)
				c[i][j]+=(a[i][k]*b[k][j]);
			}
		}
		
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c[i].length; j++) {
				System.out.print(c[i][j]+ " ");
			}
			
			System.out.println();
		}
		
	}

	private void add(int[][] a, int[][] b) {
		int[][] c = new int[a.length][b.length];

		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c[i].length; j++) {
				c[i][j]=a[i][j]+b[i][j];
			}
		}
		
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c[i].length; j++) {
				System.out.print(c[i][j]+ " ");
			}
			
			System.out.println();
		}

	}

}
