package useME;


  class Superclass {
      // Static method in the superclass
      static void process() {
          System.out.println("Static method in Superclass");
      }
  }

  class Subclass extends Superclass {
      // Static method in the subclass hiding the method in the superclass
      static void process() {
          System.out.println("Static method in Subclass");
      }
  }

  public class Demo {
      public static void main(String[] args) {
          // Calling static method in the superclass
          Superclass.process();  // Output: Static method in Superclass

          // Calling static method in the subclass
          Subclass.process();    // Output: Static method in Subclass

          // Even if the reference is of the superclass type,
          // the actual method called is determined at compile-time
          Superclass reference = new Subclass();
          reference.process();   // Output: Static method in Superclass
      }
  
}