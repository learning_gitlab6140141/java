package useME.comparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Comp1 {
	String name;
	int r_no;

	public Comp1(String name, int r_no) {
		this.name=name;
		this.r_no=r_no;
	}

	public static void main(String[] args) {
		Comp2Chil c=new Comp2Chil();
		List li=new ArrayList();
		li.add(new Comp1 ("eswar",1));
		li.add(new Comp1 ("manoj",2));
		li.add(new Comp1 ("monish",3));
		li.add(new Comp1 ("monisha",4));
		System.out.println(li);
		
		Collections.sort(li,c);
		System.out.println(li);
		
//		String[] name= {"monish","abish","zen","dinesh"};
//		for (String st : name) {
//			System.out.println(st);
//		}
//		System.out.println("\nsorted\n");
//		
//		
//		Arrays.sort(name,c);
//		for (String st : name) {
//			System.out.println(st);
//		}
	}

}
