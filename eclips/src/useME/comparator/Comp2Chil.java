package useME.comparator;

import java.util.Comparator;

public class Comp2Chil implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		int res=o1.compareTo(o2);
		if(res > 0)
			return -1;
		return 1;
	}

//	@Override
//	public int compare(Integer o1, Integer o2) {
//		if(o1>o2)
//			return -1;
//		else
//			return 1;
//		
//	}

}
