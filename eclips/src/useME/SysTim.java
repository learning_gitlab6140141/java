package useME;

public class SysTim {

	 public static void main(String[] args) {
	        // Record the start time
	        long startTime = System.currentTimeMillis();

	        // Perform some operation
	        System.out.println("start "+startTime);
	        performOperation();

	        // Record the end time
	        long endTime = System.currentTimeMillis();
	        
	        System.out.println("end   "+endTime);

	        // Calculate the execution time
	        long executionTime = endTime - startTime;

	        // Print the execution time
	        System.out.println("The operation took " + executionTime + " milliseconds.");
	    }

	    public static void performOperation() {
	        // Simulating some time-consuming operation
	    	int n=100;
	        for (int i = -n; i < n; i++) {
//	           Math.sqrt(i) ;
	        	System.out.println(i);
	        }
	    }

}
