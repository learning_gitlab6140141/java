package useME;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CobleDEmo {

	public static void main(String[] args) {
		List<CobleEmpl> emp=new ArrayList<CobleEmpl>();
		emp.add(new CobleEmpl(1,"ramesh",30000));
		emp.add(new CobleEmpl(2,"gopal",35000));
		emp.add(new CobleEmpl(3,"selva",40000));
		emp.add(new CobleEmpl(4,"monish",50000));
		
		System.out.println(emp+"\n");
		
		Collections.sort(emp);
		
		for (CobleEmpl c : emp) {
			System.out.println(c);
		}
	}

}
