package useME;

public class CobleEmpl implements Comparable<CobleEmpl>{

	int id,salary;
	String name;
	
	
	public CobleEmpl(int id, String name, int salary) {
		this.id=id;
		this.name=name;
		this.salary=salary;
	}


	@Override
	public String toString() {
		return "id= " + id + ", salary= " + salary + ", name= " + name ;
	}


	@Override
	public int compareTo(CobleEmpl o) {
		if (salary>o.salary)
			return -1;
		else 
			return 1;
		
		
//		if(name.length()>o.name.length())
//			return -1;
//		else
//			return 1;
		
//		int n=name.compareTo(o.name);
//			if(n>0)	
//				return 1;
//			else
//				return -1;

	}
	

}
