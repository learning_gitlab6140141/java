package useME.Comparable;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortFruits {

	public static void main(String[] args) {
		
		SortFruitsCol  com=new SortFruitsCol() ;

		List fru = new ArrayList<String>();
		fru.add("mango");
		fru.add("banana");
		fru.add("gova");
		fru.add("pine apple");
		fru.add("apple");
		fru.add("banana");
		
		System.out.println(fru);
		
		Collections.sort(fru);
		System.out.println("without overriding ==> "+fru);
		
		Collections.sort(fru,com);
		System.out.println("after   overriding ==> "+fru);
	}

}
