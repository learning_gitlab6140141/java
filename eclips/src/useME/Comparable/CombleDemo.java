package useME.Comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CombleDemo {

	public static void main(String[] args) {
		List<Stud> studs=new ArrayList<>();
		studs.add(new Stud(7223, "banu", 92));
		studs.add(new Stud(7221,"abish", 89));
		studs.add(new Stud(7224, "chandru", 62));
		studs.add(new Stud(7222, "ajth", 82));		
		
		
		Collections.sort(studs,(i,j)->i.marks>j.marks?-1:1);
		
		for (Stud s : studs) {
			System.out.println(s);
		}

	}

}
