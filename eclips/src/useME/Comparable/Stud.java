package useME.Comparable;

public class Stud implements Comparable<Stud> {
	int roll_no,marks;
	String name;
	
	public Stud(int roll_no, String name, int marks) {
		super();
		this.roll_no = roll_no;
		this.marks = marks;
		this.name = name;
	}

	public String toString() {
		return "[roll_no= " + roll_no +  ", name= " + name +", marks= " + marks + "]";
	}

	public int compareTo(Stud st) {
		
		return marks>st.marks?-1:1;
//		return name.length()>st.name.length()?1:-1;
//		return name.compareTo(st.name);
	}
	

	
}
