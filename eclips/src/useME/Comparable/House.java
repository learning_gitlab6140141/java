package useME.Comparable;

public class House {
	
	private String Address;  
	private int BedRoom;
	private double Area;
	private double price;
	private int YearBuilt;
	
	public House(String address, int bedRoom, double area, double price, int yearBuilt) {
		super();
		Address = address;
		BedRoom = bedRoom;
		Area = area;
		this.price = price;
		YearBuilt = yearBuilt;
	}

}
