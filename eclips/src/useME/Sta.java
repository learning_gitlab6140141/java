package useME;

public class Sta {
 static int a=10;
 
 {
	 System.out.println("non-static/object block call only in object presences"+a); 
 }
 static {
	 System.out.println("static block "+a);
 }

 public Sta(int n) {
	 this.a=n;
	 System.out.println("constructor out : "+a);
 }

	public static void main(String[] args) {
		 
		 
			 System.out.println("main block "+a); 
		 
		Sta st=new Sta(27);
		String str="4";
		System.out.println(a);
		System.out.println(Sta.a);
		int no=Integer.parseInt(str);
		System.out.println(no);
		int no2=Integer.parseUnsignedInt(str);
		System.out.println(no2);

	}

}
