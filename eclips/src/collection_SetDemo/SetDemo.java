package collection_SetDemo ;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
		ArrayList sd= new ArrayList();
		sd.add("eswar");
		sd.add("vidjesh");
		sd.add("monish");
		sd.add("monish");
		sd.add("manoj");
		sd.add("monisha");
		
		System.out.println(sd);
		
		Object[] ar=sd.toArray();
		for(Object a:ar) {
			System.out.println(a);
		}
		
		HashSet sd2=new HashSet(sd);
		System.out.println("HashSet "+sd2);
		
		LinkedHashSet lhs=new LinkedHashSet(sd);
		System.out.println("LinkedHashSet "+lhs);
		
		TreeSet ts=new TreeSet(sd);
		System.out.println("TreeSet "+ts);
		}
	
}
