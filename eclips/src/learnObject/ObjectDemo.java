package learnObject;

public class ObjectDemo {
 int i;
	public ObjectDemo(int i) {
		this.i=i;
		System.out.println("constructor "+i);
	}

	public ObjectDemo() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		ObjectDemo od=new ObjectDemo(123);
		ObjectDemo od2=new ObjectDemo();
		ObjectDemo od3=od;
		od.cal(od);
		od3.cal(od);
		
		System.out.println(od.hashCode());
		System.out.println(od2.hashCode());
		
		System.out.println(od.equals(od2));
		System.out.println(od.equals(od3));
	}

	private void cal(ObjectDemo od) {
		
		System.out.println("meth "+this.i);
		
	}
	
	public int hashCode(){
		return 10;
	}
	
}
