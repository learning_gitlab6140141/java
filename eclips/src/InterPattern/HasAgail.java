package InterPattern;

import java.util.Scanner;

public class HasAgail {

	public static void main(String[] args) {
		HasAgail ha = new HasAgail();
		Scanner sc = new Scanner(System.in);

		System.out.println("enter a number for patern");
		int n = sc.nextInt();
//		int n=5;
		ha.pattern(n);
		ha.patternre(n);
		ha.pattern2(n);
		
//		int[] nu= {120,30,016};
//		for (int i : nu) 
//			System.out.println(i);
		

	}

	private void patternre(int n) {
		System.out.println("\nREprint pattern-2\n");
		for (int row = n; row > 0; row--) {
			for (int col = n; col >0; col--) {
				if (col>row)
					System.out.print(col+" ");
				else
					System.out.print(row+" ");
			}
			System.out.println();
		}

	}

	private void pattern2(int n) {
		System.out.println("\npattern-2\n");
		for(int i=n;i>0;i--) {
			for (int j=1;j<=i;j++)
				System.out.print(i+" ");
			for(int k=i+1;k<=n;k++)
				System.out.print(k+" ");
			System.out.println();
		}
		
	}

	private void pattern(int n) {
		System.out.println("pattern-1\n");
		for (int row = n; row > 0; row--) {
			for (int col = n; col >= row; col--) {
				System.out.print(col + " ");
			}
			for(int col2=row;col2>1;col2--) {
				System.out.print(row + " ");
			}
			System.out.println();
		}
	}

}
