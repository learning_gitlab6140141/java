package apti;

public class NumRela {
	int no;
	int mult;

	public NumRela(int no, int mult) {
		this.no = no;
		this.mult = mult;
	}

	public static void main(String[] args) {
		NumRela oper = new NumRela(14, 5);
//		oper.fr();
//		oper.whl();
		oper.ad();
	}

	private void ad() {
		no=0;
		int[] n= {1,2,3,4,5,6};
		for(int i=0 ;i<n.length;i++) {
			no+=n[i];
			System.out.print(no+" ");
			
		}
		
		
	}

	private void whl() {
		System.out.print("using while\n");
		
		while (no <= 329) {
			System.out.print(no + " ");
			no += mult;
			mult *= 2;
//			System.out.println("mult " + mult + " ");

		}
		
	}

	private void fr() {
		System.out.print("using for\n");
		
	/*	for (; no <= 329; no+=0 )// try with while
		{
			System.out.print(no + " ");
			no+=mult;
			mult *= 2;	
			System.out.println("mult " + mult + " ");
		}
	*/
		//can also be done using this method copied from praba

		for (int x=0; x <= 6; x++)
		{
			System.out.print(no + " ");
			no+=mult;
			mult *= 2;	
//			System.out.println("mult " + mult + " ");
		}
//		System.out.println("\n\n" + no + " " + mult);
 	
 
	}

}
