package TryExte;

public class SubCls {

	public static void main(String[] args) {
		Supercls sc=new Supercls() 
		{
			protected void met() 
			{
				System.out.println("child method");

			}
			protected void met2() {
				System.out.println("child method second");
			}

		};
		sc.met();
		sc.met2();
		
	}

}
