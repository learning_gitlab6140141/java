package collection_MapLearn;

import java.util.Collection;
import java.util.HashMap;
//import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
//import java.util.TreeMap;

public class mapDemo {

	public static void main(String[] args) {
		
		HashMap hm=new HashMap(); 
//		LinkedHashMap hm=new LinkedHashMap(); 
//		TreeMap hm=new TreeMap(); 
		hm.put("idly", 40);
		hm.put("poori", 35);
		hm.put("pongal", 40);
		hm.put("curd", 45);
		hm.put("vadai", 30);
		hm.put("dosai", 40);
		hm.put("dosai", 60);
		
		System.out.println(hm);
		System.out.println("get(\"idly\") "+hm.get("idly"));
		System.out.println("get(45) "+hm.get(45));
		System.out.println("containsKey---> "+hm.containsKey("curd"));
		System.out.println("containsValue --> "+hm.containsValue(60));
		System.out.println(hm.remove("poori"));//shows the value
		System.out.println(hm.remove(30));
		System.out.println(hm);
		System.out.println("replace "+hm.replace("vadai", 30,50));
		System.out.println(hm);
		System.out.println("entrySet (gives all entry):- ==> "+hm.entrySet());
		System.out.println(hm.keySet());
		System.out.println(hm.values()+"\n");
		
//		Iterator j=hm.entrySet().iterator();
//		while(j.hasNext()) {
//			System.out.println("j "+j);
//		}
		
		
		Set s=hm.entrySet();//gives set of entry
		
		Collection c=hm.keySet();// key and value are collection obje
		System.out.println("c--> "+c);
		
		Iterator i=s.iterator();
		
	
		
		while(i.hasNext()) {
//			System.out.println("hasNext: "+i.next());//prints each object

			
			Map.Entry me = (Map.Entry) i.next();//changed into map.entry's obj
			System.out.println((me.getKey()+" cost "+me.getValue()));
			
			if(me.getKey().equals("curd")) {
				me.setValue(30);
			}
			
			me.setValue((int)me.getValue()+5);
		}
		System.out.println(hm);
		
	}

}
