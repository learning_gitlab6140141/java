package com.browser.firefox.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.browser.firefox.repository.BrowserInterface;



@Controller
public class fireFoxController {
	
	@Autowired
	BrowserInterface obj;
	
	@RequestMapping("form")
	public ModelAndView browser() {
		ModelAndView mav=new ModelAndView("openPage");
		mav.addObject("data",obj.findAll());
		
		return mav;
	}

}
