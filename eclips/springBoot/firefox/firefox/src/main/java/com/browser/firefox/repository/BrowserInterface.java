package com.browser.firefox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.browser.firefox.entity.FireFox;

public interface BrowserInterface extends JpaRepository<FireFox, Integer> {

}
