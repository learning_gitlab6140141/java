package com.browser.firefox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirefoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirefoxApplication.class, args);
	}

}
