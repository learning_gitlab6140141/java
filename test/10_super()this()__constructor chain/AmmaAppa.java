/*Polymorphism in Java can be achieved 
in two ways i.e., method overloading and method overriding*/
public class AmmaAppa
{
	public AmmaAppa()
	{
		this(10); // this has a argument so it go and calls the argument constructor first
		//this(20,true);
		// call to this must be first statement in constructor
		System.out.println("AmmaAppa class no_args constructor");
	}
	
	public AmmaAppa(int i)
	{
		System.out.println("AmmaAppa class one_args constructor called by pasanga");
	}
	
	public AmmaAppa(int i , boolean b)
	{
		System.out.println("AmmaAppa class two with boolean constructor .First number: "+i+" boolean:  "+b);
	}
}
/*The parent class' constructor needs to be called before the subclass' constructor. This will ensure that if you call any methods on the parent class in your constructor, the parent class has already been set up correctly.*/