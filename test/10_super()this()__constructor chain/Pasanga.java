public class Pasanga extends AmmaAppa
{
	public Pasanga()
	{
	
		System.out.println("pasanga class no_args constructor");
	}
	public Pasanga(int no)
	{
	//implicit call;
	//super(no);//super of is a method default it is in visible without arguments i.e., explicit call
	// call to super must be first statement in constructor
	super(no ,true);
		System.out.println("pasanga class one_args constructor");
		
	}
	
	public static void main (String[] args)
	{
		Pasanga pa= new Pasanga();
		Pasanga pa1= new Pasanga(30);
		
	}	
	
}