public interface BioData
	// same rules as class
	//interface method by default they are abstract
	//variables are by default finals 
{
	int seats = 15;
	
/*  = expected
	int no_of_leave;
	               ^     */
	
	public void name();
	public void mark();
	public void address();
	/*interface abstract methods cannot have body
	public void getSalery(){}
*/
	public static void main (String[] args) {}
	public static void age() {
	
	System.out.println("ade 27");
	}
}