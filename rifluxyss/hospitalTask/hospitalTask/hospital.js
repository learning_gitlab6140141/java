// var usNm;
// var mbl;
// var mail;
// var password;
// var conPass;
// var gen;
// var adrs;
// var state;
// var pin;
// var opt;
// var ckBx = [];

function validate() {
var  usNm = $(inputName).val();
var  mbl = $(inputMobileNumber).val();
var  mail = $(inputEmail).val();
var  password = $(inputPassword).val();
var  conPass = $(inputPasswordCon).val();
var  gen = $("input[name=gender]:checked").val();
var  adrs = $("#address").val();
var state = $(inputCity).val();
var  pin = $(inputZip).val();
var  opt = $("select option:selected").val();

var ckBx = [];

  $("input[name=patTyp]:checked").each(function () {
    ckBx.push($(this).val());
  });
  console.log(ckBx);

 

  if (usNm === "") {
    $("#namErr").css({ display: "block", color: "red" }).text("*enter name");
  } 
  else {
    var valNm = /^[A-Za-z]+$/;
    if (valNm.test(usNm)) {
      $("#namErr").css({ display: "none", color: "green" }).text("valid name");

      // $("#nmDn").text(usNm);

      if (mbl === "") {
        $("#noErr")
          .css({ display: "block", color: "red" })
          .text("should not be empty");
      } else {
        var valNo = /^[5-9]{1}[0-9]{9}$/;

        if (valNo.test(mbl)) {
          $("#noErr")
            .css({ display: "none", color: "green" })
            .text("valid number");
          // $(noDn).text(mbl);
          if (mail === "") {
            $("#mailErr")
              .css({ display: "block", color: "red" })
              .text("give an email");
          } else {
            var valEmail = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;

            if (valEmail.test(mail)) {
              $("#mailErr")
                .css({ display: "none", color: "green" })
                .text("valid email");
              // $(emDn).text(mail);

              if (password === "") {
                $(pasErr)
                  .css({ display: "block", color: "red" })
                  .text("enter password");
              } else {
                var valPass =
                  /^(?=.*[A-Z])(?=.*[~`!@#$%^&*_+()-|?])(?=.*\d).{8,}$/;
                if (valPass.test(password)) {
                  $(pasErr)
                    .css({ display: "none", color: "green" })
                    .text("valid password");

                  if (conPass === "") {
                    $(conPas)
                      .css({ display: "block", color: "red" })
                      .text("enter confirm password");
                  } else {
                    if (conPass === password) {
                      $(conPas)
                        .css({ display: "none", color: "green" })
                        .text("password matches");

                      if (gen == null) {
                        $(genErr)
                          .css({ display: "block", color: "red" })
                          .text("select gender");
                      } else {
                        $(genErr)
                          .css({ display: "none", color: "green" })
                          .text("selected");
                        //console.log(gen)//getting value
                        // skill box array
                        // $(gDn).text(gen);

                        if (ckBx.length == 0) {
                          $(patErr)
                            .css({ display: "block", color: "red" })
                            .text("enter patent admit type");
                        } else {
                          $(patErr)
                            .css({ display: "none", color: "green" })
                            .text(" ");
                          console.log(ckBx); //check box value

                          // $(SkDn).text(ckBx);

                          if (adrs == "") {
                            $(addErr)
                              .css({ display: "block", color: "red" })
                              .text("enter your address");
                          } else {
                            $(addErr).css({ display: "none", color: "green" });
                            // $(naDn).text(adrs).html("<b>");

                            if (state === "") {
                              $(detErr)
                                .css({ display: "block", color: "red" })
                                .text("enter your state");
                            } else {
                              $(detErr).css({
                                display: "none",
                                color: "green",
                              });
                              // $(naDn).text(state).html("<b>");

                              if (opt === "") {
                                $(detErr)
                                  .css({ display: "block", color: "red" })
                                  .text("select nation");
                              } else {
                                // $(naDn).text(opt).html("<b>");

                                $(detErr)
                                  .css({ display: "none", color: "green" })
                                  .text("selected");
                                if (pin === "") {
                                  $(detErr)
                                    .css({ display: "block", color: "red" })
                                    .text("enter pin number");
                                } else {
                                  var valPin = /^[0-9]{8}+$/;
                                  // alert(valPin.test(pin))

                                  if (valPin.test(pin)) {
                                    $(detErr)
                                      .css({ display: "none", color: "green" })
                                      .text("selected");

                                    // $(naDn).text(valPin);

                                    // $("#signUP").modal("hide");
                                    // $("#login").modal("show");

                                    // $(inputEmail3).val(mail);
                                    // $(inputPassword3).val(password);

                                    // $("#loginBtn").css({ display: "none" });
                                    // $("#account").css({ display: "block" });

                                  } else {
                                    $(detErr)
                                      .css({ display: "block", color: "red" })
                                      .text("enter valid number");
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    } else {
                      $(conPas)
                        .css({ display: "block", color: "red" })
                        .text("password not matching");
                    }
                  }
                } else {
                  $(pasErr)
                    .css({ display: "block", color: "red" })
                    .text("password must have a upper case ,number");
                }
              }
            } else {
              $("#mailErr")
                .css({ display: "block", color: "red" })
                .text("invalid email");
            }
          }
        } else {
          $("#noErr")
            .css({ display: "block", color: "red" })
            .text("Enter valid number");
        }
      }
    } else {
      $("#namErr")
        .css({ display: "block", color: "red" })
        .text("should not have numbers or special char");
    }
  }
}

function logoutBtn() {
  $("#loginBtn").css({ display: "block" });
  $("#account").css({ display: "none" });
}
