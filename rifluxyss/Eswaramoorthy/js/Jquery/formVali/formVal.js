// $("document").ready(function(){
//     $("#btn").click(function()

function validation() {
  var usNm = $("#userName").val();
  var mbl = $("#number").val();
  var mail = $("#email").val();
  var password = $("#password").val();
  var conPass = $("#ConPassword").val();
  var gen = $("input[name=gender]:checked").val();
  // var ckBx=$('input[name=skill]:checked').val();
  var opt = $("select option:selected").val();

  var ckBx = [];
  $("input[name=skill]:checked").each(function () {
    ckBx.push($(this).val());
  });
  console.log(ckBx);

  // for( i=0; i<ckBx.length;i++ ){
  //     console.log(ckBx[i])
  // }

 

  if (usNm === "") {
    $("#nmAbs").css({ display: "block", color: "red" }).text("enter name");
  } else {
    var valNm = /^[A-Za-z]+$/;
    if (valNm.test(usNm)) {
      $("#nmAbs").css({ display: "none", color: "green" }).text("valid name");
      $("#nmDn").text(usNm);

      if (mbl === "") {
        $("#mblNo")
          .css({ display: "block", color: "red" })
          .text("should not be empty");
      } else {
        var valNo = /^[5-9]{1}[0-9]{9}$/;

        if (valNo.test(mbl)) {
          $("#mblNo")
            .css({ display: "none", color: "green" })
            .text("valid number");
          $(noDn).text(mbl);
          if (mail === "") {
            $(mailErr)
              .css({ display: "block", color: "red" })
              .text("give an email");
          } else {
            var valEmail = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;

            if (valEmail.test(mail)) {
              $("#mailErr")
                .css({ display: "none", color: "green" })
                .text("valid email");
              $(emDn).text(mail);
              if (password === "") {
                $(pasVal)
                  .css({ display: "block", color: "red" })
                  .text("enter password");
              } else {
                var valPass =
                  /^(?=.*[A-Z])(?=.*[~`!@#$%^&*_+()-|?])(?=.*\d).{8,}$/;
                if (valPass.test(password)) {
                  $(pasVal)
                    .css({ display: "none", color: "green" })
                    .text("valid password");

                  if (conPass === "") {
                    $(conPas)
                      .css({ display: "block", color: "red" })
                      .text("enter confirm password");
                  } else {
                    if (conPass === password) {
                      $("#conPas")
                        .css({ display: "none", color: "green" })
                        .text("password matches");

                      if (gen == null) {
                        $(genErr)
                          .css({ display: "block", color: "red" })
                          .text("select gender");
                      } else {
                        $(genErr)
                          .css({ display: "none", color: "green" })
                          .text("selected");
                        //console.log(gen)//getting value
                        // skill box array
                        $(gDn).text(gen);

                        if (ckBx.length == 0) {
                          $(skl)
                            .css({ display: "block", color: "red" })
                            .text("enter your skills");
                        } else {
                          $(skl)
                            .css({ display: "none", color: "green" })
                            .text(" skills selected");
                          console.log(ckBx); //check box value

                          $(SkDn).text(ckBx.join(', '));

                          if (opt === "") {
                            $(nation)
                              .css({ display: "block", color: "red" })
                              .text("select nation");
                          } else {
                            $(naDn).text(opt);
                            $(nation)
                              .css({ display: "none", color: "green" })
                              .text("selected");
                            alert("all datas are entered");
                            $(complete).css({ display: "block" });
                          }
                        }
                      }
                    } else {
                      $(conPas)
                        .css({ display: "block", color: "red" })
                        .text("password not matching");
                    }
                  }
                } else {
                  $(pasVal)
                    .css({ display: "block", color: "red" })
                    .text("password must have a upper case ,number");
                }
              }
            } else {
              $("#mailErr")
                .css({ display: "block", color: "red" })
                .text("invalid email");
            }
          }
        } else {
          $("#mblNo")
            .css({ display: "block", color: "red" })
            .text("Enter valid number");
        }
      }
    } else {
      $("#nmAbs")
        .css("display", "block")
        .text("should not have numbers or special char");
    }
  }
}


/******************* try nth child ***************/
// function tblAxs(){
// var te=$("#complete").find("tr td:eq(2)").val();
var index=2;
// console.log(te)}
function tblAxs() {
  // Find the third cell (index 2) in each row of the table
  var thirdColumnCells = $("#complete tbody tr").find('td:nth-child(2)');
  console.log(thirdColumnCells)
  // Loop through each cell
  thirdColumnCells.each(function(index) {
      // Modify the content of the cell
      $(this).text("New Value " + index);
  });
}