// Golobal Variable
var uname,passWord,confirmPassword,address,phoneNumber, email, gender,food,nationality,updatefood,updatenationality,updategender,opwd,npwd,rpwd;

// Signup Form Validation Starts


function signup()
{
    uname = $('#uname').val();
    email = $('#email').val();
    passWord = $('#pwd').val();
    confirmPassword = $('#conpwd').val();
    phoneNumber = $('#mno').val();
    gender = $('input[name = "gender"]:checked').val();
    address = $('#address').val();
    food = []
    $('input[name = "food"]:checked').each(function(){
    food.push(this.value);})
    nationality = $('#nationality option:selected').val();
    namePattern = /^[A-Za-z ]+$/;
    mobilePattern = /^([6-9]{1}[0-9]{9})+$/;
    emailPattern = /^[A-Za-z0-9_.]+\@[a-z]+\.[a-z]{2,3}$/;
    $('#unameerror').css({ "display": "block" }).text("*Please Enter Your Name").hide()
    $('#unameerror').css({ "display": "block" }).text("*Digits Or Special Character Are Not Allowed").hide()
    $('#emailerror').css({ "display": "block" }).text("*Please Enter Your E-mail").hide()
    $('#emailerror').css({ "display": "block" }).text("*Please Enter Valid Mail").hide()
    $('#pwderror').css({ "display": "block" }).text("*Please Enter Your Password").hide()
    $('#conpwderror').css({ "display": "block" }).text("*Password Mismatch").hide()
    $('#mnoerror').css({ "display": "block" }).text("*Please Enter Your Phone Number").hide()
    $('#mnoerror').css({ "display": "block" }).text("*Invalid Number").hide()
    $('#gendererror').css({ "display": "block" }).text("*Please Select Your").hide()
    $('#addresserror').css({ "display": "block" }).text("*Please Select Your Address").hide()
    $('#fooderror').css({ "display": "block" }).text("*Pick One").hide()
    $('#Nationalityerror').css({ "display": "block" }).text("*Please Select Nationality").hide()

    if(uname == "")
    {
        $('#unameerror').css({ "display": "block" }).text("*Please Enter Your Name")
        $('#uname').focus()
        
    }
    else if(!uname.match(namePattern))
    {
        $('#unameerror').css({ "display": "block" }).text("*Digits Or Special Character Are Not Allowed")
        $('#uname').focus()
    }
    else if(email == "")
    {
        $('#emailerror').css({ "display": "block" }).text("*Please Enter Your E-mail")
        $('#email').focus()
       
    }
    else if(!email.match(emailPattern))
    {
        $('#emailerror').css({ "display": "block" }).text("*Please Enter Valid Mail")
        $('#email').focus()

    }
    else if(passWord == "")
    {
        $('#pwderror').css({ "display": "block" }).text("*Please Enter Your Password")
        $('#pwd').focus()
    }
    else if(confirmPassword != passWord)
    {
        $('#conpwderror').css({ "display": "block" }).text("*Password Mismatch")
        $('#conpwd').focus()

    }
    else if(phoneNumber == "")
    {
        $('#mnoerror').css({ "display": "block" }).text("*Please Enter Your Phone Number");
        $('#mno').focus()

    }
    else if(!phoneNumber.match(mobilePattern))
    {
        $('#mnoerror').css({ "display": "block" }).text("*Invalid Number");
        $('#mno').focus()

    }
    else if(gender == null)
    {
        $('#gendererror').css({ "display": "block" }).text("*Please Select Your")
        $('#gender').focus()

    }
    else if(address == "")
    {
        $('#addresserror').css({ "display": "block" }).text("*Please Select Your Address")
        $('#address').focus()

    }
    else if(food.length == 0)
    {
        $('#fooderror').css({ "display": "block" }).text("*Pick One")
        $('#food').focus()

    }
    else if( nationality == "")
    {
        $('#Nationalityerror').css({ "display": "block" }).text("*Please Select Nationality")
        $('#Nationality').focus()

    }
    else
    {
        $("#loguname ").val(uname)
        $("#logpwd").val(passWord)
        $("#editname").text(uname)
        $("#editmno").text(phoneNumber)
        $("#editmail").text(email)
        $("#editgender").text(gender)
        $("#editfood").text(food)
        $("#editaddress").text(address)
        $("#editnationality").text(nationality)
        $("#updateuname").val(uname)
        $("#updatepwd").val(passWord)
        $('#updateaddress').val(address)
        $("#updateconpwd").val(confirmPassword)
        $("#updatemno").val(phoneNumber)
        $("#updateemail").val(email)
        $('#'+gender).attr("checked","true")
        for(i = 0;i<food.length;i++)
        {
                                
            $('#'+food[i]).attr("checked","true")
                                            
        }
        $('#'+nationality).attr("selected","true")
        $('#loginmodal').modal('show')
        $('#signupmodal').modal('hide')
        

    }


}


// Signup Form Validation Ends

// update Form Validation Starts


function update()
{
    uname = $('#updateuname').val();
    email = $('#updateemail').val();
    passWord = $('#updatepwd').val();
    confirmPassword = $('#updateconpwd').val();
    phoneNumber = $('#updatemno').val();
    updategender = $('input[name = "updategender"]:checked').val()
    address = $('#updateaddress').val();
    updatefood = []
    $('input[name = "updatefood"]:checked').each(function(){
    updatefood.push(this.value);
    })
    updatenationality = $('#updatenationality option:selected').val()
    namePattern = /^[A-Za-z ]+$/;
    mobilePattern = /^([6-9]{1}[0-9]{9})+$/;
    emailPattern = /^[A-Za-z0-9_.]+\@[a-z]+\.[a-z]{2,3}$/;
    $('#updateunameerror').css({ "display": "block" }).text("*Please Enter Your Name").hide()
    $('#updateunameerror').css({ "display": "block" }).text("*Digits Or Special Character Are Not Allowed").hide()
    $('#updateemailerror').css({ "display": "block" }).text("*Please Enter Your E-mail").hide()
    $('#updateemailerror').css({ "display": "block" }).text("*Invalid Mail").hide()
    $('#updatepwderror').css({ "display": "block" }).text("*Please Enter Your Password").hide()
    $('#updateconerror').css({ "display": "block" }).text("*Password Mismatch").hide()
    $('#updatemnoerror').css({ "display": "block" }).text("*Please Enter Your Phone Number").hide()
    $('#updatemnoerror').css({ "display": "block" }).text("*Invalid Number").hide()
    $('#updateaddresserror').css({ "display": "block" }).text("*Please Select Your Address").hide()
    $('#updatefooderror').css({ "display": "block" }).text("*Pick One").hide()
    $('#updatenationalityerror').css({ "display": "block" }).text("*Please Select Nationality").hide()

    if(uname == "")
    {
        $('#updateunameerror').css({ "display": "block" }).text("*Please Enter Your Name")
        $('#updateuname').focus()
    }
    else if(!uname.match(namePattern))
    {
        $('#updateunameerror').css({ "display": "block" }).text("*Digits Or Special Character Are Not Allowed")
        $('#updateuname').focus()

    }
    else if(email == "")
    {
        $('#updateemailerror').css({ "display": "block" }).text("*Please Enter Your E-mail")
        $('#updateemail').focus()

    }
    else if(!email.match(emailPattern))
    {
        $('#updateemailerror').css({ "display": "block" }).text("*Please Enter Valid Mail")
        $('#updateemail').focus()

    }
    else if(passWord == "")
    {
        $('#updatepwderror').css({ "display": "block" }).text("*Please Enter Your Password")
        $('#updatepwd').focus()

    }
    else if(confirmPassword != passWord)
    {
        $('#updateconerror').css({ "display": "block" }).text("*Password Mismatch")
        $('#updatecon').focus()
    }
    else if(phoneNumber == "")
    {
        $('#updatemnoerror').css({ "display": "block" }).text("*Please Enter Your Phone Number");
        $('#updatemno').focus()

    }
    else if(!phoneNumber.match(mobilePattern))
    {
        $('#updatemnoerror').css({ "display": "block" }).text("*Invalid Number");
        $('#updatemno').focus()

    }
    else if(updategender == null)
    {
        $('#updategendererror').css({ "display": "block" }).text("*Please Select Your Gender")
        $('#updategender').focus()

    }
    else if(address == "")
    {
        $('#updateaddresserror').css({ "display": "block" }).text("*Please Select Your Address")
        $('#updateaddress').focus()

    }
    else if(updatefood.length == 0)
    {
        $('#updatefooderror').css({ "display": "block" }).text("*Pick One")
        $('#updatefood').focus()

    }
    else if(updatenationality == '')
    {
        $('#updatenationalityerror').css({ "display": "block" }).text("*Please Select Nationality")
        $('#updatenationality').focus()

    }
    else
    {
        $("#loguname ").val(uname)
        $("#logpwd").val(passWord)
        $("#editname").text(uname)
        $("#editmno").text(phoneNumber)
        $("#editmail").text(email)
        $("#editgender").text(updategender)
        $("#editfood").text(updatefood)
        $("#editaddress").text(address)
        $("#editnationality").text(updatenationality)
        $("#updateuname").val(uname)
        $("#updatepwd").val(passWord)
        $("#updateconpwd").val(confirmPassword)
        $("#updatemno").val(phoneNumber)
        $("#updateemail").val(email)
        $('#'+updategender).attr("checked","true")
        for(i = 0;i<updatefood.length;i++)
        {
                                
            $('#'+updatefood[i]).attr("checked","true")
                                            
        }
        $('#'+updatenationality).attr("selected","true")

        $('#updatemodal').modal('hide')
                                         
       
    }


}


// update Form Validation Ends


// Forget Password Form Validation Starts


function forgetval()
{
    opwd = $('#opwd').val()
    npwd = $('#npwd').val()
    rpwd = $('#rpwd').val()
    $('#opwderror').text("*Please Enter Old Password").hide()
    $('#npwderror').text("*New Password Same As Old Password").hide()
    $('#npwderror').text("*Please Enter New Password").hide()
    $('#rpwderror').text("*invalid Password").hide()
    if(opwd == "")
    {
        $('#opwderror').text("*Please Enter Old Password").show()
    }
    else if(opwd == npwd)
    {
        $('#npwderror').text("*New Password Same As Old Password").show()

    }
    else if(npwd == "")
    {
        $('#npwderror').text("*Please Enter New Password").show()

    }
    else if(rpwd != npwd)
    {
        $('#rpwderror').text("*invalid Password").show()
    }
    else
    {
        alert('Sucessfully Updated')
    }
}


// Forget Password Form Validation Ends


//Login Form Validation Starts

function login()
{
    

   var loguname = $('#loguname').val()
   var logpwd = $('#logpwd').val()
   if(loguname != "")
   {
        if(logpwd != "")
        {
            $('#logmenu').hide()
            $('#loginlogo').show()
            $('#loginmodal').hide()
            const loginmodal = bootstrap.Modal.getOrCreateInstance("#loginmodal")
            loginmodal.hide()
            $('#cartlogo').show()
        }
        else
        {
            $('#logpwdrror').css({ "display": "block" }).text("Please Enter Your Password")
        }
   }
   else
   {
        $('#logunamerror').css({ "display": "block" }).text("Please Enter Your Name")
   }
}

//Login Form Validation Ends


// Password Eye Shown Starts

//Login Password Shown
function logshow()
{
    $('#logpwd').attr("type","text")
    $('#hide').show()
    $('#show').hide()
}
function loghide()
{
    $('#logpwd').attr("type","password")
    $('#hide').hide()
    $('#show').show()
}

//Signup Password Shown
function signinshow()
{
    $('#pwd').attr("type","text")
    $('#signhide').show()
    $('#signshow').hide()
}
function signinhide()
{
    $('#pwd').attr("type","password")
    $('#signhide').hide()
    $('#signshow').show()
}

// Signup Conform Password Shown
function signinconshow()
{
    $('#conpwd').attr("type","text")
    $('#conpwdhide').show()
    $('#conpwdshow').hide()
}
function signinconhide()
{
    $('#conpwd').attr("type","password")
    $('#conpwdhide').hide()
    $('#conpwdshow').show()
}

//Update Password Shown
function updateshow()
{
    $('#updatepwd').attr("type","text")
    $('#uphide').show()
    $('#upshow').hide()
}
function updatehide()
{
    $('#updatepwd').attr("type","password")
    $('#uphide').hide()
    $('#upshow').show()
}
//update Conformation shown
function updconshows()
{
    $('#updateconpwd').attr("type","text")
    $('#updconhide').show()
    $('#updconshow').hide()
}
function updconhides()
{
    $('#updateconpwd').attr("type","password")
    $('#updconhide').hide()
    $('#updconshow').show()
}

//Forget Old Password Shown
function opwdshows()
{
    $('#opwd').attr("type","text")
    $('#opwdhide').show()
    $('#opwdshow').hide()
}
function opwdhides()
{
    $('#opwd').attr("type","password")
    $('#opwdhide').hide()
    $('#opwdshow').show()
}

//Forget New Password Shown
function npwdshows()
{
    $('#npwd').attr("type","text")
    $('#npwdhide').show()
    $('#npwdshow').hide()
}
function npwdhides()
{
    $('#npwd').attr("type","password")
    $('#npwdhide').hide()
    $('#npwdshow').show()
}

//Forget Re-enter Password Shown
function rpwdshows()
{
    $('#rpwd').attr("type","text")
    $('#rpwdhide').show()
    $('#rpwdshow').hide()
}
function rpwdhides()
{
    $('#rpwd').attr("type","password")
    $('#rpwdhide').hide()
    $('#rpwdshow').show()
}



// Password Eye Shown Ends

//Logout Function
function logout()
{
    $('#logmenu').show()
    $('#loginlogo').hide()
    $('#cartlogo').hide()
}

//Login Reset Button
function loginreset()
{
    var loguname = $('#loguname').val("")
    var logpwd = $('#logpwd').val("")
}

//Signup Reset Button
function resetsignup()
{
    $('#uname').val("");
    $('#email').val("");
    $('#pwd').val("");
    $('#conpwd').val("");
    $('#mno').val("");
    $('#address').val("");
    $('input[name="gender"]').prop('checked', false);
    $('input[type="checkbox"]').prop('checked', false);
    $('#nationality').prop('selectedIndex', 0);
}

//Forget Cancel Button
function forgetcancel()
{
    $('#forgetmodal').modal("hide")
    location.reload();
}


// Add Cart Function Starts
var count = 1;
var id = 1;


function cartadditems(itemName, itemPrice) {
    if($('#cartlogo:visible').length != 0)
    {
        var exist = false;
        $('.ordertable tbody tr').each(function() {
            var name = $(this).find('td:nth-child(2)').text();
            if (name == itemName) {
                exist = true;
            }
        });
        if (exist) {
            $('#cartItemCount').text(count);
            var temp = parseInt($('#' + itemName + 'quantity').val());
            temp += 1;
            $('#' + itemName + 'quantity').val(temp);
            var tempamount = parseInt($('#' + itemName + 'amount').text());
            tempamount += itemPrice;
            $('#' + itemName + 'amount').text(tempamount);
            count++;
        } else {
            $('#cartItemCount').text(count);
            var newRow = '<tr id="' + itemName + 'prod">' +
                '<td id="' + id + '">' + id + '</td>' +
                '<td id="' + itemName + '">' + itemName + '</td>' +
                '<td><input class="quantity" type="number" min="1" value="1" style="width:50px;"onkeyup="increasebtn(\'' + itemName + '\',' + itemPrice + ')" onclick="increasebtn(\'' + itemName + '\',' + itemPrice + ')" id="' + itemName + 'quantity"/></td>' +
                '<td id="' + itemName + 'price">' + itemPrice + '</td>' +
                '<td id="' + itemName + 'amount">' + itemPrice + '</td>' +
                '<td ><img src="Images/trash.svg" style = "cursor:pointer;" alt="" onclick="removecart(\'' + itemName + '\'' + ')"></td>'+
                '</tr>';
            $('.ordertable tbody').append(newRow);
            $('#cartempty').hide();
            $('#checkout').show();
            count++;
            id++;
            
        }
        var grandtotal = 0;
        $('.ordertable tbody tr').each(function() {
            var tempamount = $(this).find('td:nth-child(5)').text() * 1;
            grandtotal += tempamount;
        });
        $('#totalamount').text(grandtotal);
    }
    else
    {
        $('#addcartblock').modal('show')
    }
}

$("#dis").click(function() {
    if ($(this).prop('checked')==true) { 
        discountamount = $('#totalamount').text() * 1;
        originalAmmount = discountamount;
        percent = 0.20 * discountamount;
        discountamount -= percent;
        $('#totalamount').text(discountamount);
    }
    else
    {
        $('#totalamount').text(originalAmmount);
    }
});

function increasebtn(itemName, itemPrice) {
    var quantityCount = 0
    $('.quantity').each(function(){ 
        quantityCount = quantityCount + ($(this).val()*1)
    });
    count = quantityCount
    // console.log(quantityCount)
    $('#cartItemCount').text(count);
    var temp = parseInt($('#' + itemName + 'quantity').val());
    // temp += 1;
    // $('#' + itemName + 'quantity').val(temp);
    var tempamount = temp * itemPrice;
    $('#' + itemName + 'amount').text(tempamount);
    var grandtotal = 0;
    $('.ordertable tbody tr').each(function() {
        var tempamount = $(this).find('td:nth-child(5)').text() * 1;
        grandtotal += tempamount;
    });
    
    $('#totalamount').text(grandtotal);
    count++;
}
var grandtotal = 0
var result = [];
var quanarr = [];
var productName = [];
var granddiscount= 0; 
function checkoutitem()
{
    $('#checkoutmodal').modal('show')
    $('#orderstatus').modal('hide')
    $('.ordertable tbody tr').each(function() {
        
        
        quant = $(this).find('td:nth-child(3) input[type = "number"]').val()
        console.log(quant)
        if(quant != undefined)
        {
            quanarr.push(quant)
        }
        if($(this).find('td:nth-child(2)').text() != "")
        {
            productName.push($(this).find('td:nth-child(2)').text())
        }
    })
    let i = 0;
    let j = 0;
    while(i < productName.length)
    {
        var newCheckoutrow = '<tr>' +
                             '<td> Name: </td>' +
                             '<td>' + productName[i] + '</td>' +
                          '</tr>'+
                          '<tr>' +
                             '<td> Quantity: </td>' +
                             '<td>' + quanarr[i] + '</td>' +
                          '</tr>';
        $('.checkouttable tbody').append(newCheckoutrow)
        i++;

    }
    
    var newCheckoutrow =  '<tr>' +
                             '<td> Total Amount: </td>' +
                             '<td>' + $('#totalamount').text() + '</td>' +
                          '</tr>';
        $('.checkouttable tbody').append(newCheckoutrow)
}
function removecart(itemName)
{
    $('#'+itemName+'prod').remove()
    var quantityCount = 0
    $('.quantity').each(function(){ 
        quantityCount = quantityCount + ($(this).val()*1)
    });
    count = quantityCount
    $('#cartItemCount').text(count);
    count++;
    var grandtotal = 0;
    $('.ordertable tbody tr').each(function() {
        var tempamount = $(this).find('td:nth-child(5)').text() * 1;
        grandtotal += tempamount;
    });
    $('#totalamount').text(grandtotal);
    
}