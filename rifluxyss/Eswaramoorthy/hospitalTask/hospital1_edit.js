// working on this

var usNm;
var mbl;
var mail;
var password;
var conPass;
var gen;
var adrs;
var state;
var pin;
var opt;
var ckBx = [];
var valEmail = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$/;

console.log("as global-> " + password + " usnm-->" + usNm);

function validate() {
  usNm = $(inputName).val();
  mbl = $(inputMobileNumber).val();
  mail = $(inputEmail).val();
  password = $(inputPassword).val();
  conPass = $(inputPasswordCon).val();
  gen = $("input[name=gender]:checked").val();
  adrs = $("#address").val();
  state = $(inputCity).val();
  pin = $(inputZip).val();
  opt = $("select option:selected").val();

  ckBx = [];

  $("input[name=patTyp]:checked").each(function () {
    ckBx.push($(this).val());
  });
  console.log(ckBx);

  if (usNm === "") {
    $("#namErr")
      .css({ display: "block", color: "red" })
      .text("*Please enter your name");
    $("#inputName").focus();
  }
  // else{
  //       $("#signUP").modal("hide");
  //       $("#loginMod").modal("show");

  //       console.log(' password inside validate-> '+password+'-- '+usNm);

  //       $(inputEmail3).val(mail);
  //       $(inputPassword3).val(password);
  // }
  else {
    var valNm = /^[A-Za-z]$/;
    if (valNm.test(usNm)) {
      $("#namErr").css({ display: "none" });

      // *************************
      $("#nmDn").text(usNm);
      $("#userModalLabel").text("Welcome " + usNm);

      if (mbl === "") {
        $("#noErr")
          .css({ display: "block", color: "red" })
          .text("*Should not be empty");
        $("#inputMobileNumber").focus();
      } else {
        var valNo = /^[5-9]{1}[0-9]{9}$/;

        if (valNo.test(mbl)) {
          $("#noErr").css({ display: "none" });
          $(noDn).text(mbl);
          if (mail === "") {
            $("#mailErr")
              .css({ display: "block", color: "red" })
              .text("*Please enter an email");
            $("#inputEmail").focus();
          } else {
            if (valEmail.test(mail)) {
              $("#mailErr").css({ display: "none" });
              $(emDn).text(mail);
              if (password === "") {
                $(pasErr)
                  .css({ display: "block", color: "red" })
                  .text("*Please enter password");
                $("#inputPassword").focus();
              } else {
                var valPass =
                  /^(?=.*[A-Z])(?=.*[~`!@#$%^&*_+()-|?])(?=.*\d).{8,}$/;
                if (valPass.test(password)) {
                  $(pasErr).css({ display: "none" });

                  if (conPass === "") {
                    $(conPas)
                      .css({ display: "block", color: "red" })
                      .text("*Please enter confirm password");
                    $("#inputPasswordCon").focus();
                  } else {
                    if (conPass === password) {
                      $(conPas).css({ display: "none" });

                      if (gen == null) {
                        $(genErr)
                          .css({ display: "block", color: "red" })
                          .text("*Select the gender");
                        $("input[name=gender]").focus();
                      } else {
                        $(genErr).css({ display: "none" });
                        //console.log(gen)//getting value
                        // skill box array
                        // *************************
                        $(gDn).text(gen);

                        if (ckBx.length == 0) {
                          $(patErr)
                            .css({ display: "block", color: "red" })
                            .text("*Please give patent admit type");
                          $("input[name=patTyp]").focus();
                        } else {
                          $(patErr).css({ display: "none" });
                          console.log(ckBx); //check box value

                          // *************************
                          $(SkDn).text(ckBx);

                          if (adrs == "") {
                            $(addErr)
                              .css({ display: "block", color: "red" })
                              .text("*Enter your address");
                            $("#address").focus();
                          } else {
                            $(addErr).css({ display: "none" });
                            $(AdIn).text(adrs);

                            if (state === "") {
                              $(detErr)
                                .css({ display: "block", color: "red" })
                                .text("*Enter your state");
                              $("#inputCity").focus();
                            } else {
                              $(detErr).css({
                                display: "none",
                              });

                              // *************************
                              $(StIn).text(state);

                              if (opt === "") {
                                $(detErr)
                                  .css({ display: "block", color: "red" })
                                  .text("*Select nation");
                                $("select").focus();
                              } else {
                                // *************************
                                $(naIn).text(opt);

                                $(detErr).css({ display: "none" });

                                if (pin === "") {
                                  $(detErr)
                                    .css({ display: "block", color: "red" })
                                    .text("*Enter pin number");
                                  $("#inputZip").focus();
                                } else {
                                  var valPin = /^[0-9]+$/;
                                  // alert(valPin.test(pin))

                                  if (valPin.test(pin)) {
                                    $(detErr).css({ display: "none" });
                                    $(naDn).text(pin);
                                    // *************************

                                    $("#signUP").modal("hide");
                                    $("#loginMod").modal("show");

                                    $(inputEmail3).val(mail);
                                    $(inputPassword3).val(password);
                                  } else {
                                    $(detErr)
                                      .css({ display: "block", color: "red" })
                                      .text("*Please enter valid number");
                                    $("#inputZip").focus();
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    } else {
                      $(conPas)
                        .css({ display: "block", color: "red" })
                        .text("*Password not matching");
                      $("#inputPasswordCon").focus();
                    }
                  }
                } else {
                  $(pasErr)
                    .css({ display: "block", color: "red" })
                    .text("*Password must have a upper case and number");
                  $("#inputPassword").focus();
                }
              }
            } else {
              $("#mailErr")
                .css({ display: "block", color: "red" })
                .text("*Enter valid email");
              $("#inputEmail").focus();
            }
          }
        } else {
          $("#noErr")
            .css({ display: "block", color: "red" })
            .text("*Enter valid number");
          $("#inputMobileNumber").focus();
        }
      }
    } else {
      $("#namErr")
        .css({ display: "block", color: "red" })
        .text("*Numbers or special char are not allowed");
      $("#inputName").focus();
    }
  }
}

function updateAcc() {
  usNm = $(inputName).val();
  mbl = $(inputMobileNumber).val();
  mail = $(inputEmail).val();
  password = $(inputPassword).val();
  conPass = $(inputPasswordCon).val();
  gen = $("input[name=gender]:checked").val();
  adrs = $("#address").val();
  state = $(inputCity).val();
  pin = $(inputZip).val();
  opt = $("select option:selected").val();

  ckBx = [];

  $("input[name=patTyp]:checked").each(function () {
    ckBx.push($(this).val());
  });
  console.log(ckBx);

  if (usNm === "") {
    $("#namErr")
      .css({ display: "block", color: "red" })
      .text("*Please enter your name");
    $("#inputName").focus();
  }
  // else{
  //       $("#signUP").modal("hide");
  //       $("#loginMod").modal("show");

  //       $(inputEmail3).val(mail);
  //       $(inputPassword3).val(password);
  // }
  else {
    var valNm = /^[A-Za-z]+$/;
    if (valNm.test(usNm)) {
      $("#namErr").css({ display: "none", color: "green" }).text("valid name");

      // *************************
      $("#nmDn").text(usNm);
      $("#userModalLabel").text("Welcome " + usNm);

      if (mbl === "") {
        $("#noErr")
          .css({ display: "block", color: "red" })
          .text("should not be empty");
        $("#inputMobileNumber").focus();
      } else {
        var valNo = /^[5-9]{1}[0-9]{9}$/;

        if (valNo.test(mbl)) {
          $("#noErr")
            .css({ display: "none", color: "green" })
            .text("valid number");
          $(noDn).text(mbl);
          if (mail === "") {
            $("#mailErr")
              .css({ display: "block", color: "red" })
              .text("give an email");
            $("#inputEmail").focus();
          } else {
            var valEmail = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;

            if (valEmail.test(mail)) {
              $("#mailErr")
                .css({ display: "none", color: "green" })
                .text("valid email");
              $(emDn).text(mail);
              if (false) {
              } else {
                if (true) {
                  $(pasErr)
                    .css({ display: "none", color: "green" })
                    .text("valid password");

                  if (false) {
                  } else {
                    if (true) {
                      $(conPas)
                        .css({ display: "none", color: "green" })
                        .text("password matches");

                      if (gen == null) {
                        $(genErr)
                          .css({ display: "block", color: "red" })
                          .text("select gender");
                        $("input[name=gender]").focus();
                      } else {
                        $(genErr)
                          .css({ display: "none", color: "green" })
                          .text("selected");
                        //console.log(gen)//getting value
                        // skill box array
                        // *************************
                        $(gDn).text(gen);

                        if (ckBx.length == 0) {
                          $(patErr)
                            .css({ display: "block", color: "red" })
                            .text("enter patent admit type");
                          $("input[name=patTyp]").focus();
                        } else {
                          $(patErr).css({ display: "none", color: "green" });
                          console.log(ckBx); //check box value

                          // *************************
                          $(SkDn).text(ckBx);

                          if (adrs == "") {
                            $(addErr)
                              .css({ display: "block", color: "red" })
                              .text("enter your address");
                            $("#address").focus();
                          } else {
                            $(addErr).css({ display: "none" });
                            $(AdIn).text(adrs);

                            if (state === "") {
                              $(detErr)
                                .css({ display: "block", color: "red" })
                                .text("enter your state");
                              $("#inputCity").focus();
                            } else {
                              $(detErr).css({
                                display: "none",
                                color: "green",
                              });

                              // *************************
                              $(StIn).text(state);

                              if (opt === "") {
                                $(detErr)
                                  .css({ display: "block", color: "red" })
                                  .text("select nation");
                                $("select").focus();
                              } else {
                                // *************************
                                $(naIn).text(opt);

                                $(detErr)
                                  .css({ display: "none", color: "green" })
                                  .text("selected");

                                if (pin === "") {
                                  $(detErr)
                                    .css({ display: "block", color: "red" })
                                    .text("enter pin number");
                                } else {
                                  var valPin = /^[0-9]+$/;
                                  // alert(valPin.test(pin))

                                  if (pin.length != 6) {
                                    $(detErr)
                                      .css({ display: "block", color: "red" })
                                      .text("enter valid pin number");
                                    $("#inputZip").focus();
                                  }

                                  if (valPin.test(pin)) {
                                    $(detErr)
                                      .css({ display: "none", color: "green" })
                                      .text("selected");
                                    // *************************
                                    $(naDn).text(pin);

                                    $("#signUP").modal("hide");
                                    $("#userModal").modal("show");

                                    $(inputEmail3).val(mail);
                                    $(inputPassword3).val(password);
                                  } else {
                                    $(detErr)
                                      .css({ display: "block", color: "red" })
                                      .text("enter valid number");
                                    $("#inputZip").focus();
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    } else {
                    }
                  }
                } else {
                }
              }
            } else {
              $("#mailErr")
                .css({ display: "block", color: "red" })
                .text("invalid email");
              $("#inputEmail").focus();
            }
          }
        } else {
          $("#noErr")
            .css({ display: "block", color: "red" })
            .text("Enter valid number");
          $("#inputMobileNumber").focus();
        }
      }
    } else {
      $("#namErr")
        .css({ display: "block", color: "red" })
        .text("should not have numbers or special char");
      $("#inputName").focus();
    }
  }
}

function updatePass() {
  var oldPass = $("#oldPassword").val();
  var newPass = $(ChaNewPassword).val();
  var conPass = $(newPasswordCon).val();

  var valPass = /^(?=.*[A-Z])(?=.*[~`!@#$%^&*_+()-|?])(?=.*\d).{8,}$/;

  if (oldPass != password) {
    $(oldPasErr)
      .css({ display: "block", color: "red" })
      .text("*enter correct password");
  } else if (newPass.length == "") {
    $(oldPasErr).css({ display: "none" });
    $(newPasErr)
      .css({ display: "block", color: "red" })
      .text("*enter password");
  } else if (newPass.length < 8) {
    $(newPasErr)
      .css({ display: "block", color: "red" })
      .text("*length must be min character");
  } else if (!valPass.test(newpass)) {
    $(newPasErr)
      .css({ display: "block", color: "red" })
      .text("password must have a upper case ,number");
  }
  //confirm password
  else if (conPass.length == "") {
    $(newPasErr).css({ display: "none" });
    $(newConPasErr)
      .css({ display: "block", color: "red" })
      .text("*enter confirm password");
  } else if (conPass != newPass) {
    $(newPasErr).css({ display: "none" });
    $(newConPasErr)
      .css({ display: "block", color: "red" })
      .text("*password miss match");
  } else {
    $(newPasErr).css({ display: "none" });
    $(newPasErr).css({ display: "none" });
    $(newConPasErr).css({ display: "none" });

    password = newPass;
    $("#chaPassMod").modal("hide");
  }
}

function loginBtn() {
  var mailCheck = $("#inputEmail3").val();
  var passCheck = $("#inputPassword3").val();

  if (mailCheck == "") {
    $("#logMail")
      .css({ display: "block", color: "red" })
      .text("*please enter email");
  }
  // else if (!valEmail.test(mailCheck)) {
  //   $("#logMail")
  //     .css({ display: "block", color: "red" })
  //     .text("*please enter valid email");
  // }
  else if (passCheck == "") {
    $("#logMail").css({ display: "none" });
    $("#logPass")
      .css({ display: "block", color: "red" })
      .text("*please enter password");
  } else {
    $("#logPass").css({ display: "none" });

    $("#loginBtn").css({ display: "none" });
    $("#loginMod").modal("hide");
    $(".account").css({ display: "block" });
  }
}

function logOutBtn() {
  $("#loginBtn").css({ display: "block" });
  $(".account").css({ display: "none" });
  $(".hideOnLogin").css({ display: "block" });
  $("#updBtn").css({ display: "none" });
}

function editUser() {
  $(".hideOnLogin").css({ display: "none" });
  $(updBtn).css({ display: "flex" });
}

function togglePassword(element) {
  var $passwordField = $(element).closest(".pasHei").find(".form-control");
  var $toggleIcon = $(element).find("img");
  var fieldType = $passwordField.attr("type");
  //
  if (fieldType === "password") {
    $passwordField.attr("type", "text");
    $toggleIcon.attr({ src: "images/passShow-4.png", alt: "show" });
  } else {
    $passwordField.attr("type", "password");
    $toggleIcon.attr({ src: "images/passHIde-4.png", alt: "hide" });
  }
}

/**************** booking functions ****************/

var s_no = 1;

var newApp = true;

function Booking(id, DrNm) {
  var date = $(id + ' input[name="day"]').val();
  var time = $(id + ' input[name="time"]').val();
  var pur = $(id + " select option:selected").val();
  var paTy = [];

  $(id + " input[name=patTyp]:checked").each(function () {
    paTy.push($(this).val());
  });

  var tbId = id + date + time;

  if (date == "") {
    $(".rDaErr")
      .css({ display: "block", color: "red" })
      .text("*please select a date");
  } else if (time == "") {
    $(".rDaErr").css({ display: "none" });

    $(".rTiErr")
      .css({ display: "block", color: "red" })
      .text("*please enter time");
  } else if (paTy.length == 0) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });

    $(".typErr")
      .css({ display: "block", color: "red" })
      .text("*please select type");
  } else if (pur == "" || pur === undefined) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });

    $(".purErr")
      .css({ display: "block", color: "red" })
      .text("*please select purpose");
  } else {
    $(id).modal("hide");
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });
    $(".purErr").css({ display: "none" });

    $("#adDetails tbody tr").each(function () {
      var tsTrId = $(this).attr("id");

      if (tsTrId === tbId) {
        newApp = false;
        alert(
          "already there is an appointment please choose another time slot"
        );
        return false;
      } else {
        newApp = true;

        return false;
      }
    });

    if (newApp) {
      var row =
        `<tr id="` +
        tbId +
        `"><td>` +
        s_no +
        `</td><td>` +
        DrNm +
        `</td><td>` +
        paTy +
        `</td><td>` +
        pur +
        `</td><td class="appDt bg">` +
        date +
        `</td><td>` +
        time +
        `</td>` +
        `<td class="center"><button data-bs-toggle="modal"
    data-bs-target="` +
        id +
        `"
        onclick= "ToBook('` +
        id +
        `','#` +
        s_no +
        `')"  class="delApp" >edit</button><button  class="delApp" >delete</button></td></tr>`;

      $("#adDetails").css({ display: "block" });
      $(".noApp").css({ display: "none" });
      $("#adDetails>tbody").append(row);

      //button hide and show

      $(id + " .book").css({ display: "block" });

      var btn =
        ` <button type="button" class="btn btn-primary" id=` +
        s_no +
        ` style="display: none;" onclick= editBooking(` +
        s_no +
        ",'" +
        id +
        `','` +
        DrNm +
        `','` +
        date +
        `','` +
        time +
        `') >Update</button>`;
      $(id + " .modal-footer").append(btn);

      $(id + " .edit")
        .css({ display: "none" })
        .attr(
          "onclick",
          `editBooking(` +
            s_no +
            ",'" +
            id +
            `','` +
            DrNm +
            `','` +
            date +
            `','` +
            time +
            `')`
        );

      // s.no increment
      s_no++;
    }
  }
}

$("body").on("click", " .delApp", function () {
  $(this).parents("tr").remove();

  var rowCount = $("#adDetails >tbody >tr").length;
  if (rowCount == 0) {
    $("#adDetails").css({ display: "none" });
    $(".noApp").css({ display: "block" });
  }
});

function ToBook(id, s_no) {
  $(id + " .book").css({ display: "none" });
  $(s_no).css({ display: "block" });
}

function editBooking(s_no, id, DrNm, oldDate, oldTime) {
  //update book

  var date = $(id + ' input[name="day"]').val();
  var time = $(id + ' input[name="time"]').val();
  var pur = $(id + " select option:selected").val();
  var paTy = [];

  $(id + " input[name=patTyp]:checked").each(function () {
    paTy.push($(this).val());
  });

  var tbId = id + date + time;

  //checking input statement
  if (date == "") {
    $(".rDaErr")
      .css({ display: "block", color: "red" })
      .text("*please select a date");
  } else if (time == "") {
    $(".rDaErr").css({ display: "none" });

    $(".rTiErr")
      .css({ display: "block", color: "red" })
      .text("*please enter time");
  } else if (paTy.length == 0) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });

    $(".typErr")
      .css({ display: "block", color: "red" })
      .text("*please select type");
  } else if (pur == "" || pur === undefined) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });

    $(".purErr")
      .css({ display: "block", color: "red" })
      .text("*please select purpose");
  } else {
    $(id).modal("hide");
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });
    $(".purErr").css({ display: "none" });

    //button
    $(id + " .book").css({ display: "block" });
    $(id + " .edit").css({ display: "none" });

    $("#adDetails tbody tr").each(function () {
      var tsTrId = $(this).attr("id");

      if (tsTrId === tbId) {
        newApp = false;
        alert(
          "already there is an appointment please choose another time slot"
        );
        return false;
      } else {
        newApp = true;

        return false;
      }
    });

    if (newApp) {
      var row =
        `<tr id="` +
        tbId +
        `"><td>` +
        s_no +
        `</td><td>` +
        DrNm +
        `</td><td>` +
        paTy +
        `</td><td>` +
        pur +
        `</td><td class="appDt bg">` +
        date +
        `</td><td>` +
        time +
        `</td>` +
        `<td class="center"><button data-bs-toggle="modal"
    data-bs-target="` +
        id +
        `"
        onclick= "ToBook('` +
        id +
        `','#` +
        s_no +
        `')"  class="delApp" >edit</button><button  class="delApp" >delete</button></td></tr>`;

      $("#adDetails").css({ display: "block" });
      $(".noApp").css({ display: "none" });
      $("#adDetails>tbody").append(row);
    } else {
      var row =
        `<tr id="` +
        tbId +
        `"><td>` +
        s_no +
        `</td><td>` +
        DrNm +
        `</td><td>` +
        paTy +
        `</td><td>` +
        pur +
        `</td><td class="appDt bg">` +
        oldDate +
        `</td><td>` +
        oldTime +
        `</td>` +
        `<td class="center"><button data-bs-toggle="modal"
    data-bs-target="` +
        id +
        `"
        onclick= "ToBook('` +
        id +
        `','#` +
        s_no +
        `')"  class="delApp" >edit</button><button  class="delApp" >delete</button></td></tr>`;

      $("#adDetails").css({ display: "block" });
      $(".noApp").css({ display: "none" });
      $("#adDetails>tbody").append(row);
    }

    //button hide and show

    $(id + " .book").css({ display: "block" });
    $("#" + s_no)
      .css({ display: "none" })
      .attr(
        "onclick",
        `editBooking(` +
          s_no +
          ",'" +
          id +
          `','` +
          DrNm +
          `','` +
          date +
          `','` +
          time +
          `')`
      );
  }
}
