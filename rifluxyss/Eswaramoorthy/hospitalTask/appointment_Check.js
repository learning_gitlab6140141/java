function editBooking(modId, DrNm) {
  var id = "#" + modId;
  var date = $(id + ' input[name="day"]').val();
  var time = $(id + ' input[name="time"]').val();
  var pur = $(id + " .selPur option:selected").val();
  var paTy = [];
  
  $(id + " input[name=patTyp]:checked").each(function () {
    paTy.push($(this).val());
  });

  var tbId = "tr" + modId + date + time;

  if (date == "") {
    $(".rDaErr")
      .css({ display: "block", color: "red" })
      .text("*please select a date");
  } else if (time == "") {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr")
      .css({ display: "block", color: "red" })
      .text("*please enter time");
  } else if (paTy.length == 0) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr")
      .css({ display: "block", color: "red" })
      .text("*please select type");
  } else if (pur == "" || pur === undefined) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });
    $(".purErr")
      .css({ display: "block", color: "red" })
      .text("*please select purpose");
  } else {
    $(id).modal("hide");
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });
    $(".purErr").css({ display: "none" });

    // Find the index of the table row to edit
    var index = $("#adDetails tbody tr").index($("#" + tbId));

    // Update the table row with new values
    $("#adDetails tbody tr:eq(" + index + ")")
      .find(".drName")
      .text(DrNm);
    $("#adDetails tbody tr:eq(" + index + ")")
      .find(".patType")
      .text(paTy.join(", "));
    $("#adDetails tbody tr:eq(" + index + ")")
      .find(".purpose")
      .text(pur);
    $("#adDetails tbody tr:eq(" + index + ")")
      .find(".date")
      .text(date);
    $("#adDetails tbody tr:eq(" + index + ")")
      .find(".time")
      .text(time);
  }
}
