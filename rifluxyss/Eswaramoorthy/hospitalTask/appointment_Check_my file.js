function Booking(modId, DrNm) {
  // console.log("print id from  ", modId);
  var id = "#" + modId;

  var tbId = ""+modId+date+time;

  // console.log("id createsd on booking tabel", tbId);

  var date = $(id + ' input[name="day"]').val();
  var time = $(id + ' input[name="time"]').val();
  var pur = $(id + " .selPur option:selected").val();
  var paTy = [];

  $(id + " input[name=patTyp]:checked").each(function () {
    paTy.push($(this).val());
  });
  // alert(paTy);
  // alert(pur);

  var tbId = "tr" + modId;

  if (date == "") {
    $(".rDaErr")
      .css({ display: "block", color: "red" })
      .text("*please select a date");
  } else if (time == "") {
    $(".rDaErr").css({ display: "none" });

    $(".rTiErr")
      .css({ display: "block", color: "red" })
      .text("*please enter time");
  } else if (paTy.length == 0) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });

    $(".typErr")
      .css({ display: "block", color: "red" })
      .text("*please select type");
  } else if (pur == "" || pur === undefined) {
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });

    $(".purErr")
      .css({ display: "block", color: "red" })
      .text("*please select purpose");
  } else {
    $(id).modal("hide");
    $(".rDaErr").css({ display: "none" });
    $(".rTiErr").css({ display: "none" });
    $(".typErr").css({ display: "none" });
    $(".purErr").css({ display: "none" });

    // if()

    var row =
      <tr id=" +
      (tbId+date+time) +
      "><td class="sNoBk"> +
      s_no++ +
      </td><td> +
      DrNm +
      </td><td> +
      paTy +
      </td><td> +
      pur +
      </td><td class="date"> +
      date +
      </td><td class="time"> +
      time +
      </td> +
      `<td class="edDel"><button class="delApp" data-bs-toggle="modal"
    data-bs-target="` +
      id +
      "onclick="editBk(+tbId+)">edit</button><button class="delApp">delete</button></td></tr>;

    $("#adDetails").css({ display: "block" });
    $(".noApp").css({ display: "none" });
    $("#adDetails>tbody").append(row);

    // var rowCount = $('#adDetails >tbody >tr').length;
    // console.log('length',rowCount)
  }
}
