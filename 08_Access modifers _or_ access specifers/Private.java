package pack.accessMod;

public class Private
{
	
	private Private()
	{
		System.out.println("\nconstructor is called only inside this class");
	}
	
	public static void main (String[] args)
	{
		Private obj = new Private();
		obj.call();
	}
	
	private void call()
	{
		System.out.println("\nmethod called only inside this class");
	}
}