public interface Rules
	// same rules as class
	//interface method by default they are abstract
	//variables are by default finals and static
	//interface is a contract or rules
	//no constructors
{
	int no_of_leave = 15; //in interface by default final and static
	int min_salary=15000;
	
/*  = expected
	int no_of_leave;
	               ^    
	
	error: Rules is abstract; cannot be instantiated
	Rules ru=new Rules();
	         ^            */

	
	public void comeOnTime(); //in interface by defult abstract
	public void takeLeave();
	public void getSalery();
	/*interface abstract methods cannot have body
	public void getSalery(){}
*/
}