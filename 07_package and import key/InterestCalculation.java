package com.indianBank.deposit;

public class InterestCalculation
{
	String name;
	
	public InterestCalculation(String name)
	{
		this.name=name;
		System.out.println("\nconstructor output");
		System.out.println("Interest-calculation for:-"+name);
	}
	
	public static void main(String[] arg)
	{
		//System.out.println ("Indian-Bank");
		InterestCalculation ic = new InterestCalculation("mani");
		ic.calculation();
		
	}
	
	public void calculation()
	{
		System.out.println("\nmethod output");
		System.out.println("Interest-calculation for:-"+name);
	}
}


/*
***output***
constructor output
Interest-calculation for:-mani

method output
Interest-calculation for:-mani
*/