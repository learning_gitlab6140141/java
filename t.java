public class BookShop1
{
  String name;
  //Constructor overLoading
public BookShop1(int no) //arguments
{
  System.out.println("Are you constructor? "+ no);  
}
//no-arg constructor  
public BookShop1()
{
  
}
public BookShop1(String s){
  name=s;
  //System.out.println(name);
}
public static void main(String[] args)
{
  BookShop1 book1 = new BookShop1(10); //parameter
  BookShop1 book2 = new BookShop1();
  
  BookShop1 book3 = new BookShop1("ABC");
  System.out.println(book3.name);
  book3.name="xyz";  
  System.out.println(book3.name);
}


}