package synchronizedMul;

public class SynchMulti {

	public static void main(String[] args) {
		Tables t = new Tables();

		Thread tr = new Thread() {
			public void run() {
				t.printTable(5);
			}

		};

//		Thread tr2= new Thread() {
//			public void run() {
//				t.printTable(2);
//			}
//			
//		};
//		Thread tr2= new Thread(  ()->  {t.printTable(2);  });
		Thread tr2 = new Thread(() -> t.printTable(2));

		tr.start();

		tr2.start();

		System.out.println("main");

	}

}
