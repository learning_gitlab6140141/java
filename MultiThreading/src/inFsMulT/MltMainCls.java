package inFsMulT;

public class MltMainCls {

	public static void main(String[] args) {
		extCls mc = new extCls();
		mc.start();

		/*********************
		 * has only one runable method so change to nnonomus class
		 *********/

		Runnable in = new ImpleCls(); // java.lang.ClassCastException
		Thread t1 = new Thread(in);

//		in.start();//interface have no start method just abstract run met.. to overcome create a object for thread class 
		t1.start();

		/**************
		 * from seeing above method we feel waste of one class so use as inner class
		 ****************/

//		Runnable in = new Runnable() {
//
//			@Override
//			public void run() {
//				int i = 0;
//				for (int a = 0; a < 6; a++) {
//					System.out.println((i += 100) + " interface");
//					try {
//						Thread.sleep(i);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//
//			}
//		};

		/************** above inner class can be even cutshot to lamda ****************/
//		Runnable in = () -> {
//			{
//				int i = 0;
//				for (int a = 0; a < 6; a++) {
//					System.out.println((i += 100) + " interface");
//					try {
//						Thread.sleep(i);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//
//			}
//		};
		
		
		Runnable in1 = MltMainCls::MulTst;
		Thread t=new Thread(in1);
		
		t.start();

		try {
			mc.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("end");

	}
	
	public static void MulTst() {
		int i = 0;
		for (int a = 0; a < 6; a++) {
			System.out.println((i += 100) + " separate method");
			try {
				Thread.sleep(i);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
