package learnthreads;

public class TreadDemo {

  public static void main(String[] args) {
      
    TreadChild ts = new TreadChild();
    ts.start();
    
    TreadChild ts2 = new TreadChild();
    ts2.start();
    System.out.println(ts.getId());
    System.out.println(ts2.getId());
    
    System.out.println(ts.getName());
    System.out.println(ts2.getName());
    
    System.out.println(ts.getPriority());
    System.out.println(ts2.getPriority());
    
    System.out.println(ts.isAlive());
    System.out.println(ts2.isAlive());
    
    System.out.println(ts.isDaemon());
    System.out.println(ts2.isDaemon());
    
    TreadDemo.loop();
  }
  public static void loop(){
    for(int i=1;i<=5;i++) {
      System.out.println("TreadDemo "+i);
    }
  }

  

}