package learnthreads;

public class ThreadDemo2 {

	public static void main(String[] args) throws InterruptedException {
		ThreadChild2  ch2=new ThreadChild2 ();
		System.out.println("after new *********"+ch2.getState());
		
		ch2.start();
		
//		ch2.join();
		ch2.interrupt();
		
		for (int i=0;i<5;i++) {
			System.out.println("b4 td *********"+ch2.getState());
			System.out.println("TD "+i);
			System.out.println("after  td *********"+ch2.getState());
		}
		
		System.out.println("after  loop *********"+ch2.getState());
	}

}
