package methodRefOrDoubleColenOpe;

public class DoubleCo {
	
	public DoubleCo(){
		System.out.println("\nno arg const ");
	}
	
	public DoubleCo(int no){
		System.out.println("\nconstructor ==> " +no);
	}
	
	public static void main(String[] args) {
		DoubleCo dc=new DoubleCo();
		Contract c0=no-> System.out.println(no);
		c0.display(190);
		Contract c=System.out::println;
		c.display(100);
		
		Contract c1=dc::show;	//non-static method
		c1.display(20);
		
		
		Contract c2= DoubleCo::show1;	//static method,orbitary
		c2.display(67);
		
		Contract c3=new DoubleCo()::show;;	//non-static method
		c3.display(44);
		
		Contract c4=DoubleCo::new;; //calling constructor
		c4.display(29);
	}
	
	public void show(int no) {
		System.out.println("\nnon-static method ==> "+ no);
	}
	
	public static void show1(int no) {
		System.out.println("\nstatic method ==> "+ no);
	}

}
