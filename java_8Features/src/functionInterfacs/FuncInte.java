package functionInterfacs;
import java.util.ArrayList;
import java.util.function.*;

public class FuncInte {

	public static void main(String[] args) {
		Function<String,Integer> f = (str)->str.length();
		System.out.println(f.apply("selvakumar"));
//		public int apply(String str) {
//		return str.length();
//	}
		
		ArrayList<Integer> al=new ArrayList<Integer>();
		al.add(100);
		al.add(50);
		al.add(600);
		al.add(300);
		al.add(32);
		
		al.forEach(num->System.out.println(num));
		
		Consumer<Integer> c=n -> System.out.println("Consumer "+n);
		c.accept(34);
		
//		External looping
//		System.out.println("for each method");
//		for (Integer in : al) {
//			System.out.println(in);
//		}
//		System.out.println("\nindex bases");
//		for(int i=0;i<al.size();i++)
//			System.out.println(al.get(i));

	}


}
