package default_static;


import java.util.function.Predicate;

public class predicat2Demo {
	public static void main(String[] args) {

//  Predicate<Integer> p = marks -> (marks>=35);
//  System.out.println(p.test(36));

//  Predicate<String> p = arg->(arg.length()>5); 
//  System.out.println(p.test("selva"));

  int[] ar = { 10, 30, 34, 20, 67, 56 };

  Predicate<Integer> p = (no) -> (no % 2 == 0);
  Predicate<Integer> p2 = (no) -> (no > 30);

  predicat2Demo pd = new predicat2Demo();
  pd.check(p2.negate(), ar);//negate reverse
  pd.check(p2.and (p), ar);
  pd.check(p2.or (p), ar);
  
  

//  ArrayList al = new ArrayList();
//  al.add("h");
//
//  Predicate<Collection> p2 = (al2) -> (al2.size() > 0);
//  System.out.println(p2.test(al));

//  PedicateDemo pd = new PedicateDemo();
//  System.out.println(pd.check(45));
 }

 private void check(Predicate<Integer> p, int[] ar) {
  for (int i = 0; i < ar.length; i++) {
   System.out.println(p.test(ar[i]));
  }
  
 }

// public boolean check(int marks) {
//  if(marks>=35) {
//   return true;
//  }
//  else
//   return false;
// }

}