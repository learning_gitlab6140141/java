package default_static;

public class Worker implements Contract,Contract2 {

	public static void main(String[] args) {
		Contract c=new Worker();
//		System.out.println("-----");
		c.calculation(200,400);//dynamic binding
		
		Worker w=new Worker();
//		System.out.println("++++++++++");
		w.calculation(100,200);
	
		
	}

	@Override
	public void calculation(int no1, int no2) {
		// TODO Auto-generated method stub
//		System.out.println("-----");
		Contract.super.calculation(no1, no2);
//		System.out.println("++++++++++");
		Contract2.super.calculation(no1, no2);
	}

}
