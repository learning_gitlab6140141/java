package default_static;

import java.util.function.Predicate;

public class PredicateDemo {

//	public static void main(String[] args) {
//		
////		Predicate<Integer> p= (mark)-> (mark>=35);
////		System.out.println(p.test(44));
//		
//		
//		/*String comparation*/
//		Predicate<String> s= arg -> (arg.length()>5);
//		System.out.println(s.test("peater"));
//		
//		/*array*/
////		int[] ar= {12,45,67,86,66};
////		Predicate<Integer> p= no-> (no%2==0);{
////			for (int i=0;i<ar.length;i++)
////				System.out.println(ar[i]+" is "+p.test(ar[i]));
////		}
//		
//		 int[] ar = { 10, 30, 34, 20, 67, 56 };
//
//		  Predicate<Integer> p = (no) -> (no % 2 == 0);
//		  Predicate<Integer> p2 = (no) -> (no % 2!= 0);
//
//		  PredicateDemo pd = new PredicateDemo ();
//		  //pd.check(p,ar);
//		  pd.check(p, ar);
//		//  ArrayList al = new ArrayList();
//		//  al.add("h");
//		//
//		//  Predicate<Collection> p2 = (al2) -> (al2.size() > 0);
//		//  System.out.println(p2.test(al));
//
//		//  PedicateDemo pd = new PedicateDemo();
//		//  System.out.println(pd.check(45));
//		 }
//		  
//		  
//		
//	}
////ArrayList al = new ArrayList();
////al.add("h");
////
////Predicate<Collection> p2 = (al2) -> (al2.size() > 0);
////System.out.println(p2.test(al));
//
////PedicateDemo pd = new PedicateDemo();
////System.out.println(pd.check(45));
//}
//
//private void check(Predicate<Integer> p, int[] ar) {
//for (int i = 0; i < ar.length; i++) {
//System.out.println(p.test(ar[i]));
//}
//
//	
//		/*top two lines do this 
//		PredicateDemo pd=new PredicateDemo();
//		System.out.println(pd.check(30));
//	}
//	
//	public boolean check (int marks) {
//		if(marks>=35)
//			return true;
//		return false;
//	} */
//}
	public static void main(String[] args) {

	//  Predicate<Integer> p = marks -> (marks>=35);
	//  System.out.println(p.test(36));

	//  Predicate<String> p = arg->(arg.length()>5); 
	//  System.out.println(p.test("selva"));

	  int[] ar = { 10, 30, 34, 20, 67, 56 };

	  Predicate<Integer> p = (no) -> (no % 2 == 0);
	  Predicate<Integer> p2 = (no) -> (no >30);

	  PredicateDemo pd = new PredicateDemo();
	  //pd.check(p,ar);
//	  pd.check(p, ar);
	  pd.check(p2.negate(), ar);//negate reverse
//	  pd.check(p2.and (p), ar);
//	  pd.check(p2.or (p), ar);
	  
	  

	//  ArrayList al = new ArrayList();
	//  al.add("h");
	//
	//  Predicate<Collection> p2 = (al2) -> (al2.size() > 0);
	//  System.out.println(p2.test(al));

	//  PedicateDemo pd = new PedicateDemo();
	//  System.out.println(pd.check(45));
	 }

	 private void check(Predicate<Integer> p, int[] ar) {
	  for (int i = 0; i < ar.length; i++) {
	   System.out.println(p.test(ar[i]));
	  }
	  
	 }

	// public boolean check(int marks) {
	//  if(marks>=35) {
	//   return true;
	//  }
	//  else
	//   return false;
	// }

	}
