package streamAPI;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class StreamDemo2 {

  public static void main(String[] args) {
    
    int[] ar = {89,63,75,83,64};
    System.out.println(ar.length);
    
    IntStream s = Arrays.stream(ar);
    long count = s.count();
    System.out.println(count);
    
//    IntStream s1 = Arrays.stream(ar);
//    s1 = s1.sorted();
    
    
//    OptionalDouble od= Arrays //1
//    .stream(ar)//intermediate operations
//    
//    .average();//intermediate operations //1
//
//    System.out.println(od.isPresent()); //1
//    System.out.println(od.getAsDouble()); //1
    
//    count

//    long od = Arrays
//      .stream(ar)//intermediate operations
//      .count();//intermediate operations
//      //.forEach(System.out::println);//Terminal operations
//      System.out.println(od);

//    average

//    OptionalDouble od = Arrays
//      .stream(ar)//intermediate operations
//      .average();//intermediate operations
      //.forEach(System.out::println);//Terminal operations
//      System.out.println(od.isPresent());
//      System.out.println(od.getAsDouble());
      
//      

//    		  max
//
//    		  OptionalInt od = Arrays
//    		    .stream(ar)//intermediate operations
//    		    .max();//intermediate operations
//    		    //.forEach(System.out::println);//Terminal operations
//    		    System.out.println(od.isPresent());
//    		    System.out.println(od.getAsInt());


//    		  min
//

    		  OptionalInt od = Arrays
    		    .stream(ar)//intermediate operations
    		    .min();//intermediate operations
    		    //.forEach(System.out::println);//Terminal operations
    		    System.out.println(od.isPresent());
    		    System.out.println(od.getAsInt());
    		    
//    		    		distinct	
    		    		Arrays
    		    		  .stream(ar)//intermediate operations
    		    		  .distinct()
    		    		  .forEach(System.out::print);
    

    		    		//findFirst

    		    		OptionalInt od1 = Arrays
    		    		  .stream(ar)//intermediate operations
    		    		  .findFirst();//intermediate operations
    		    		  //.forEach(System.out::println);//Terminal operations
    		    		  System.out.println(od1.isPresent());
    		    		  System.out.println(od1.getAsInt());
    

    		    		//findAny

    		    		OptionalInt od3 = Arrays
    		    		  .stream(ar)//intermediate operations
    		    		  .findAny();//intermediate operations
    		    		  //.forEach(System.out::println);//Terminal operations
    		    		  System.out.println(od3.isPresent());
    		    		  System.out.println(od3.getAsInt());

  }

  
}