package streamAPI;

import java.util.Arrays;
import java.util.stream.IntStream;

public class StreamDemo {

  public static void main(String[] args) {
    
    int[] ar = {82,63,75,83,64};
//    System.out.println(ar.length);
    
//    IntStream s = Arrays.stream(ar);
//    long count = s.count();
//    System.out.println(count);
    
//    IntStream s1 = Arrays.stream(ar);
//    s1 = s1.sorted();
    Arrays
    .stream(ar)                        //intermediate operations
    .sorted()                          //intermediate operations
    .forEach(System.out::println);     //terminal operations

  }

  
}