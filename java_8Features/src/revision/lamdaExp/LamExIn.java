package revision.lamdaExp;

@FunctionalInterface
public interface LamExIn {
    void cl(int a, String q);

    default void calc(int a, int b) {
        System.out.println("sub " + (a - b));
    }

    static int ad3(int a) {
        return a + 3;
    }
}

