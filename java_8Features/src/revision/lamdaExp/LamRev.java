package revision.lamdaExp;

public class LamRev implements LamExIn, LamExIn2 {

    public static void main(String[] args) {
        LamExIn lei = (n, d) -> System.out.println("called " + n + " " + d);
        lei.cl(4, "hello");

        // Corrected method reference
//        LamExIn lei1 = System.out::println;
//        lei1.cl(4, "hello");

        LamExIn lr = new LamRev();
        lr.calc(5, 7);

        System.out.println("ad3 static method " + LamExIn.ad3(5));
    }

    @Override
    public void calc(int a, int b) {
        LamExIn.super.calc(a, b);
        LamExIn2.super.calc(a, b);//why super
    }

    @Override
    public void cl(int a, String q) {
        // Implementation for cl method
    }
}