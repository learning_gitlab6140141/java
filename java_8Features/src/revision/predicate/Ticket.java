package revision.predicate;

import java.util.function.Predicate;

public class Ticket {

	public static void main(String[] args) {
		Predicate<Integer> p = (no) -> (no > 60);
		System.out.println("~~~~~~~~~~~~~~~ "+p.test(70) +" ~~~~~~~~~~~~~~~");

		int[] ar = { 10, 45, 6, 68, 9 };
//		System.out.println("~~~~~~~~~~~~~~~eve~~~~~~~~~~~~~~~~~");
		Predicate<Integer> pa1 = no -> no % 2 == 0;
//		System.out.println("~~~~~~~~~~~~~~~greater~~~~~~~~~~~~~~~~~");
		Predicate<Integer> pa2 = no -> (no >= 20);

		for (int i : ar) {
			System.out.println(pa1.test(i));
		}
		System.out.println("~~~~~~~~~~~~~~~eve~~~~~~~~~~~~~~~~~");
		check(pa1, ar);
		System.out.println("~~~~~~~~~~~~~~~greater~~~~~~~~~~~~~~~~~");
		check(pa2, ar);
		
		System.out.println("~~~~~~~~~~~~~~~pa1.or(pa2),ar~~~~~~~~~~~~~~~~~");
		
		check(pa1.or(pa2),ar);
		
		System.out.println("~~~~~~~~~~~~~~~~~and()~~~~~~~~~~~~~~~");
		check(pa2.and(pa1), ar);
		
		System.out.println("~~~~~~~~~~~~~~~negate()~~~~~~~~~~~~~~~~~");
		check(pa2.negate(), ar);

		
		System.out.println("~~~~~~~~~and()~~~~~negate()~~~~~~~~~~");
		check(pa2.and(pa1).negate(), ar);
	}


	private static void check(Predicate<Integer> p, int[] ar) {
		for (int i : ar) {
//			System.out.println(p.test(i));
			if (p.test(i))
				System.out.println(i);
		}
	}

}
