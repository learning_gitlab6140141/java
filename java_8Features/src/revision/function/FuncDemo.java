package revision.function;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class FuncDemo {

	public static void main(String[] args) {
		Function<String, Integer> f = name -> name.length();
		System.out.println(f.apply("faf iaoi"));

		List<Integer> va = Arrays.asList(10, 23, 21, 10, 15, 9);
		System.out.println(va);
//refer inner_class
	System.out.println("changing to lamda");	
		va.forEach(ele -> System.out.println(ele));
		
		System.out.println("changing to lamda with condition");	
		
		va.forEach(ele -> {
			if(ele>20)
			System.out.println(ele);
		});

	}

}
