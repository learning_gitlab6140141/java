package revision.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

public class InnCls implements Contra {

	public static void main(String[] args) {
		// lamda
//		Contra c=(a)->{for (int i=0;i<=4;i++) {
//				System.out.println(i+") hi"); } };
//
//		c.ad1(6);

		// lamda
		Contra nc = x -> System.out.println("hello "+x);

		nc.ad1(30);
// above lamda expression is explained briefly below
		Contra nc1 = new Contra() {
			
			@Override
			public void ad1(int a) {
				System.out.println("num "+(a));
				
			}
		};

		nc.ad1(4);
		nc1.ad1(6);
		
		
		
		List<Integer> va = Arrays.asList(10, 23, 21, 10, 15, 9);
		Contra co = (a) -> System.out.println("\nbelow list "+a);

		co.ad1(30);
			
		va.forEach(ele -> System.out.println(ele));
//		
//		va.forEach(co-> System.out.println(co));
//		
		

	}

	@Override
	public void ad1(int a) {
		// TODO Auto-generated method stub
		
	}


}
