package revision.doubleColen;

import java.util.ArrayList;

public class Demo {

	public static void main(String[] args) {
		ArrayList al=new ArrayList();
		al.add(23);
		al.add(45);
		al.add(66);
		al.add(74);
		al.forEach(v -> System.out.println(v));// here ve use accept() from consumer interface
		
		System.out.println("\ndouble colen operator or method reference\n");
		
		al.forEach(System.out::println);
	}

}
