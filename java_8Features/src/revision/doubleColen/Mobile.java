package revision.doubleColen;

public class Mobile {

	public void show(int val) { //do same as interface doing
		
		System.out.println(val+5);
		
	}
	
	public static void dis(int val) { //do same as interface doing
		
		System.out.println(val+9);
		
	}
	
	public static void main(String[] args) {
		
		Mobile m=new Mobile(); 
		
		Contract c= nu -> System.out.println(nu);
		c.display(10);
// method reference		
		Contract c1= System.out::println;
		c1.display(8778);
		
//		as both do same process we shoul say the interface to take this method insterd of defining
		Contract c3= m::show;// must have same arguments
		c3.display(68);
		
		// used for static
		Contract c4= Mobile::dis;// must have same arguments
		c4.display(6884);		

	}

}
