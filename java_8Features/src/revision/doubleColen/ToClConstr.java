package revision.doubleColen;

public class ToClConstr {
	
	int no;
	public ToClConstr(int no) {
		this.no=no;
		System.out.println("constructor "+no);
	}

	public static void show(int n) {
		System.out.println("show "+n);
	}
	public static void main(String[] args) {
		Contract c=ToClConstr::show;
		c.display(55);
//		c=ToClConstr::new;//call/refers constructor
//		c.display(55);
		
		
	}

}
