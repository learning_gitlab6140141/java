package revision.StreamAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamDemoColl {

	public static void main(String[] args) {
		StreamDemoColl sdc= new StreamDemoColl();
		sdc.inte();
//		System.out.println("\nalpha\n");
//		sdc.alp();
//		sdc.mapCo();
		
	}

	private void mapCo() {
		HashMap<String, Integer> hm= new HashMap<String, Integer>();
		hm.put("kevin", 60);
		hm.put("lebron", 75);
		hm.put("irvng", 98);
		hm.put("james", 80);
		
		
//      map stream
		System.out.println("\nmap stream and sorting key");		
		hm
		.keySet()
		.stream()
		.sorted()                              //ascendind
//		.sorted((e1,e2) -> e2.compareTo(e1))   //descending
		.forEach(System.out::println);
		
		
//      map stream
		System.out.println("\nmap stream and sorting key");		
		hm
		.entrySet()
		.stream()
//		.sorted(Map.Entry.comparingByValue())
		.sorted(Map.Entry.comparingByKey())
		.forEach(System.out::println);
		
		
//      map stream
		System.out.println("\nkeyset convert to set");	
		List<String> sl=
		hm
		.keySet()
		.stream()
		.collect(Collectors.toList());  //toset
		System.out.println(sl);
		
//		sl.stream()
//		.anyMatch(na -> na.endsWith("n"))
//		.forEach(System.out::println);
		
	}

	private void alp() {
		ArrayList<String> al=new ArrayList<String>();//issue might be due to the use of raw types for your ArrayLists.
		al.add("iasdoiadj");
		al.add("pekiwejr");
		al.add("ais");
		al.add("cdisuf");
		al.add("gweufhw");
		al.add("ais");
		al.add("cdisuf");
		al.add("gweufhw");
		
		al
		.stream()
		.distinct()
//		.sorted((e1, e2) -> e2.compareTo(e1))                          // comparable for ascending descending
		.sorted((e1, e2) -> -1)                                        // reverse insertion order
		.forEach(System.out::println);
		
//		uppercase
		System.out.println("\nuppercase\n");		
		al
		.stream()
		.distinct()
		.map(n -> n.toUpperCase())
		.forEach(System.out::println);
		
	}

	private void inte() {
		List<Integer> l=new ArrayList<Integer>();
		l.add(30);
		l.add(60);
		l.add(20);
		l.add(16);
		l.add(67);
		l.add(60);
		l.add(20);
		l.add(16);
		l.add(67);
		l.add(78);
		l.add(81);
		
		
		
		l
		.stream()
		.sorted()
		.forEach(System.out::println);
		
		
//		unique value
		System.out.println("\ndistinct() Ascendind");
		l
		.stream()
		.distinct().sorted()
		.forEach(System.out::println);
		
//		descending order
		System.out.println("\nDescending");
		l
		.stream()
		.distinct()
//		.sorted((e1,e2) -> -1)                             //comparable 
		.sorted((e1,e2) -> e2.compareTo(e1))               //comparable 
		.forEach(System.out::println);
		
//		descending order
		System.out.println("\nlimit and skip ");		
		l
		.stream()
		.distinct()
		.sorted()
		.limit(4)                                      //
		.skip(2)
//		.limit(4)
		.forEach(System.out::println);
		
		
//		math operation in array
		System.out.println("\nreduce ");
		System.out.println(l
				   .stream()
				   .distinct().sorted()
				   .reduce((n1,n2) -> -n1+n2));
		
		System.out.println((l
						   .stream()
						   .distinct()
						   .reduce((n1,n2) -> n1+n2)).get());  // to sum the array
		
		
//		descending order
		System.out.println("\nconvert array ");
		
		Object[] ob =l
				.stream()
				.distinct()
				.toArray();
		for (Object o : ob) {
			System.out.println(o);
		}
		
//		descending order
		System.out.println("\nmax ");	
		
//		Optional<Integer> re=
//		l
//		.stream()
//		.max((e1,e2)->e1.compareTo(e2));
//		
//		System.out.println(re.get());
		
		System.out.println(
		l
		.stream()
		.max((e1,e2)->e1.compareTo(e2))
		.get());
		
		
		
		
	}

}
