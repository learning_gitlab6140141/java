package revision.StreamAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.stream.IntStream;

public class StreamDemoAry {

	public static void main(String[] args) {
		
		
		
		int[]  nu= {10,2,5,8,3,4,5,5,10,2,3};
		System.out.println("nu.length -> "+nu.length);
		
		
/**************************pipeline**************************/			
		
		IntStream st= Arrays.stream(nu);                        // IntStream is under Stream to handle integer
		long count = st.count(); // only long
		System.out.println("st.count() -> "+count);             //stream has already been operated upon or closed
//		st.close();                                             //stream has already been operated upon or closed  (i.e., only one time use)
//		System.out.println("stream out-> "+Arrays.stream(nu).count());
		
		
//		st=st.sorted();                                         // this st stream is stopped we can create new stream
//		st.forEach(no-> System.out.println(no));
		
//		st.forEach(System.out::println);                        //can be pipelined by
		
//		Arrays.stream(nu).sorted().forEach(System.out::println);//is return as below for readibility
		Arrays
		.stream(nu)                                            //intermediate operation are lazy operations
		.sorted()                                              //intermediate operation
		.forEach(System.out::println);                         //terminal operation are Eager Operations
	
	/* other methods  */
		
/**************************agrigate**************************/	
		
		
// 1)     average
		System.out.println("\navg()");
		System.out.println(Arrays
		.stream(nu)                                            
		.average()         );                                   
		   
//		OptionalDouble op=Arrays.stream(nu).average();
//		System.out.println(op.getAsDouble());
		
		System.out.println(Arrays
				           .stream(nu) 
				           .average()
				           .getAsDouble()
				           );
		
// 2)     max	and min
		System.out.println("\nmax()");
		System.out.println(Arrays
		                   .stream(nu) 
		                   .min()   //---------- > OptionalInt[45]
		                   .getAsInt()
		           		 );
		
// 2)    findFirst and findAny
		System.out.println("\nfindFirst()");
		System.out.println(Arrays
						   .stream(nu) 
				           .findFirst()  //---------- > OptionalInt[10]
				           .getAsInt()
				           );
		
//		unique value
		System.out.println("\ndistinct()");
		Arrays
		.stream(nu)
		.distinct()
		.forEach(System.out::println);
		
//		multiples of any number
		System.out.println("\nmultiples of any number");
		Arrays
		.stream(nu)
		.filter(n -> n%4==0) //.sorted()
		.forEach(System.out::println);
		
	}

}
