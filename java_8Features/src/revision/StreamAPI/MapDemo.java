package revision.StreamAPI;

import java.util.ArrayList;
import java.util.List;

public class MapDemo {

	public static void main(String[] args) {
		MapDemo mp=new MapDemo();
		mp.inte();
		mp.alp();
	}

	private void alp() {
		ArrayList<String> al=new ArrayList<String>();//issue might be due to the use of raw types for your ArrayLists.
		al.add("iasdoiadj");
		al.add("pekiwejr");
		al.add("ais");
		al.add("cdisuf");
		al.add("gweufhw");
		al.add("ais");
		al.add("cdisuf");
		al.add("gweufhw");
		
//		uppercase
		System.out.println("\nuppercase\n");		
		al
		.stream()
		.distinct()
		.map(n -> n.toUpperCase())
		.forEach(System.out::println);
		
//		concardinate
		System.out.println("\nconcordinate\n");		
		al
		.stream()
		.distinct()
		.map(n -> n+" B.E.,")                //to perform some process
		.forEach(System.out::println);
		
	}

	private void inte() {
		List<Integer> l=new ArrayList<Integer>();
		l.add(30);
		l.add(60);
		l.add(20);
		l.add(16);
		l.add(67);
		l.add(60);
		l.add(20);
		l.add(16);
		l.add(67);
		
		l
		.stream()
		.map(n-> n/2) // changes the data as per our requirement and give 
		.forEach(System.out::println);
		
		
	}

}
