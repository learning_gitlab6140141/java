package revision.StreamAPI;

import java.util.ArrayList;
import java.util.List;

public class ParStDemo {

	public static void main(String[] args) {
		List<Integer> l=new ArrayList<Integer>();
		l.add(30);
		l.add(60);
		l.add(20);
		l.add(16);
		l.add(78);
		l.add(81);
		
		System.out.println("\nparalle stream");
		l
		.parallelStream()
		.sorted()
//		.isParallel()
		.forEach(System.out::println);
		
		System.out.println("\nparalle stream sorted");
		l
		.parallelStream().sorted()
		.forEachOrdered(System.out::println);
		
		System.out.println("\nstream");
		
		l
		.stream()
		.sorted()
		.forEach(System.out::println);
		
	System.out.println("\nstream to parallel");
		
		l
		.stream() .parallel()
		.sorted()
		.forEach(System.out::println);
	}

}
