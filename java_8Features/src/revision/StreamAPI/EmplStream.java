package revision.StreamAPI;

import java.util.ArrayList;
import java.util.stream.Stream;

public class EmplStream {

	public static void main(String[] args) {
		ArrayList<Employee> el=new ArrayList<Employee>();
		
//		Employee e1=new Employee("Mani", 901, 5, 60000);
//		el.add(e1);
//		Employee e2=new Employee("Kumar", 902, 4, 45000);
//		Employee e3=new Employee("Arun", 911, 7, 135000);

		el.add(new Employee("Mani", 901, 5, 60000));
		el.add(new Employee("Kumar", 902, 4, 45000));
		el.add(new Employee("Arun", 911, 7, 135000));
		
//		el
//		.stream()
//		.forEach(System.out::println);
		
//		example 1
		
//		Stream<Employee> empStr=el.stream();
//		System.out.println(empStr);
//		
//		Stream<String> empNamStr=empStr.map(emp -> emp.getName());
//		
//		empNamStr
//		.sorted()
//		.forEach(System.out::println);
		
		el
		.stream()
		.map(emp -> emp.getName())
		.sorted()
		.forEach(System.out::println);
		
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/		
//		example 2 ----- take specific data like DB
		
		System.out.println("\nExperience");
		
//		Stream<Employee> empStr1=el.stream();
//		
//		Stream<Employee> empExpSt=empStr1.filter(n -> n.getExp() >= 5);
//		
//		empExpSt
//		.map(yr -> yr.getExp() )
//		.sorted((e1,e2) -> e2.compareTo(e1))
//		.forEach(System.out::println);
	System.out.println("mapping **********");	
		el
		.stream()
		.filter(n -> n.getExp() >= 5)     // return boolear 
		.map(yr -> yr.getName() )         // match
//		.map(na ->na.getName())
//		.sorted((e1,e2) -> e2.compareTo(e1))
		.forEach(System.out::println);
		
		el
		.stream()
		.filter(n -> n.getExp() >= 5)     // return boolear 
		.map(Employee::getSalary)
//		.map(sa -> sa <= 80000 ? 90000 : salary)
		.forEach(System.out::println);
		
		 el.stream()
         .filter(n -> n.getExp() >= 5)
         .peek(employee -> {
             if (employee.getSalary() <= 80000) {
                 employee.setSalary(90000);
             }
         })
         .forEach(System.out::println);
		
		
	}

}
