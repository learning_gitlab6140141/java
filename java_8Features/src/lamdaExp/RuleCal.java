package lamdaExp;

public class RuleCal implements Rule {
	public static void main(String[] args) {
		
//		Rule rule=()->{System.out.println("from interface");}; //rule is reference not object
//		rule.add();
		
//		Rule rule=(n1,n2)->System.out.println(n1+n2);
//		rule.add(10, 30);
		
		Rule rule=(n1,n2)->{return(n1+n2);};//for return must have {}
		
		
		int no=rule.add(90, 30);
		System.out.println("~~~ "+no);
		
		RuleCal rc=new RuleCal();
		rc.add();
		Rule dy=new RuleCal();
		dy.divide();

		Rule.sub();
	}
	
	public void sub() {
		System.out.println("sub of child method");
		}
	
	public void add() {
	System.out.println("ruleCal method");
	}
	
	public void divide() {
		System.out.println("ruleCal div-met");
	}

	@Override
	public int add(int no1, int no2) {
		// TODO Auto-generated method stub
		return no1+no2;
	}
}
