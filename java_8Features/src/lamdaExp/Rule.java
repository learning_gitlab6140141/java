package lamdaExp;

@FunctionalInterface
public interface Rule {

//	public void add();//without arguments without return
//	public void add(int no1,int no2);//with arguments without return
	public int add(int no1,int no2);//with arguments with return
//must have only one abstract method,and have any number of default and static method
//
	
	public default void divide() { //default is a keyword used in functional interface,it also called defended methods /virtual extension method/that is predefined
		System.out.println("div-met");
	}

	public static void sub() {
		System.out.println("sub-met");
	}
}
