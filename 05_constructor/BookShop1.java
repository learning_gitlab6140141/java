public class BookShop1
{
	// constructor overloading
	public BookShop1(int no) // arguments
	{
		System.out.println("constructor"+no);
	}
	//no- argument constructor... different from default costructor(which is not needed to mention)
	public BookShop1()
	{
		System.out.println("different from default construvtor- needs to be defined");
	}
	public BookShop1(String a)
	{
		System.out.println(a+"is a String");
	}
	public static void main (String[] args)
	{
		BookShop1 book1 =new BookShop1(10); //parameter
		BookShop1 book2 =new BookShop1();
		BookShop1 book3 =new BookShop1("qpwo");
	}
}


