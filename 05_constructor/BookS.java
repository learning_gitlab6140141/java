public class BookS
{
	// constructor overloading
	public BookS(int no ,String name) // arguments
	{
		System.out.println("constructor 1 "+no +name);//10
		//System.out.println("constructor 2 "+name);//string
	}
	//no- argument constructor... different from default costructor(which is not needed to mention)
	
	public BookS(int no) // arguments
	{
		System.out.println("constructor 3 "+no);
	}

	public static void main (String[] args)
	{
		BookS book1 =new BookS(10); //parameter
		//BookS book2 =new BookS(40);
		//BookS book3 =new BookS(34);
		BookS book =new BookS(56,"feudywd");
		book.buy();//method
		book.buy(30);
		book.buy(40);
		book.buy(50);
		book.buy(80);
	}
	
	public void buy()
	{
		System.out.println("method");
	}
	public void buy(int a)
	{
		System.out.println(a);
	}
} 


