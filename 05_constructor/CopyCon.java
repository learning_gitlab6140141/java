class Const {
    // data members of the class.
    String name;
    int id;
 
    // Parameterized Constructor
    Const(String name, int id)
    {
        this.name = name;
        this.id = id;
    }
 
    // Copy Constructor
    //That's helpful when we want to copy a complex object that has several fields,
    Const(Const a)
    {
        this.name = a.name;
        this.id = a.id;
    }
}
class CopyCon {
    public static void main(String[] args)
    {
        // This would invoke the parameterized constructor.
       // System.out.println("First Object");
        Const obj = new Const("avinash", 68);
        //System.out.println("GeekName :" + geek1.name+ " and GeekId :" + geek1.id);
 
        //System.out.println();
 
        // This would invoke the copy constructor.
        Const obj2 = new Const(obj);
        System.out.println("Copy Constructor used Second Object");
        System.out.println("Name :" + obj2.name+ " and Id :" + obj2.id);
    }
}