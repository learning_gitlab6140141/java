public class BookShop
{
	String name;//Declared instance/object/non_static variable
	int pages;
	String author;
	
	/*constructors
	1) should have same class name
	2) no return data type required
	3) call automatically when object is created
	4) used to initializing object specific values*/
	
	public BookShop (String name, int pages,String author) // constructor
	{
		//"this"- it is key word used to refer current object ie., similar to call "book1.name; book1.pages" 
		//it can be used in methods or constructors 
		//mostly used to eliminate the confusion between the class attributes and parameters
		
		this.name = name;
		this.pages = pages;
		this.author = author;
		
		System.out.println("\nout put from constructor");
		System.out.println("\nBook name "+name);
		System.out.println(" - Pages "+pages);
		System.out.println(" - author "+author);
		
	}
	
	public static void main ()
	{
		System.out.println("i am main method");
	}
	public static void main (String[] arg)
	{
		BookShop book1 = new BookShop("Rich habits",160, "tom corley"); //parameters
		//book1.name = "Rich habits";
		//book1.pages = 160;
		//book1.author = "tom corley";
		
		BookShop book2 = new BookShop("mossad",240,"zohar");
		
		
		
		book1.buy();
		book2.buy();
		
	}
	public void buy() //method definition
	{
		System.out.println("\n out put from method");
		System.out.println("\nBook name "+name);
		System.out.println(" - Pages "+pages);
		System.out.println(" - author "+author);
		
	}
	
}