package snake;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		JFrame frame =new JFrame("snakeGame");
		
		//create obj for 'SnakeGame' which is send to frame which is used to create pannel 		
		SnakeGame game = new SnakeGame();
		
		frame.add(game);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();//used to match screen 
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}
