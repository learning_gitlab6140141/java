public class BookShop2
{
	String name; // Declare instance variable or non - static variable
	int pages; // Declare instance variable or non - static variable
	String author; // Declare instance variable or non - static variable
	int mrp; // Declare instance variable or non - static variable 
	
	// constructor
	// 1) Should have same class name 
	// 2) No return data type required
	// 3) call automatically when object is created 
	// 4) used to intializing object specific values
	
	// this keyword
	// this keyword refer to current object
	// this keyword cannot be used in static
	
	public BookShop2( String name, int pages , String author , int mrp) // constructor
	{
		this.name = name;       // this keyword is refered as current object name 
		this.pages = pages;
		this.author = author;
		this.mrp = mrp;
		
	}
	public BookShop2()
	{
		
	}
	public static void main(String [] args )
	{
		
		BookShop2 book1 = new BookShop2("xyz",2000,"Author 1",500); // object creating and initilize the object value using Constructor
		
		//book1.name = "abc";
		//book1.pages = 150;
		//book1.author = "auther1";
		
		
		
		BookShop2 book2 = new BookShop2("ABC",200,"Author 2",600); // object creating and initilize the object value using Constructor
		
		BookShop2 book3 = new BookShop2();
		
		
		
		//book2.name = "xyz";
		//book2.pages = 150;
		//book2.author = "auther2";
		
		book1.buy(); // call method using object
		//book1.details(); // call method using object
		book2.buy(); // call method using object
		//book2.details(); // call method using object
	String trial= book1.buy();
		System.out.println("returan==>"+trial);
		
	}
	public String buy() // Method  defining
	{

		System.out.println("Book Name: "+name+" \nPages: "+pages+" \nAuthor Name: "+author); // printing the values
		return name; 
		/* error: unreachable statement
		System.out.println(name);
		^
BookShop1.java:66: error: missing return statement
	}
	^
2 errors
*/
		
	}
	public void details() // Method defining
	{
		System.out.println("MRP : "+mrp+" RS");
		
	}
}



/* 
output
Book Name: XYZ 
Pages: 2000 
Author Name: Author 1
Book Name: ABC 
Pages: 200 
Author Name: Author 2
*/