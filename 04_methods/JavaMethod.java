public class JavaMethod
{
	public static void main (String[] args) //static method		
	{
		JavaMethod jm = new JavaMethod();
		System.out.println(jm.decimal());
		System.out.println(jm.test());
	}
	public float decimal() // instance method		
	{
		float a=34.6f;
			return a;
		/*A method is a block of code which only runs when it is called.

		You can pass data, known as parameters, into a method.

		Methods are used to perform certain actions, and they are also known as functions.

		Why use methods? To reuse code: define the code once, and use it many times.

		*/
	}	
			public boolean test()
	{
		return (10<8);
	}
}