public class SuperMarket
{
	static String shopName ="Eswar";//static variable
	static int doorNo = 5;
	String name; //non-static / instance/
	int price;
	
	public static void main (String [] args)
		
	{
		System.out.println(SuperMarket.shopName);
		System.out.println(SuperMarket.doorNo);
		System.out.println(shopName);
		
		SuperMarket product1 = new SuperMarket();
		System.out.println("=-=-=->"+product1.shopName);
			product1.name="soap";
			product1.price =30;
			System.out.println(product1.name);
			System.out.println(product1.price);
		SuperMarket product2 = new SuperMarket();
			product2.name="dal";
			product2.price =300;
			System.out.println(product2.name);
			System.out.println(product2.price);
		
		product1.buy(); //method calling statement(like switch)
	}
	
	public void buy() // method definition(like bulb), called system signature
	{
		System.out.println("buy method called succed");// method body
	}
}