package StudentData;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Empl {

	public static void main(String[] args) throws SQLException {
		Empl ee=new Empl();
		ee.view();
		ee.insert();
		System.out.println("////////////////////////////*******insert()*******//////////////////////////////////////////");
		ee.view();
		ee.rem();
		ee.upd();
		System.out.println("////////////////////////////*******rem()*******//////////////////////////////////////////");
		ee.view();

	}

	private void upd() {
		int no=4; 
		String name="manju";
		String url="jdbc:postgresql://localhost:5432/emp";
		String userName="postgres";
		String password="emp";
		String queru="UPDATE emp_details set name = ? where emp_id = ?";//replace
		//String queru="UPDATE emp_details set name = 'hanei' where emp_id = 52";
		try {
			Connection con= DriverManager.getConnection(url,userName,password);
			/*without prepared statement
			 * Statement st=con.createStatement(queru);
			 * st.executeUpdate();*/
			PreparedStatement ps= con.prepareStatement(queru);//remove
			ps.setInt(2, no);//replace - st.setInt(2, no);
			ps.setString(1, name);//replace
			ps.executeUpdate();//replace
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void rem() throws SQLException {

		int no=154;
		String url="jdbc:postgresql://localhost:5432/emp";
		String userName="postgres";
		String password="emp";
		String queru="DELETE FROM emp_details where emp_id = ?";
		Connection coIn=DriverManager.getConnection(url,userName,password);
		PreparedStatement ps= coIn.prepareStatement(queru);
		ps.setInt(1, no);
		ps.executeUpdate();
		coIn.close();
		
	}

	private void insert() throws SQLException {
		int no=154;
		String name="Robin";
		
		
		String url="jdbc:postgresql://localhost:5432/emp";
		String userName="postgres";
		String password="emp";
//		String query="INSERT INTO emp_details values (153,'manikam')";
		String query="INSERT INTO emp_details values (?,?)";// prepared statement
		
		Connection coIn=DriverManager.getConnection(url,userName,password);
//		Statement SIO=coIn.createStatement();
//		int row=SIO.executeUpdate(query);
		PreparedStatement pI= coIn.prepareStatement(query);
		pI.setInt(1, no);
		pI.setString(2, name);
		System.out.println("@@@@@@@ "+pI.executeUpdate());
		
		
		coIn.close();
		
		
	}

	private void view() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/emp";
		String userName="postgres";
		String password="emp";
		String query="select * from emp_details";
		
		Connection con= DriverManager.getConnection(url,userName,password);
		Statement stClOb= con.createStatement();
		ResultSet rs =stClOb.executeQuery(query);//just
		while(rs.next()) {
			System.out.print(rs.getInt(1));
			System.out.println(" "+rs.getString(2));
		}
		
		con.close();
		
	}

}
