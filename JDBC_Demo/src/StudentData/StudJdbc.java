package StudentData;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class StudJdbc {

	public static void main(String[] args) throws SQLException {
		StudJdbc sj=new StudJdbc();
//		sj.insert();
//		sj.view();
		sj.update();
		sj.view();
		sj.dlt();
		

	}

	private void dlt() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String query="delete from students where sno = ? " ;
		
		Connection con= DriverManager.getConnection(url,userName,password);
		PreparedStatement ps=con.prepareStatement(query);
		ps.setInt(1, 2);
		ps.execute();
		ps.close();
		
	}

	private void update() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String query="update students set name = ? where sno = ?";
		
		Connection con= DriverManager.getConnection(url,userName,password);
		PreparedStatement ps=con.prepareStatement(query);
		ps.setString(1, "gokul");
		ps.setInt(2, 1);
		ps.executeUpdate();
		con.close();
	}

	private void insert() throws SQLException {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter no, name,age");
		int no=sc.nextInt();
		String name=sc.next();
		int age=sc.nextInt();
		
		
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String query="insert into students values (?,?,?)";
		
		Connection con= DriverManager.getConnection(url,userName,password);
		PreparedStatement ps=con.prepareStatement(query);
		ps.setInt(1, no);
		ps.setString(2, name);
		ps.setInt(3, age);
		
		int change=ps.executeUpdate();
		System.out.println(change+" affected");
		con.close();
		
		
	}

	private void view() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/learn_jdbc";
		String userName="postgres";
		String password="emp";
		String query="select * from students";
		
		Connection con= DriverManager.getConnection(url,userName,password);
		Statement st=con.createStatement();
		ResultSet rs=st.executeQuery(query);
		
		while(rs.next()) {
			System.out.println(rs.getInt(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getInt(3)+" \n");
		}
	}

	
	
	
	
	
	
	
}
