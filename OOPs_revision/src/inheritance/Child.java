package inheritance;

public class Child extends Parent {
	
	public static void main(String[] args) {
		Parent p=new Parent();//has-a relation
		
		Child c=new Child();
		
		
		Parent pc=new Child();
//		Child ch=(Child) new Parent(); //java.lang.ClassCastException:
		
	System.out.println("******parent object******");			
		p.exercise();
//		p.out(); // undefined method in parent
		p.read();     // only parents method accesed and child's cant. 
		
	System.out.println("******child object******");	
		c.exercise();
		c.out(); 
		c.read();
		
	System.out.println("******dynamic object******");	
		pc.exercise();
//		pc.out();  // undefined method in parent
		pc.read();
		
		
		
		
	}
	
	public void exercise() {
		System.out.println("child jocking"); 
	}
	
	public void out() {
		System.out.println("child outer world"); 
	}
	

}
