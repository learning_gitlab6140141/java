package inheritance.multiThreading;

public class Child  extends Parent{

	public static void main(String[] args) {
		
		 Parent pc=new Child();
		 Child c=new Child();
		 Parent p=new Parent();
		 
		 pc.start();
		 c.start();
		 p.start();
		 
//		 pc.TrySup("dynamic");
//		 System.out.println(pc.getName());
//		 
//		 c.TrySup("child");
//		System.out.println(c.getName());
//		
//		 p.TrySup("parent");
//			System.out.println(c.getName());
		 
//		 for (int i=1;i<11;i++)
//			 System.out.println("child class : "+i);

	}
	
	public void TrySup(String s) {
		System.out.println("super.start();  "+s);
		
		super.start();		
	}

}
