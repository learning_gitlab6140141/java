package inheritance;

public class Parent {
	
	public static void main(String[] args) {
		Child p= new Child();
		p.exercise();
	}

	public void read() {
		System.out.println("parent book for clarity");
		
	}

	public void exercise() {
		System.out.println("parent walking"); 
	}
}
