// entire element is taken because the error msg will find its parent usnig this

const form=document.querySelector("#form")
const username=document.querySelector("#username")
const email=document.querySelector("#email")
const password=document.querySelector("#password")
const cpassword=document.querySelector("#cpassword")

//submit only if the boolian is true or else should not allow

form.addEventListener('submit',(obj)=>{ //() is an annonomus function that is without function name
                                        //obj stores the returned value from 
    if(validateInputs()==false)
        obj.preventDefault(); //obj.preventDefault();
})

function validateInputs(){
   const username_value= username.value.trim()
   const email_value= email.value.trim()
   const password_value= password.value.trim()
   const cpassword_value= cpassword.value.trim()
   let success=true; // boolian variable



   if(username_value==="")
   {
    success=false
    setError(username,'User Name Required')
   }
   else{
    setSuccess(username)
   }



   if(email_value==="")
   {
    success=false
    setError(email,'Email Id Required')
   }
   else if(validateEmail(email_value)==false || !email_value.includes('@'))
   {
    success=false
    setError(email,'Invalid Email Id')
   }
   else{
    setSuccess(email)
   }


   if(password_value==="")
   {
    success=false
    setError(password,'password required')
   }
   else if (password_value.length < 8){
    success=false
    setError(password,'passsword must have minimum 8 characters')    
   }
   else if (validatePassword(password_value)==false){
    success=false
    setError(password,'passsword must have Upper case and a special character')
}


   else{
    setSuccess(password)
        if (password_value.length > 9) {
        setPassword(password, 'week password');
    } else if (password_value.length > 12) {
        setPassword(password, 'moderate password');
    } else if (password_value.length > 16) {
        setPassword(password, 'strong password');
    }
   }

   if(cpassword_value==="")
   {
    success=false
    setError(cpassword,'password required')
   }
   
   else if (cpassword_value===password_value){
    setSuccess(cpassword)
   }
   else{    
    success=false
    setError(cpassword,'passsword doesnot match')
   }

   return success

}

function setError(element,message)
{
    let input_group= element.parentElement//
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=message
    input_group.classList.add('error')//css
   
}

function setSuccess(element)
{
    let input_group= element.parentElement
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=""
    input_group.classList.add('success')//css
    input_group.classList.remove('error')//css
}

function setPassword(element, message) {
    let input_group = element.parentElement;
    let warn_element = input_group.querySelector('.warn');

    warn_element.innerHTML = message;
    input_group.classList.add('success');
    input_group.classList.add('warn');
    input_group.classList.remove('error');
}


// function setError(element,message)
// {
//    let input_group=element.parentElememt
//    let error_element=input_group.querySelector('.error')

//    error_element.innerHTML=message
//    input_group.classList.add('.error')
// }


function validateEmail(ev) {
   return String(ev).toLowerCase().match(
       /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
     );
 }

 function validatePassword(password) {
    // Check if the password contains an uppercase letter and a special character
    return /[A-Z]/.test(password)&&/\d/.test(password) && /[!@#$%^&*(),.?":{}|<>]/.test(password);
}