f// Function declaration: print_cons
function print_cons(text) {
    console.log("hello " + text);
}

// Function declaration: print_pag
function print_pag(text1) {
    document.querySelector('#msg').innerHTML = "hello " + text1;
}

// Function declaration: Get_eleme
function Get_eleme(callback) {
    // Function call: callback('eswar')
    callback('eswar');
}

// Function call: Get_eleme(print_cons)
Get_eleme(print_cons);

// Function call: Get_eleme(print_pag)
Get_eleme(print_pag);


// Function print_cons:
// Function variable: print_cons
// Argument: text

// Function print_pag:
// Function variable: print_pag
// Argument: text1

// Function Get_eleme:
// Function variable: Get_eleme
// Argument: callback

// Function calls:
// Get_eleme(print_cons): Calls Get_eleme with print_cons as the callback function.
// Get_eleme(print_pag): Calls Get_eleme with print_pag as the callback function.

// In the function Get_eleme, the callback parameter represents a function that will be called within Get_eleme. The actual functions passed as arguments (print_cons and print_pag) are then called with the provided values ('eswar'). The result is the printing of "hello eswar" in the console for print_cons and updating the HTML content of an element with the id 'msg' for print_pag.
