package com.employees.employee.repositery;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employees.employee.entity.employeeEntity;

public interface EmployeeRepository extends JpaRepository<employeeEntity, Integer> {

}
