package com.employees.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.ModelAndView;

import com.employees.employee.entity.employeeEntity;
import com.employees.employee.repositery.EmployeeRepository;

@Controller 
public class EmployeeController {
	
	@Autowired
	EmployeeRepository em;
	
@RequestMapping("eswar")
public ModelAndView name(){
	ModelAndView mav=new ModelAndView("details");
	mav.addObject("emplo",em.findAll());
	return mav;
}

@GetMapping("/emp")
public ModelAndView newEmp(employeeEntity ee) {
	ModelAndView mav=new ModelAndView("adEmp");
	mav.addObject("mve",ee);
	return mav;
	}
@PostMapping("/emp")
public String saveEmp(employeeEntity so) {
	em.save(so);
	return "redirect:/eswar";
}

}
