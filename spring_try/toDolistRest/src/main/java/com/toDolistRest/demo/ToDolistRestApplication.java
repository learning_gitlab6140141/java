package com.toDolistRest.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToDolistRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToDolistRestApplication.class, args);
	}

}
