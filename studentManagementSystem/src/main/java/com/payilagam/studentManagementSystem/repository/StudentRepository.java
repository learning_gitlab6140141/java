package com.payilagam.studentManagementSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payilagam.studentManagementSystem.entity.StudentEntity;

public interface StudentRepository extends JpaRepository<StudentEntity, Long>{

}
