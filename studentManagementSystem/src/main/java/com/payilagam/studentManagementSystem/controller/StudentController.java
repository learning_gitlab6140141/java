package com.payilagam.studentManagementSystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.studentManagementSystem.entity.StudentEntity;
import com.payilagam.studentManagementSystem.repository.StudentRepository;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;


@Controller
public class StudentController {
	
	@Autowired
	StudentRepository repo;
	
	@RequestMapping("index")
	public ModelAndView listStudent () {
		ModelAndView mav=new ModelAndView("listStud");
		mav.addObject("students",repo.findAll());
		
		return mav;
	}
	
	@GetMapping("/student/new")
	public ModelAndView addStudent(StudentEntity stud) {
		ModelAndView mav=new ModelAndView("form");
		mav.addObject("stude",stud);
		return mav;
	}
	@PostMapping("/student/new")
	public String saveStudent(StudentEntity ent) {
		repo.save(ent);
		return "redirect:/index";
	}
	
	@GetMapping("/student/edit/{id}")
	public ModelAndView updateStudent(@PathVariable long id) {
		ModelAndView mav =new ModelAndView("editStudent");
		mav.addObject("stUp",repo.findById(id).get());
		return mav;
	}
	
	@PostMapping("/student/edit/{id}")
	public String saveUpdate(@PathVariable long id, StudentEntity upt) {
        // Set the ID explicitly to ensure you are updating the correct entry
        upt.setS_id(id);
        repo.save(upt);
        return "redirect:/index";
	}
	
	@GetMapping("/student/delete/{id}")
	public String deleteStu(@PathVariable long id) {
		repo.deleteById(id);
		return "redirect:/index";
	}
	
	

}
