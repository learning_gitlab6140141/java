// let gi=prompt('enter a number for getting squire')
// let op=document.getElementById('e')
// let sqno= sq(gi)
// op.innerHTML=`squire of the no ${gi} is ${sqno} `

// function sq(gi){
//     return gi*=gi
// }



/***************** variable key ******************/

// let n='fire god eswar'     //global

// function funCal(a){         //method/function
//      a='learning'           //local
//     console.log('global/local : '+n);
//     if(true){              //block scope
//         const a='java script'
//         console.log('indide if : '+a);
//     }
// console.log('fun cl : '+a);
// }

// console.log(n);
// funCal(n);

/***************** hoisting ******************/
// hoisting (here var n) 
// function cla(){
    
//     if(true){
//         n='lord' //similar to var n=''
        
//         // const n='lodan'
//     }

//     console.log(n)
// }

// cla()

/***************** coercion ******************/
// console.log(42+'5')
// console.log(42-'5')


/***************** lodical operator return ******************/
// const a=42, b='asas',c=null
// console.log(c||b)// not return boolean return the value
// console.log(b&&a)

// function greet(na){
//     console.log(`hello.. ${na|| 'visitor'} !`)
// }

// greet()//without args
// greet('hdhhdhd')// with args


// z()


// function z(a){console.log("with args " )}
// function z(){console.log("no args")}

/***************** equality and string equality ******************/
// console.log(42=='42') //equality
// console.log(42==='42') //string equality

/***************** nullish coalescing operator ******************/
// let res=null;

// // if (res == undefined)
// // res='helo'            // replaced as

// res ??='hello'  //res=res?? 'helooo'

// console.log(res)


/***************** function ******************/

// function tes(a,b){
    
//     // console.log('addtion',a+b)
//     // return a+b
//     if(a&&b)
//     {
//     if (a>b)
//     return a

//     else if (b>a)
//     return b

//     return "both are equal"
// }

// else return 0;

// }
// let out=tes(7,8)
// console.log(out)

// console.log(tes())


/***************** closure ******************/
/*$$$$$$$$$$$$$$$$$ $$$$$$$$$$$$$$$$$ $$$$$$$$$$$$$$$$$*/

// function out(){
//     b=10;
//     function inn(){
//         a=10
//         console.log(b+a)
//     }
//     return inn
// }

// let soFu=out()
// console.log(soFu())        //this acts as inner function
// console.log(soFu)

/*************************************************/
/***************** arrays ******************/

// let colour=['red','blue','violer',4,56,{name:'abdul',age:67}]//accept all data typr,object
// console.log(colour)
// console.log(colour[5].name) //viewing object

// let ar=new Array('red',8,'violer',4,)//create using Array class
// console.log (ar[2])//call by index
// console.log (ar)
// ar[1]='yellow'
// console.log (ar)
// console.log ("length of array ar ",ar.length)

// for(i=0;i<colour.length;++i)
// console.log (colour[i])

// ar=[] // make it empty
// console.log ("length of array ar ",ar.length)

/***************** arrays method  ******************/

// let arr=['a','s','q',1,2,3,4]
// console.log(arr)
// console.log(  arr.indexOf('q')   )

// arr.push('f')
// console.log(arr)
// console.log(arr.pop())
// console.log(arr)
// console.log(arr.shift())
// console.log(arr)
// arr.unshift(('g'))
// console.log(arr)
// arr.splice(3,2,7,8,'u')//remove index 3 to (2-1) and insert remaining 
// console.log(arr)

/***************** arrays map method******************/
// const pro=['red rice','ulundu kali','ragi puttu','godumai ']
// proMap=pro.map(anFu => { return '* '+anFu;} )

// console.log(proMap)       // map used to transform

// proMap.forEach( ele => {
//     console.log(ele)
// });

/***************** arrays filter method******************/
// var nu=[10,3,5,9,3,8,7]
// let res=nu.filter(n =>{ return n>=5;})
// console.log(res)

// var creature=[{name:'shark',lives:'ocean'},//inside obj its called prpoerty or field
//               {name:'lion',lives:'forest'},
//               {name:'Whale',lives:'ocean'},
//               {name:'monkey',lives:'jungle'},
//               {name:'tiger',lives:'forest'}]
// let water=creature.filter(n => {return n.lives=='ocean';})

// console.log(water)
// for(i=0;i<water.length;i++)
// console.log(water[i].name)



/***************** other way to create object(Object.create)******************/
// let person = Object.create ({fNam:'karukuvel',work:'Engineer'})  //Object -> is class

// console.log(person.fNam)

// console.log(person['fNam'])

// person.fNam='praba'
// delete person.work
// console.log(person)
// console.log('fNam' in person)

/***************** object key value  methods ******************/

// var creature={name:'shark',lives:'ocean',diet:'omni'}

// let values=Object.values(creature)//similar to key value in java
// console.log(values);
// let key=Object.keys(creature)//similar to key value in java
// console.log(key);

/***************** object entries  methods ******************/

// let obj={name:'aadam',age:20,native:'chennai'}//want to change to array value
// //if key is in number will prit in ascending
// let ar=Object.entries(obj)
// console.log(ar)

/***************** this key word
 * used in class or obj ******************/
// function creaChar(name){
//     return {
//         name,//name:'eswar',//this represent the object not the argument
//         greet: function() {
//             console.log (`hello.... ${this.name}!`);
//               return `hello.... ${this.name}!`;//chatgpt
//         }
//     };
// }

// const cha = creaChar('banner'); // Create a character object with name 'banner'
// console.log('fun out',cha)
// console.log(cha.greet()); // Call the greet method of the object

// const { greet } = creaChar('banner'); // Destructuring assignment to extract greet method
// console.log(greet()); // This won't work as greet is not executed here



/***************** new key word ******************/
// function creaChar(name,age){
//     return this.name=name,this.age=age
// }
// let ch=new creaChar('eswar',25)//function also act as class to create object
// console.log(ch.name)
// console.log(ch.age)
// console.log(ch)
/***************** prototype  
 * object have prototype property connected with another object
 * example if i search name in obj-1 where it has no name it search name in obj-2 for name and gets it ******************/


/***************** 1)external user defined ******************/

/*$$$$$$$$$$$$$$$$$ $$$$$$$$$$$$$$$$$ $$$$$$$$$$$$$$$$$*/
// const chara = {
//     anger: function() {
//         console.log('Hulk');
//     }
// };

// const green = {
//     name: 'Dr.Banner',
//     __proto__: chara // Set the prototype to chara
// };
// const g=green
// console.log('char fro gr ',g)
// console.log(green.anger()); // Access the inherited function via prototype
// console.log(chara.anger()); // Call the function directly from chara
/***************** 2)make it in-build ******************/


/*$$$$$$$$$$$$$$$$$ $$$$$$$$$$$$$$$$$ $$$$$$$$$$$$$$$$$*/
// function chara (name) {  //function acts as class
//     this.name=name
//     this.anger = function() {
//         console.log(`${this.name} -> Hulk`);
//     }
// }

// function Fight(name){
//     this.name=name
// }

// Fight.prototype =new chara() // create a obj for chara and give it to fighter .prototype acces this property, so this obj will be Fight function prototype. so the object created using Fighter will be connected to chara

// const green = new Fight('Dr.Banner')

// console.log(green.anger());
// console.log(green)

/***************** class ******************/
/*on using class there is no need to use prototype*/
// class Character{
//     constructor(name){
//         this.name=name;
//     }

//     attack() {
//         console.log(`${this.name}:swing!`)
//     }
// }

// class Fighter extends Character{
//     constructor(name){
//         super(name)
//     }
// }

// const fight =new Fighter('Hulk')
// fight.attack()

/***************** for...in obj******************/

// let design={color:'blue',patten:'flower',length:20};
// for(const pr in design){
//     console.log('key-> ',pr)//show only key
// }
// // console.log(Object.values(design))
// console.log('valued->', design['patten'])//to access value
// for(const pr in design){
//     console.log('value using key-> ',design[pr])//show only key
// }

/***************** for...in array ******************/
// let ar=['green','blue','yellow',9,5]

// console.log('index values')
// for(let x in ar){
//     console.log(x)
// }

// console.log('inner values')
// for(let x in ar){
//     console.log(ar[x])
// }

/***************** try catch finally******************/

// function add(a,b){return a+b}

// let res=0
// try{
// res=add(10,20)
// console.log(res)}
// catch (e){
//     console.log({nm:e.name,msg: e.message})
// }
// finally{
//     console.log('always works',res)
// }
// console.log('bye')

/***************** promise ******************/

// function getUser() {
//     return [{usnm:'abi',email:'abi@yhu'},{usnm:'banu',email:'banu@jaj'}]
// }

// function fiUs(usNm){
//     const user=getUser();
//     console.log(user)
//     const u=user.find(us => us.usnm=== usNm);
//     return u
// }

// console.log(fiUs('abi'))

//Asynchronous operation
// function getUser() {
//     return new Promise(() =>{ let users=[];

//     setTimeout((resolve,reject) => {
//     users= [{usnm:'abi',email:'abi@yhu'},{usnm:'banu',email:'banu@jaj'}
// ];
// resolve(users)
// },1000);

//     return users
// })
//     }

//     let p=getUser()
//     p.then((users)=>console.log(users))    
    // function fiUs(usNm){
    //     const user=getUser();
    //     console.log(user)
    //     const u=user.find(us => us.usnm=== usNm);
    //     return u
    // }
    
    // console.log(fiUs('abi'))

/***************** program ******************/  

/***************** Using JavaScript objects ******************/

// console.log('Using JavaScript objects')

// let str = 'GeeksforGeeks'
  
// let result = {} 
// for(let i = 0;i< str.length;i++){ 
//   let ch = str.charAt(i) 
//   if(!result[ch]){ 
//     result[ch] =1; 
//   } 
//   else{ 
//     result[ch]+=1 
//   } 
// } 
// console.log( 
//     "The occurrence of each letter in given string is:",result)


/***************** Using JavaScript map ******************/

    // console.log('Using JavaScript map')


    // let str = 'GeeksforGeeks'
  
    // let result = new Map() 
    // for(let i = 0;i< str.length;i++){ 
    //   let ch = str.charAt(i) 
    //   if (!result.get(ch)) result.set(ch, 1); 
    //     else { 
    //         result.set(ch, result.get(ch) + 1); 
    //     } 
    // } 
    // console.log(result) 
    // console.log( 
        // "The occurrence of each letter in given  
        // string is:",result)
    



    console.log('Using JavaScript array')

let str = "GeeksforGeeks"; 
  
let strArray = str.split(""); 
  
// console.log(strArray) 
let result = strArray.reduce((chars, ch) => { 
    if (!chars[ch]) { 
        chars[ch] = 1; 
    } else { 
        chars[ch] += 1; 
    } 
    // console.log(ch); 
    return chars; 
}, []); 
  
// console.log(result) 
console.log( 
    "The occurrence of each letter in given string is:", result)

