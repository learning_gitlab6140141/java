// only for search btn
    let searchForm = document.querySelector('.search-form');

    document.querySelector('#search-btn').onclick = () =>{
        searchForm.classList.toggle('active');

        // searchForm.classList.remove('active');
        cart.classList.remove('active');
        loginForm.classList.remove('active');
        navbar.classList.remove('active');

    }
//end for search btn

//start for cart
    let cart = document.querySelector('.shopping-cart');
    document.querySelector('#cart-btn').onclick = () =>{
        cart.classList.toggle('active');

        searchForm.classList.remove('active');
        cart.classList.remove('active');
        loginForm.classList.remove('active');
        navbar.classList.remove('active');
    }
//end fot cart
// form start section

    let loginForm = document.querySelector('.login-form');
    document.querySelector('#login-btn').onclick = () =>{
        loginForm.classList.toggle('active');

        searchForm.classList.remove('active');
        cart.classList.remove('active');
        loginForm.classList.remove('active');
        navbar.classList.remove('active');
    }
//form end section
//media query toggle
let navbar = document.querySelector('.navbar');
document.querySelector('#menu-btn').onclick = () =>{
    navbar.classList.toggle('active');

    searchForm.classList.remove('active');
    cart.classList.remove('active');
    loginForm.classList.remove('active');
    // navbar.classList.remove('active');
}


window.onscroll = () =>{
    searchForm.classList.remove('active');
    cart.classList.remove('active');
    loginForm.classList.remove('active');
    navbar.classList.remove('active');
}

//start  carousel design
let slides = document.querySelectorAll('.home .slides-container .slide');
let index = 0;

function next() {
    slides[index].classList.remove('active');
    index = (index + 1) % slides.length;
    slides[index].classList.add('active');
}

function prev() {
    slides[index].classList.remove('active');
    index = (index - 1 + slides.length) % slides.length;
    slides[index].classList.add('active');
}

//end carousel design

/*************  form validation start  ***************/

const form=document.querySelector("#form")
const username=document.querySelector("#username")
const email=document.querySelector("#email")
const passsword=document.querySelector("#password")
const cpassword=document.querySelector("#cpassword")

form.addEventListener('submit',(obj)=>{
    if(validateInputs()==false)
    obj.preventDefault();
})

function validateInputs(){
    const username_value=username.val.trim()
    const email_value= email.value.trim()
   const password_value= password.value.trim()
   const cpassword_value= cpassword.value.trim()
   let success=true; // boolian variable

   if(username_value===""){
    success==false
    setError(username,'User Name Required')
   }
   else{
    setSuccess(username)
   }

   if(email_value==="")
   {
    success=false
    setError(email,'Email Id Required')
   }
   else if(validateEmail(email_value)==false || !email_value.includes('@'))
   {
    success=false
    setError(email,'Invalid Email Id')
   }
   else{
    setSuccess(email)
   }


   if(password_value==="")
   {
    success=false
    setError(password,'password required')
   }
   else if (password_value.length < 8){
    success=false
    setError(password,'passsword must have minimum 8 characters')    
   }
   else if (validatePassword(password_value)==false){
    success=false
    setError(password,'passsword must have Upper case and a special character')
}


   else{
    setSuccess(password)
        if (password_value.length > 9) {
        setPassword(password, 'week password');
    } else if (password_value.length > 12) {
        setPassword(password, 'moderate password');
    } else if (password_value.length > 16) {
        setPassword(password, 'strong password');
    }
   }

   if(cpassword_value==="")
   {
    success=false
    setError(cpassword,'password required')
   }
   
   else if (cpassword_value===password_value){
    setSuccess(cpassword)
   }
   else{    
    success=false
    setError(cpassword,'passsword doesnot match')
   }

   return success

}


function setError(element,message){
    let input_group= element.parentElement//
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=message
    input_group.classList.add('error')
   
}

function setSuccess(element)
{
    let input_group= element.parentElement
    let error_element=input_group.querySelector('.error')

    error_element.innerHTML=""
    input_group.classList.add('success')
    input_group.classList.remove('error')
}

function setPassword(element, message) {
    let input_group = element.parentElement;
    let warn_element = input_group.querySelector('.warn');

    warn_element.innerHTML = message;
    input_group.classList.add('success');
    input_group.classList.add('warn');
    input_group.classList.remove('error');
}

function validateEmail(ev) {
    return String(ev).toLowerCase().match(
        /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
      );
  }
 
  function validatePassword(password) {
     // Check if the password contains an uppercase letter and a special character
     return /[A-Z]/.test(password)&&/\d/.test(password) && /[!@#$%^&*(),.?":{}|<>]/.test(password);
 }

/*************  form validation end  ***************/
/***************  job portal  *****************/
