document.addEventListener("DOMContentLoaded", function() {
    // Fetch and display project data
    const projectsSection = document.getElementById('projects');
    const projectsData = [
        { name: 'Grocery Store', description: 'Built using HTML, CSS, and JS' },
        { name: 'Job Portal', description: 'Developed with HTML, CSS, and JS' }
        // Add more project data as needed
    ];

    projectsData.forEach(project => {
        const projectDiv = document.createElement('div');
        projectDiv.innerHTML = `<h3>${project.name}</h3><p>${project.description}</p>`;
        projectsSection.appendChild(projectDiv);
    });
});
