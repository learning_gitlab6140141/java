public class Bus
{
	String seat1 = "Abish with his son";
	float ticket1 = 1.5f;
	String seat2 = "Bhanu and Bhuvi";
	short ticket2 = 2;
	String seat3 = "Charen";
	short ticket3 =1;
	public static void main (String[] arg)
	{
		Bus person1 = new Bus();
		Bus person2 = new Bus();
		Bus person3 = new Bus();
		
		System.out.print("passenger :"+person1.seat1);
		System.out.println("....No.of tickets :"+person1.ticket1);
		System.out.print("passenger :"+person2.seat2);
		System.out.println("....No.of tickets :"+person2.ticket2);
		System.out.print("passenger :"+person3.seat3);
		System.out.println("....No.of tickets :"+person3.ticket3);
	}
}

//A Java object is a member (also called an instance) of a Java class. Each object has an identity,
//a behavior and a state. The state of an object is stored in fields (variables), while methods (functions)
//isplay the object's behavior. Objects are created at runtime from templates, which are also known as classes.
