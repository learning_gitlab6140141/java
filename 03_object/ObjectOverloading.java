public class ObjectOverloading
{
	public static void main (String [] args)
	{
		ObjectOverloading object = new ObjectOverloading(); //creating object
		
		object.math("doing calculation");
		
		int result = object.math(23);// calling method with one value
		System.out.println(result);//printing the variable returned from te method
		
		System.out.println(object.math(23,40.6f));// calling method with two value
				
		double result1 = object.math(23.4,56.6,20.6);// calling method with three value
		System.out.println(result1);
		
		
		
	}
	
	public float math (int no1, float no2)
	{
		return no1 + no2 ;
	}
	
	public int math(int no)
	{
		return no+30;
	}
	
	public double math(double dec1,double dec2,double dec3)
	{
		return (dec1*dec2)/dec3;
	}
	
	public void math(String proc)
	{
		System.out.println(proc);
	}
}